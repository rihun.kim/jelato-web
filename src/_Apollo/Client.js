import { ApolloClient } from "apollo-client";
import { InMemoryCache } from "apollo-cache-inmemory";
import { HttpLink } from "apollo-link-http";
import { onError } from "apollo-link-error";
import { ApolloLink, split } from "apollo-link";
import { WebSocketLink } from "apollo-link-ws";
import { getMainDefinition } from "apollo-utilities";

import { resolvers } from "./ClientState";

const httpLink = new HttpLink({
  uri: process.env.NODE_ENV === "development" ? "http://localhost:1990/" : "NOTYET",
  headers: {
    Authorization: `Bearer ${sessionStorage.getItem("TOKEN")}`
  }
});

const wsLink = new WebSocketLink({
  uri: process.env.NODE_ENV === "development" ? `ws://localhost:1990` : "NOTYET",
  options: {
    reconnect: true,
    connectionParams: {
      token: sessionStorage.getItem("TOKEN")
    }
  }
});

export default new ApolloClient({
  resolvers: resolvers,
  link: ApolloLink.from([
    onError(({ graphQLErrors, networkError }) => {
      if (graphQLErrors)
        graphQLErrors.map(({ message, locations, path }) =>
          console.log(`[GraphQL error]: Message: ${message}, Location: ${locations}, Path: ${path}`)
        );
      if (networkError) console.log(`[Network error]: ${networkError}`);
    }),
    split(
      ({ query }) => {
        const definition = getMainDefinition(query);
        return definition.kind === "OperationDefinition" && definition.operation === "subscription";
      },
      wsLink,
      httpLink
    )
  ]),
  cache: new InMemoryCache()
});
