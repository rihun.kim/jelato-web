export const resolvers = {
  Mutation: {
    doLogin: (_, { token }, { cache }) => {
      sessionStorage.setItem("TOKEN", token);
      return null;
    },
    doLogout: (_, __, { cache }) => {
      sessionStorage.removeItem("TOKEN");
      window.location = "/";
      return null;
    }
  },
  Query: {
    isLogin: () => {
      return Boolean(sessionStorage.getItem("TOKEN")) || false;
    }
  }
};
