import { gql } from "apollo-boost";

export const READ_OWNPLAYERS = gql`
  query {
    readOwnplayers {
      id
      name
      position

      playground {
        id
        code
        color
        status
        title
        story
        players {
          id
          name
          position
        }
      }
    }
  }
`;

export const SEARCH_PLAYGROUNDS = gql`
  query searchPlaygrounds($keyword: String!) {
    searchPlaygrounds(keyword: $keyword) {
      id
      code
      color
      status
      title
      story
    }
  }
`;

export const CREATE_PLAYGROUND = gql`
  mutation createPlayground($color: String!, $title: String!, $story: String!) {
    createPlayground(color: $color, title: $title, story: $story) {
      id
      code
      color
      title
      story
      createdAt
    }
  }
`;

export const CREATE_PLAYER = gql`
  mutation createPlayer($name: String!, $playgroundId: String!, $position: String!) {
    createPlayer(name: $name, playgroundId: $playgroundId, position: $position) {
      id
      name
      playground {
        id
        color
        code
        title
        status
        story
      }
      position
      createdAt
    }
  }
`;

export const UPDATE_PLAYER = gql`
  mutation updatePlayer($name: String, $playerId: String!, $position: String) {
    updatePlayer(name: $name, playerId: $playerId, position: $position) {
      id
      name
      position
    }
  }
`;

export const DELETE_PLAYER = gql`
  mutation deletePlayer($playerId: String!, $playgroundId: String) {
    deletePlayer(playerId: $playerId, playgroundId: $playgroundId)
  }
`;

export const READ_PLAYGROUND = gql`
  query readPlayground($playgroundId: String!) {
    readPlayground(playgroundId: $playgroundId) {
      id
      players {
        id
        name
        position
      }
    }
  }
`;

export const UPDATE_PLAYGROUND = gql`
  mutation updatePlayground(
    $playgroundId: String!
    $color: String
    $status: Boolean
    $title: String
    $story: String
  ) {
    updatePlayground(
      playgroundId: $playgroundId
      color: $color
      status: $status
      title: $title
      story: $story
    ) {
      id
      code
      color
      title
      status
      story
    }
  }
`;
