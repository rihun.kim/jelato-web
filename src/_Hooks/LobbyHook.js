import { gql } from "apollo-boost";

export const IS_LOGIN = gql`
  query {
    isLogin @client
  }
`;

export const DO_LOGIN = gql`
  mutation doLogin($token: String!) {
    doLogin(token: $token) @client
  }
`;

export const DO_LOGOUT = gql`
  mutation doLogout {
    doLogout @client
  }
`;

export const REQUEST_TOKEN = gql`
  mutation requestUserToken($email: String!, $password: String!) {
    requestUserToken(email: $email, password: $password)
  }
`;

export const CREATE_VSER = gql`
  mutation createVser($email: String!) {
    createVser(email: $email)
  }
`;

export const CREATE_USER = gql`
  mutation createUser($email: String!, $password: String!, $code: String!) {
    createUser(email: $email, password: $password, code: $code)
  }
`;

export const VERIFY_USER = gql`
  mutation verifyUser($email: String!) {
    verifyUser(email: $email)
  }
`;

export const VERIFY_CODE = gql`
  mutation verifyCode($email: String!, $password: String!, $code: String!) {
    verifyCode(email: $email, password: $password, code: $code)
  }
`;
