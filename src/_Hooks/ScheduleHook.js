import { gql } from "apollo-boost";

export const CREATE_SCHEDULE = gql`
  mutation createSchedule(
    $playerId: String!
    $playgroundId: String!
    $start: String!
    $end: String!
    $title: String!
    $color: String!
  ) {
    createSchedule(
      playerId: $playerId
      playgroundId: $playgroundId
      start: $start
      end: $end
      title: $title
      color: $color
    ) {
      id
      start
      end
      title
      color
      createdAt

      player {
        id
        name
        position
      }
    }
  }
`;

export const READ_SCHEDULES = gql`
  query readSchedules($playgroundId: String!) {
    readSchedules(playgroundId: $playgroundId) {
      id
      color
      start
      end
      title
      createdAt

      player {
        id
        name
        position
      }
    }
  }
`;

export const UPDATE_SCHEDULE = gql`
  mutation updateSchedule(
    $scheduleId: String!
    $start: String
    $end: String
    $title: String
    $color: String
  ) {
    updateSchedule(
      scheduleId: $scheduleId
      start: $start
      end: $end
      title: $title
      color: $color
    ) {
      id
    }
  }
`;

export const DELETE_SCHEDULE = gql`
  mutation deleteSchedule($scheduleId: String!) {
    deleteSchedule(scheduleId: $scheduleId)
  }
`;
