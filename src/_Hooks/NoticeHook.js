import { gql } from "apollo-boost";

export const CREATE_NOTICE = gql`
  mutation createNotice(
    $playerId: String!
    $playgroundId: String!
    $title: String!
    $story: String!
  ) {
    createNotice(playerId: $playerId, playgroundId: $playgroundId, title: $title, story: $story) {
      id
      player {
        id
        name
      }
      playground {
        id
      }
      story
      title
      createdAt
    }
  }
`;

export const READ_NOTICES = gql`
  query readNotices($playgroundId: String!, $noticeId: String) {
    readNotices(playgroundId: $playgroundId, noticeId: $noticeId) {
      id
      player {
        id
        name
      }
      playground {
        id
      }
      story
      title
      createdAt
    }
  }
`;

export const UPDATE_NOTICE = gql`
  mutation updateNotice($noticeId: String!, $title: String, $story: String) {
    updateNotice(noticeId: $noticeId, title: $title, story: $story) {
      id
    }
  }
`;

export const DELETE_NOTICE = gql`
  mutation deleteNotice($noticeId: String!) {
    deleteNotice(noticeId: $noticeId)
  }
`;
