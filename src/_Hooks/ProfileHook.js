import { gql } from "apollo-boost";

export const READ_USER = gql`
  query {
    readUser {
      id
      email
    }
  }
`;

export const UPDATE_USER = gql`
  mutation updateUser($email: String, $password: String) {
    updateUser(email: $email, password: $password)
  }
`;

export const DELETE_USER = gql`
  mutation deleteUser($password: String!) {
    deleteUser(password: $password)
  }
`;
