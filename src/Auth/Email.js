import { SESSION_KEY_USEREMAIL, SESSION_VALUE_NONE } from './Constant';

export const getUserEmail = () => {
  return sessionStorage.getItem(SESSION_KEY_USEREMAIL);
};

export const setUserEmail = userEmail => {
  sessionStorage.setItem(SESSION_KEY_USEREMAIL, userEmail);
};

export const clearUserEmail = () => {
  sessionStorage.setItem(SESSION_KEY_USEREMAIL, SESSION_VALUE_NONE);
};
