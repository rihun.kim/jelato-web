import React, { createContext, useContext, useState } from 'react';

import { clearUserEmail, setUserEmail } from './Email';
import { clearUserToken, setUserToken } from './Token';
import { SESSION_VALUE_NONE } from './Constant';

export const AuthContext = createContext();
export const AuthProvider = ({ userToken, children }) => {
  const [_isLogin, _setIsLogin] = useState(userToken === SESSION_VALUE_NONE ? false : true);

  const _doLogin = (userEmail, userToken) => {
    try {
      setUserEmail(userEmail);
      setUserToken(userToken);
      _setIsLogin(true);
    } catch (e) {
      console.log(e);
    }
  };
  const _doLogout = () => {
    try {
      clearUserToken();
      clearUserEmail();
      _setIsLogin(false);
    } catch (e) {
      console.log(e);
    }
  };

  return <AuthContext.Provider value={{ _isLogin, _doLogin, _doLogout }}>{children}</AuthContext.Provider>;
};

const _IsLogin = () => {
  const { _isLogin } = useContext(AuthContext);
  return _isLogin;
};

const _UseDoLogin = () => {
  const { _doLogin } = useContext(AuthContext);
  return _doLogin;
};

const _UseDoLogout = () => {
  const { _doLogout } = useContext(AuthContext);
  return _doLogout;
};

export const isLogin = () => _IsLogin();
export const useDoLogin = () => _UseDoLogin();
export const useDoLogout = () => _UseDoLogout();
