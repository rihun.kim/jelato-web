import { SESSION_KEY_USERTOKEN, SESSION_VALUE_NONE } from './Constant';

export const setAuth = async setUserToken => {
  const userToken = getUserToken();
  setUserToken(userToken == null ? SESSION_VALUE_NONE : userToken);
};

export const getUserToken = () => {
  return sessionStorage.getItem(SESSION_KEY_USERTOKEN);
};

export const setUserToken = userToken => {
  sessionStorage.setItem(SESSION_KEY_USERTOKEN, userToken);
};

export const clearUserToken = () => {
  sessionStorage.setItem(SESSION_KEY_USERTOKEN, SESSION_VALUE_NONE);
};
