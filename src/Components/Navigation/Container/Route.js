import React from 'react';

import Lobby from '../Lobby/Main';
import Hall from '../Hall/Main';

export const LobbyRoute = () => {
  return <Lobby />;
};

export const HallRoute = () => {
  return <Hall />;
};
