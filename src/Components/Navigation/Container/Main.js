import React from 'react';
import { BrowserRouter } from 'react-router-dom';

import { LobbyRoute, HallRoute } from './Route';
import { isLogin } from '../../../Auth/Context';

const Main = () => {
  return <BrowserRouter>{isLogin() ? <HallRoute /> : <LobbyRoute />}</BrowserRouter>;
};

export default Main;
