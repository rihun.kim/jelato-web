import React from 'react';
import { Route, Switch } from 'react-router-dom';

import Selector from '../../Selector/Main';
import Playground from '../../Screen/Hall/Playground/Container/Main';
import Schedule from '../../Screen/Hall/Schedule/Container/Main';
import Notice from '../../Screen/Hall/Notice/Container/Main';
import Chat from '../../Screen/Hall/Chat/Container/Main';

const Main = () => {
  const main = (
    <Switch>
      <Route exact path="/" component={Playground} />
      <Route exact path="/schedule/:barcode" component={Schedule} />
      <Route exact path="/notice/:barcode" component={Notice} />
      <Route exact path="/chat/:barcode" component={Chat} />
    </Switch>
  );

  return <Selector main={main} />;
};

export default Main;
