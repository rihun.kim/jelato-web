import React from 'react';
import { Route, Switch } from 'react-router-dom';

import Lobby from '../../Screen/Lobby/Container/Main';

const Main = () => {
  return (
    <Switch>
      <Route exact path="/" component={Lobby} />
    </Switch>
  );
};

export default Main;
