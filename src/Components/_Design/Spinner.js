import React from "react";
import Loader from "react-loader-spinner";

import styled from "styled-components";

const MainSpinnerViewer = styled.div`
  width: ${(props) => String(props.width) + "px"};
  height: 100%;

  align-items: center;
  display: flex;
  flex-direction: column;
  justify-content: center;
  left: ${(props) => String(props.left) + "px"};
  position: ${(props) => String(props.position)};
  top: 0;
`;

const SubSpinnerViewer = styled.div`
  width: 100%;
  height: ${(props) => props.height};

  align-items: center;
  display: flex;
  justify-content: center;
  margin: ${(props) => props.margin};
`;

const ChatSpinnerViewer = styled.div`
  width: 150px;
  height: 18px;

  align-items: center;
  background-color: #ffffff2b;
  border: none;
  border-radius: 5px;
  display: flex;
  flex-direction: column;
  justify-content: center;
  margin: 0 auto 20px auto;
`;

const ScheduleSpinnerViewer = styled.div`
  // width: 150px;
  height: 18px;

  align-items: center;
  background-color: #ffffff2b;
  border: none;
  border-radius: 5px;
  display: flex;
  flex-direction: column;
  justify-content: center;
  margin: auto auto auto auto;
`;

export const InnerSpinner = ({ color = "#2b2b28", height = "100%", margin = "0 0 0 0" }) => {
  return (
    <SubSpinnerViewer height={height} margin={margin}>
      <Loader type="ThreeDots" color={color} height={18} width={18} />
    </SubSpinnerViewer>
  );
};

export const Spinner = ({ windowLeft = "0", windowWidth = "0", position = "fixed" }) => {
  if (windowWidth === "0") windowWidth = window.innerWidth;

  return (
    <MainSpinnerViewer left={windowLeft} width={windowWidth} position={position}>
      <Loader type="Grid" color="#2b2b28" height={30} width={30} />
    </MainSpinnerViewer>
  );
};

export const ChatSpinner = () => {
  return (
    <ChatSpinnerViewer>
      <Loader type="ThreeDots" color="#ffffff" height={18} width={18} />
    </ChatSpinnerViewer>
  );
};

export const ScheduleSpinner = () => {
  return (
    <ScheduleSpinnerViewer>
      <Loader type="ThreeDots" color="#000000" height={18} width={18} />
    </ScheduleSpinnerViewer>
  );
};

export const LoginSpinner = () => {
  return (
    <ScheduleSpinnerViewer>
      <Loader type="ThreeDots" color="#000000" height={18} width={18} />
    </ScheduleSpinnerViewer>
  );
};

export const ButtonSpinner = () => {
  return (
    <ScheduleSpinnerViewer>
      <Loader type="ThreeDots" color="#000000" height={18} width={18} />
    </ScheduleSpinnerViewer>
  );
};
