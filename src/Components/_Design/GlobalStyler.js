import { createGlobalStyle } from "styled-components";

export default createGlobalStyle`
  body {
    font-family:-apple-system,BlinkMacSystemFont,'Segoe UI',Roboto,Oxygen,Ubuntu,Cantarell,'Open Sans','Helvetica Neue',sans-serif;
    margin: 0 0 0 0;
  }
  a {
    text-decoration: none;
  }
  ul {
    list-style-type: none;
    padding: 0;
  }
  li {
    outline: none;
  }
  * {
    box-sizing: border-box;
  }
`;

export const MAIN_COLOR = "#2b2b28";
export const TAB_INACTIVE_COLOR = "gray";
