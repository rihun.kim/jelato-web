import React, { useState } from "react";
import { useLazyQuery } from "react-apollo";

import { Viewer, InnerViewer } from "./Styler";
import { SEARCH_PLAYGROUNDS } from "../../../Hooks/PlaygroundHook";
import Header from "./Header";
import OwnList from "./OwnList";
import SearchList from "./SearchList";

const Container = ({ setRouteRefresh, remoteOwnplayers }) => {
  const [clickView, setClickView] = useState("ownView");
  const [
    searchPlaygroundsLazyQuery,
    { loading: searchLoading, data: remotePlaygrounds },
  ] = useLazyQuery(SEARCH_PLAYGROUNDS, { fetchPolicy: "no-cache" });

  return (
    <Viewer width={window.innerWidth - 200 + "px"} height="100vh" left="200px" position="fixed">
      <InnerViewer width="1060px" height="100%" margin="0 auto 0 auto">
        <Header
          loading={searchLoading}
          clickView={clickView}
          setClickView={setClickView}
          searchPlaygroundsLazyQuery={searchPlaygroundsLazyQuery}
        />
        {clickView === "ownView" ? (
          <OwnList setRouteRefresh={setRouteRefresh} remoteOwnplayers={remoteOwnplayers} />
        ) : (
          false
        )}
        {clickView === "searchView" ? (
          <SearchList
            setRouteRefresh={setRouteRefresh}
            remoteOwnplayers={remoteOwnplayers}
            remotePlaygrounds={remotePlaygrounds}
          />
        ) : (
          false
        )}
      </InnerViewer>
    </Viewer>
  );
};

export default Container;
