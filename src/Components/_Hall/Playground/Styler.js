import React, { useState } from "react";

import styled from "styled-components";
import { IoMdClose, IoMdSearch, IoMdHappy, IoIosPower, IoMdRadioButtonOff } from "react-icons/io";
import { MdExpandMore, MdExpandLess } from "react-icons/md";
import { GiFireBomb } from "react-icons/gi";

//
export const Viewer = styled.div`
  width: ${(props) => props.width};
  height: ${(props) => props.height};

  left: ${(props) => props.left};
  margin: ${(props) => props.margin};
  overflow-y: auto;
  padding: ${(props) => props.padding};
  position: ${(props) => props.position};
`;

export const InnerViewer = styled.div`
  width: ${(props) => props.width};
  height: ${(props) => props.height};

  background-color: #ffffff;
  margin: ${(props) => props.margin};
  overflow-y: auto;
`;

export const GridViewer = styled.div`
  width: ${(props) => props.width};
  height: ${(props) => props.height};

  background-color: #ffffff;
  display: grid;
  grid-gap: 20px;
  grid-template-columns: repeat(4, minmax(210px, 250px));
`;

export const ModalViewer = styled.div`
  width: 700px;
  height: ${(props) => props.height};

  background-color: #ffffff;
  border-radius: 3px;
  box-shadow: 0 0 4px rgba(0, 0, 0, 0.14), 0 4px 8px rgba(0, 0, 0, 0.28);
  left: ${(props) => props.left};
  padding: 40px 25px 20px 25px;
  position: fixed;
  top: ${(props) => props.top};
  z-index: 3;
`;

export const ListViewer = styled.div`
  width: ${(props) => props.width};
  height: ${(props) => props.height};

  background-color: #ffffff;
  overflow-y: auto;
  text-align: left;
`;

export const HeaderViewer = styled.div`
  width: 330px;
  height: 28px;

  border-bottom: 1px solid #e0e0e0;
  display: flex;
  margin: auto 0 auto auto;
`;

//
export const ListTitle = styled.div`
  width: 100%;
  height: 15px;

  font-size: 14px;
  font-weight: 500;
  margin: 0 0 20px 0;
`;

export const List = styled.div`
  width: 100%;
  height: 25px;

  font-size: 12px;
  font-weight: 400;
`;

//
export const MoreButton = ({ onClick }) => {
  return (
    <MdExpandMore
      style={{
        width: "15px",
        height: "15px",
        color: "#a5a2a2",
        cursor: "pointer",
        position: "absolute",
        right: 60,
        top: 20,
        zIndex: 3,
      }}
      onClick={onClick}
    />
  );
};

export const LessButton = ({ onClick }) => {
  return (
    <MdExpandLess
      style={{
        width: "15px",
        height: "15px",
        color: "#a5a2a2",
        cursor: "pointer",
        position: "absolute",
        right: 60,
        top: 20,
        zIndex: 3,
      }}
      onClick={onClick}
    />
  );
};

export const SaveButton = ({ onClick }) => {
  return (
    <IoMdRadioButtonOff
      title="저장하기"
      style={{
        width: "14px",
        height: "14px",
        color: "#a5a2a2",
        cursor: "pointer",
        position: "absolute",
        right: 40,
        top: 20,
        zIndex: 3,
      }}
      onClick={onClick}
    />
  );
};

export const CloseButton = ({ onClick }) => {
  return (
    <IoMdClose
      title="닫기"
      style={{
        width: "16px",
        height: "16px",
        color: "#a5a2a2",
        cursor: "pointer",
        position: "absolute",
        right: 20,
        top: 20,
        zIndex: 3,
      }}
      onClick={onClick}
    />
  );
};

export const DeleteButton = ({ onClick }) => {
  return (
    <GiFireBomb
      title="삭제하기"
      style={{
        width: "15px",
        height: "15px",
        color: "#a5a2a2",
        cursor: "pointer",
        position: "absolute",
        right: 20,
        top: 20,
        zIndex: 3,
      }}
      onClick={onClick}
    />
  );
};

const HeaderButtonViewer = styled.div`
  color: ${(props) => (props.clickView === "" ? "#929191" : "#e095b8")};
  cursor: pointer;
  margin: auto 4px auto 4px;
`;

export const HeaderButton = ({ clickView, onClick, type }) => (
  <HeaderButtonViewer clickView={clickView} onClick={onClick}>
    {type === "s" ? <IoMdSearch /> : false}
    {type === "o" ? <IoMdHappy /> : false}
    {type === "p" ? <IoIosPower /> : false}
  </HeaderButtonViewer>
);

//
export const useInput = (prefilled) => {
  const [value, setValue] = useState(prefilled);

  const onChange = (e) => {
    setValue(e.target.value);
  };

  return { onChange, value };
};

const onKeyDown = (e, onSearch, setClickView) => {
  if (e.keyCode === 13) {
    e.preventDefault();
    onSearch();
    setClickView("searchView");
  }
};

const SearchInputViewer = styled.input`
  width: 280px;
  height: 26px;

  border: none;
  font-size: 14px;
  padding: 0 0 0 15px;
  outline: none;
`;

export const SearchInput = ({ onChange, onSearch, placeholder, setClickView, value }) => (
  <SearchInputViewer
    defaultValue={value}
    onChange={onChange}
    onKeyDown={(e) => onKeyDown(e, onSearch, setClickView)}
    placeholder={placeholder}
    type="text"
  />
);

const InputViewer = styled.input`
  width: 100%;
  height: 35px;

  border: 0;
  border-bottom: 1px solid #e0e0e0;
  font-size: 14px;
  margin: 0 0 10px 0;
  outline: none;
  padding: 0 15px 0 15px;
`;

export const Input = ({ onChange, placeholder, value }) => (
  <InputViewer defaultValue={value} onChange={onChange} placeholder={placeholder} type="text" />
);

const ListInputViewer = styled.input`
  width: 90%;
  height: 18px;

  border: 0;
  border-bottom: 1px solid #e0e0e0;
  font-size: 12px;
  outline: none;
`;

export const ListInput = ({ onChange, placeholder, value }) => (
  <ListInputViewer defaultValue={value} onChange={onChange} placeholder={placeholder} type="text" />
);

const TextareaViewer = styled.textarea`
  width: 100%;
  height: 110px;

  border: 0;
  border-bottom: 1px solid #e0e0e0;
  font-size: 14px;
  margin: 0 0 10px 0;
  outline: none;
  padding: 10px 15px 10px 15px;
  resize: none;
  white-space: pre-wrap;
`;

export const TextareaInput = ({ onChange, placeholder, value }) => (
  <TextareaViewer defaultValue={value} onChange={onChange} placeholder={placeholder} type="text" />
);

//
export const Group = styled.div`
  display: ${(props) => props.display};
  float: ${(props) => props.float};
`;

export const Card = styled.div`
  width: 250px;
  height: 340px;

  background-image: ${(props) => props.color};
  border-radius: 3px;
  border: ${(props) => props.border};
  box-shadow: 1px 3px 4px rgba(0, 0, 0, 0.19), 0 1px 0 rgba(0, 0, 0, 0.23);
  cursor: pointer;
  padding: 20px 20px 20px 20px;
  position: relative;
`;

export const Color = styled.div`
  width: 20px;
  height: 25px;
  background-image: ${(props) => props.color};
  border-radius: 3px;
  color: #ffffff;
  cursor: pointer;
  font-size: 14px;
  margin: 0 auto 0 auto;
  text-align: center;
`;

export const Text = styled.div`
  width: ${(props) => props.width};
  height: ${(props) => props.height};

  border-bottom: ${(props) => props.borderBottom};
  bottom: ${(props) => props.bottom};
  color: ${(props) => props.color};
  float: ${(props) => props.float};
  font-size: ${(props) => props.fontSize};
  font-weight: ${(props) => props.fontWeight};
  line-height: ${(props) => props.lineHeight};
  margin: ${(props) => props.margin};
  overflow: ${(props) => props.overflow};
  padding: ${(props) => props.padding};
  position: ${(props) => props.position};
  text-align: ${(props) => props.textAlign};
  white-space: pre-wrap;
`;

//
export const CardStyle = {
  code: {
    color: "#ffffff",
    fontSize: "15px",
    float: "right",
  },
  title: {
    width: "210px",
    height: "44px",
    fontSize: "18px",
    fontWeight: "600",
    margin: "70px 0 0 0",
    overflow: "hidden",
  },
  story: {
    width: "210px",
    height: "110px",
    color: "#ffffff",
    fontSize: "13px",
    fontWeight: "300",
    margin: "15px 0 0 0",
    overflow: "hidden",
  },
  name: {
    width: "210px",
    height: "15px",
    bottom: "20px",
    color: "#ffffff",
    fontSize: "13px",
    fontWeight: "70",
    overflow: "hidden",
    position: "absolute",
  },
};

export const ModalStyle = {
  code: {
    color: "#ffffff",
    fontSize: "15px",
    float: "right",
  },
  title: {
    width: "100%",
    height: "35px",
    color: "#7d7d7d",
    fontSize: "14px",
    lineHeight: "35px",
    margin: "0 0 10px 0",
    padding: "0 15px 0 15px",
    textAlign: "left",
    overflow: "hidden",
  },
  story: {
    width: "100%",
    height: "150px",
    color: "#7d7d7d",
    fontSize: "14px",
    padding: "10px 15px 10px 15px",
    textAlign: "left",
    overflow: "auto",
  },
  confirm: {
    width: "100%",
    color: "#797979",
    fontSize: "14px",
    margin: "0 0 5px 0",
    textAlign: "center",
  },
  textbutton: {
    color: "#a0a0a0",
    cursor: "pointer",
    fontSize: "10px",
    textAlign: "center",
  },
};
