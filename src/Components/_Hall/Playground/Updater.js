import React, { useState } from "react";
import { useQuery, useMutation } from "@apollo/react-hooks";
import { Carousel } from "react-responsive-carousel";
import "react-responsive-carousel/lib/styles/carousel.min.css";

import {
  ModalViewer,
  InnerViewer,
  ListViewer,
  ListTitle,
  List,
  ListInput,
  Color,
  MoreButton,
  LessButton,
  CloseButton,
  SaveButton,
  DeleteButton,
  Text,
  Group,
  Input,
  useInput,
  ModalStyle,
  TextareaInput,
} from "./Styler";
import {
  getPlayerByPlayerId,
  getPlayerByPlaygroundId,
  getLeftMove,
  getTopMove,
  deletePlayerByPlayerId,
  deletePlaygroundByPlaygroundId,
  gradColors,
  getGradColorIndex,
} from "./Tool";
import {
  READ_PLAYGROUND,
  UPDATE_PLAYER,
  DELETE_PLAYER,
  UPDATE_PLAYGROUND,
} from "../../../Hooks/PlaygroundHook";
import { InnerSpinner } from "../../Design/Spinner";
import Confirmer from "./Confirmer";

const OwningMembers = (playgroundId) => {
  const { loading, data: remotePlayground } = useQuery(READ_PLAYGROUND, {
    variables: { playgroundId: playgroundId },
    fetchPolicy: "no-cache",
  });

  const filter = (players) => {
    let filteredPlayers = { owning: {}, ining: [], outing: [], waiting: [] };
    for (const player of players) {
      if (player.position === "ining") {
        filteredPlayers.ining.push(player);
      } else if (player.position === "owning") {
        filteredPlayers.owning = player;
      } else if (player.position === "outing") {
        filteredPlayers.outing.push(player);
      } else if (player.position === "waiting") {
        filteredPlayers.waiting.push(player);
      }
    }
    return filteredPlayers;
  };

  if (loading || remotePlayground === undefined) {
    return false;
  }

  return filter(remotePlayground.readPlayground.players);
};

const OwningUpdater = ({ clickPlayer, remotePlaygrounds, remoteOwnplayers, toggleModal }) => {
  const [, setRefresh] = useState("");
  const [moreable, setMoreable] = useState(true);
  const [clickColor, setClickColor] = useState(getGradColorIndex(clickPlayer.playground.color));
  const [deletePlayerMutation] = useMutation(DELETE_PLAYER);
  const [updatePlayerMutation] = useMutation(UPDATE_PLAYER);
  const [updatePlaygroundMutation] = useMutation(UPDATE_PLAYGROUND);
  const titleUseInput = useInput(clickPlayer.playground.title);
  const nameUseInput = useInput(clickPlayer.name);
  const storyUseInput = useInput(clickPlayer.playground.story);

  const onSave = () => {
    try {
      if (nameUseInput.value === "") {
        alert("닉네임을 적어주세요.");
      } else if (titleUseInput.value === "") {
        alert("Playground 의 이름을 적어주세요.");
      } else if (storyUseInput.value === "") {
        alert("Playground 를 소개해주세요.");
      } else {
        updatePlayerMutation({
          variables: {
            playerId: clickPlayer.id,
            name: nameUseInput.value,
          },
        });
        updatePlaygroundMutation({
          variables: {
            playgroundId: clickPlayer.playground.id,
            color: gradColors[clickColor],
            title: titleUseInput.value,
            story: storyUseInput.value,
          },
        });
        clickPlayer.name = nameUseInput.value;
        clickPlayer.playground.color = gradColors[clickColor];
        clickPlayer.playground.title = titleUseInput.value;
        clickPlayer.playground.story = storyUseInput.value;
        toggleModal();
      }
    } catch (error) {
      alert(error);
    }
  };
  const onDelete = () => {
    try {
      deletePlayerMutation({
        variables: {
          playerId: clickPlayer.id,
          playgroundId: clickPlayer.playground.id,
        },
      });
      deletePlayerByPlayerId(remoteOwnplayers.readOwnplayers, clickPlayer.id);
      if (remotePlaygrounds !== undefined) {
        deletePlaygroundByPlaygroundId(
          remotePlaygrounds.searchPlaygrounds,
          clickPlayer.playground.id
        );
      }
      toggleModal();
    } catch (error) {
      alert(error);
    }
  };
  const onOuting = (id) => {
    try {
      updatePlayerMutation({
        variables: {
          playerId: id,
          position: "outing",
        },
      });
      let player =
        getPlayerByPlayerId(filteredPlayers.waiting, id) ||
        getPlayerByPlayerId(filteredPlayers.ining, id);
      player.position = "outing";
      setRefresh(new Date());
    } catch (error) {
      alert(error);
    }
  };
  const onIning = (id) => {
    try {
      updatePlayerMutation({
        variables: {
          playerId: id,
          position: "ining",
        },
      });
      getPlayerByPlayerId(filteredPlayers.waiting, id).position = "ining";
      setRefresh(new Date());
    } catch (error) {
      alert(error);
    }
  };

  let filteredPlayers = OwningMembers(clickPlayer.playground.id);

  return (
    <ModalViewer height="340px" left={getLeftMove(700)} top={getTopMove(340)}>
      {moreable ? (
        <>
          <MoreButton onClick={() => setMoreable(false)} />
          <SaveButton onClick={onSave} />
          <CloseButton onClick={toggleModal} />
        </>
      ) : (
        <>
          <LessButton onClick={() => setMoreable(true)} />
          <DeleteButton onClick={onDelete} />
        </>
      )}
      <Carousel showArrows={false} showStatus={false} showThumbs={false}>
        <InnerViewer height="280px">
          <Input {...nameUseInput} />
          <Input {...titleUseInput} />
          <TextareaInput {...storyUseInput} />
          <Group display="flex">
            {gradColors.map((color, index) => (
              <Color key={index} color={color} onClick={() => setClickColor(index)}>
                {index === clickColor ? "*" : false}
              </Color>
            ))}
          </Group>
        </InnerViewer>
        {filteredPlayers ? (
          <ListViewer height="280px">
            <ListTitle>MEMBERS</ListTitle>
            <List>
              {filteredPlayers.owning.name} (owner)
              <Group display="flex" float="right"></Group>
            </List>
            {filteredPlayers.ining.length === 0
              ? false
              : filteredPlayers.ining.map(({ id, name }) => (
                  <List key={id}>
                    {name} (member)
                    <Group display="flex" float="right">
                      <Text style={ModalStyle.textbutton} onClick={() => onOuting(id)}>
                        강퇴
                      </Text>
                    </Group>
                  </List>
                ))}
            {filteredPlayers.waiting.length === 0
              ? false
              : filteredPlayers.waiting.map(({ id, name }) => (
                  <List key={id}>
                    {name}
                    <Group display="flex" float="right">
                      <Text style={ModalStyle.textbutton} margin="0 20px 0 0">
                        멤버로 승인하시겠습니까?
                      </Text>
                      <Text
                        style={ModalStyle.textbutton}
                        margin="0 10px 0 0"
                        onClick={() => onIning(id)}
                      >
                        승인
                      </Text>
                      <Text style={ModalStyle.textbutton} onClick={() => onOuting(id)}>
                        거절
                      </Text>
                    </Group>
                  </List>
                ))}
          </ListViewer>
        ) : (
          <ListViewer height="280px">
            <InnerSpinner />
          </ListViewer>
        )}
      </Carousel>
    </ModalViewer>
  );
};

const IningUpdater = ({ clickPlayer, remotePlaygrounds, remoteOwnplayers, toggleModal }) => {
  const [nameDisable, setNameDisable] = useState(true);
  const [deletePlayerMutation] = useMutation(DELETE_PLAYER);
  const [updatePlayerMutation] = useMutation(UPDATE_PLAYER);
  const nameUseInput = useInput(clickPlayer.name);

  const toggleName = () => {
    nameDisable ? setNameDisable(false) : setNameDisable(true);
  };
  const onSave = () => {
    try {
      if (nameUseInput.value === "") {
        alert("닉네임 적어주세요.");
      } else {
        updatePlayerMutation({
          variables: {
            playerId: clickPlayer.id,
            name: nameUseInput.value,
          },
        });
        clickPlayer.name = nameUseInput.value;
        toggleModal();
      }
    } catch (error) {
      alert(error);
    }
  };
  const onDelete = () => {
    try {
      deletePlayerMutation({
        variables: {
          playerId: clickPlayer.id,
        },
      });
      deletePlayerByPlayerId(remoteOwnplayers.readOwnplayers, clickPlayer.id);
      if (remotePlaygrounds !== undefined) {
        deletePlaygroundByPlaygroundId(
          remotePlaygrounds.searchPlaygrounds,
          clickPlayer.playground.id
        );
      }
      toggleModal();
    } catch (error) {
      alert(error);
    }
  };
  const filter = (players) => {
    let filteredPlayers = { owning: {}, ining: [] };
    for (const player of players) {
      if (player.id === clickPlayer.id) {
      } else if (player.position === "ining") {
        filteredPlayers.ining.push(player);
      } else if (player.position === "owning") {
        filteredPlayers.owning = player;
      }
    }
    return filteredPlayers;
  };

  const filteredPlayers = filter(clickPlayer.playground.players);
  return (
    <ModalViewer height="340px" left={getLeftMove(700)} top={getTopMove(340)}>
      <MoreButton />
      <SaveButton onClick={onSave} />
      <CloseButton onClick={toggleModal} />
      <ListViewer height="280px">
        <ListTitle>MEMBERS</ListTitle>
        <List>{filteredPlayers.owning.name} (owner)</List>
        <List>
          {nameDisable ? nameUseInput.value + " (me)" : <ListInput {...nameUseInput} />}
          <Group display="flex" float="right">
            <Text style={ModalStyle.textbutton} margin="0 10px 0 0" onClick={toggleName}>
              {nameDisable ? "닉네임 변경" : "확인"}
            </Text>
            <Text style={ModalStyle.textbutton} onClick={onDelete}>
              {nameDisable ? "탈퇴" : false}
            </Text>
          </Group>
        </List>
        {filteredPlayers.ining.length === 0
          ? false
          : filteredPlayers.ining.map(({ id, name }) => <List key={id}>{name}</List>)}
      </ListViewer>
    </ModalViewer>
  );
};

const Updater = ({ clickPlaygroundId, remotePlaygrounds, remoteOwnplayers, toggleModal }) => {
  let clickPlayer = getPlayerByPlaygroundId(remoteOwnplayers.readOwnplayers, clickPlaygroundId);

  if (clickPlayer.playground.status === false) {
    return (
      <Confirmer
        type="d"
        clickPlayer={clickPlayer}
        remoteOwnplayers={remoteOwnplayers}
        toggleModal={toggleModal}
      />
    );
  } else if (clickPlayer.position === "outing") {
    return (
      <Confirmer
        type="o"
        clickPlayer={clickPlayer}
        remoteOwnplayers={remoteOwnplayers}
        toggleModal={toggleModal}
      />
    );
  } else if (clickPlayer.position === "waiting") {
    return (
      <Confirmer
        type="w"
        clickPlayer={clickPlayer}
        remoteOwnplayers={remoteOwnplayers}
        toggleModal={toggleModal}
      />
    );
  } else if (clickPlayer.position === "ining") {
    return (
      <IningUpdater
        clickPlayer={clickPlayer}
        remotePlaygrounds={remotePlaygrounds}
        remoteOwnplayers={remoteOwnplayers}
        toggleModal={toggleModal}
      />
    );
  } else if (clickPlayer.position === "owning") {
    return (
      <OwningUpdater
        clickPlayer={clickPlayer}
        remotePlaygrounds={remotePlaygrounds}
        remoteOwnplayers={remoteOwnplayers}
        toggleModal={toggleModal}
      />
    );
  }
};

export default Updater;
