import React, { useState } from "react";

import { useMutation } from "@apollo/react-hooks";

import { InnerSpinner } from "../../Design/Spinner";
import { CloseButton, SaveButton, ModalViewer, Text, ModalStyle, useInput, Input } from "./Styler";
import { CREATE_PLAYER } from "../../../Hooks/PlaygroundHook";
import { getLeftMove, getTopMove, getPlaygroundByPlaygroundId } from "./Tool";

const Joiner = ({ clickPlaygroundId, remotePlaygrounds, remoteOwnplayers, toggleModal }) => {
  const [modalDisable, setModalDisable] = useState(false);
  const [createPlayerMutation] = useMutation(CREATE_PLAYER);
  const nameUseInput = useInput("");

  const onWaiting = async () => {
    try {
      if (nameUseInput.value === "") {
        alert("Playground 에서 사용할 닉네임 적어주세요.");
      } else {
        setModalDisable(true);
        const {
          data: { createPlayer },
        } = await createPlayerMutation({
          variables: {
            name: nameUseInput.value,
            playgroundId: clickPlaygroundId,
            position: "waiting",
          },
        });
        remoteOwnplayers.readOwnplayers.push(createPlayer);
        toggleModal();
      }
    } catch (error) {
      alert(error);
    }
  };

  let playground = getPlaygroundByPlaygroundId(
    remotePlaygrounds.searchPlaygrounds,
    clickPlaygroundId
  );
  return (
    <ModalViewer height="310px" left={getLeftMove(700)} top={getTopMove(310)}>
      {modalDisable ? (
        <InnerSpinner />
      ) : (
        <>
          <SaveButton onClick={onWaiting} />
          <CloseButton onClick={toggleModal} />
          <Input placeholder="Playground 에서 사용할 닉네임" {...nameUseInput} />
          <Text style={ModalStyle.title}>{playground.title}</Text>
          <Text style={ModalStyle.story}>{playground.story}</Text>
        </>
      )}
    </ModalViewer>
  );
};

export default Joiner;
