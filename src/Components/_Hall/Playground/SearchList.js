import React, { useState } from "react";

import Updater from "./Updater";
import Joiner from "./Joiner";
import { getPositionMark, getPlayerByPlaygroundId } from "./Tool";
import { GridViewer, Card, Text, CardStyle } from "./Styler";

const SearchList = ({ setRouteRefresh, remoteOwnplayers, remotePlaygrounds }) => {
  const [clickPlaygroundId, setClickPlaygroundId] = useState("");
  const [joinerModal, setJoinerModal] = useState(false);
  const [updaterModal, setUpdaterModal] = useState(false);

  const toggleJoinerModal = () => {
    joinerModal ? setJoinerModal(false) : setJoinerModal(true);
  };
  const toggleUpdaterModal = () => {
    setRouteRefresh(new Date());
    updaterModal ? setUpdaterModal(false) : setUpdaterModal(true);
  };
  const filter = (playgrounds) => {
    let filteredPlaygrounds = [];
    for (let playground of playgrounds) {
      if (playground.status === true) {
        let player = getPlayerByPlaygroundId(remoteOwnplayers.readOwnplayers, playground.id);
        if (player === undefined) {
          filteredPlaygrounds.push(playground);
        } else {
          filteredPlaygrounds.push({
            ...player.playground,
            ownplayerName: player.name,
            ownplayerPosition: player.position,
          });
        }
      }
    }
    return filteredPlaygrounds;
  };

  if (remotePlaygrounds === undefined) return false;

  let filteredPlaygrounds = filter(remotePlaygrounds.searchPlaygrounds);
  return (
    <GridViewer>
      {filteredPlaygrounds.length === 0
        ? false
        : filteredPlaygrounds.map(
            ({ id, code, color, status, title, story, ownplayerName, ownplayerPosition }) => (
              <Card
                key={id}
                color={color}
                onClick={() => {
                  ownplayerName === undefined ? toggleJoinerModal() : toggleUpdaterModal();
                  setClickPlaygroundId(id);
                }}
              >
                {getPositionMark(status, ownplayerPosition)}
                <Text style={CardStyle.code}>{code}</Text>
                <Text color="#ffffff" style={CardStyle.title}>
                  {title}
                </Text>
                <Text style={CardStyle.story}>{story}</Text>
                <Text style={CardStyle.name}>{ownplayerName}</Text>
              </Card>
            )
          )}
      {joinerModal && (
        <Joiner
          clickPlaygroundId={clickPlaygroundId}
          remotePlaygrounds={remotePlaygrounds}
          remoteOwnplayers={remoteOwnplayers}
          toggleModal={toggleJoinerModal}
        />
      )}
      {updaterModal && (
        <Updater
          clickPlaygroundId={clickPlaygroundId}
          remotePlaygrounds={remotePlaygrounds}
          remoteOwnplayers={remoteOwnplayers}
          toggleModal={toggleUpdaterModal}
        />
      )}
    </GridViewer>
  );
};

export default SearchList;
