import React, { useState } from "react";
import { useMutation } from "@apollo/react-hooks";

import { ModalViewer, SaveButton, CloseButton, Group, Color } from "./Styler";
import { getLeftMove, getTopMove } from "./Tool";
import { gradColors } from "./Tool";
import { InnerSpinner } from "../../Design/Spinner";
import { CREATE_PLAYER, CREATE_PLAYGROUND } from "../../../Hooks/PlaygroundHook";
import { useInput, Input, TextareaInput } from "./Styler";

const Creater = ({ remoteOwnplayers, toggleModal }) => {
  const [clickColor, setClickColor] = useState(0);
  const [modalDisable, setModalDisable] = useState(false);
  const [createPlayerMutation] = useMutation(CREATE_PLAYER);
  const [createPlaygroundMutation] = useMutation(CREATE_PLAYGROUND);
  const nameUseInput = useInput("");
  const titleUseInput = useInput("");
  const storyUseInput = useInput("");

  const onCreate = async () => {
    try {
      if (nameUseInput.value === "") {
        alert("새 Playground 에서 사용할 닉네임을 적어주세요.");
      } else if (titleUseInput.value === "") {
        alert("새 Playground 이름을 적어주세요.");
      } else if (storyUseInput.value === "") {
        alert("새 Playground 소개해주세요.");
      } else {
        setModalDisable(true);
        const {
          data: { createPlayground },
        } = await createPlaygroundMutation({
          variables: {
            color: gradColors[clickColor],
            title: titleUseInput.value,
            story: storyUseInput.value,
          },
        });
        const {
          data: { createPlayer },
        } = await createPlayerMutation({
          variables: {
            name: nameUseInput.value,
            playgroundId: createPlayground.id,
            position: "owning",
          },
        });
        remoteOwnplayers.readOwnplayers.push(createPlayer);
        toggleModal();
      }
    } catch (error) {
      alert(error);
    }
  };

  return (
    <ModalViewer height="310px" left={getLeftMove(700)} top={getTopMove(310)}>
      {modalDisable ? (
        <InnerSpinner />
      ) : (
        <>
          <SaveButton onClick={onCreate} />
          <CloseButton onClick={toggleModal} />
          <Input placeholder="새 Playground 에서 사용할 닉네임" {...nameUseInput} />
          <Input placeholder="새 Playground 이름" {...titleUseInput} />
          <TextareaInput placeholder="새 Playground 소개해주세요." {...storyUseInput} />
          <Group display="flex">
            {gradColors.map((color, index) => (
              <Color key={index} color={color} onClick={() => setClickColor(index)}>
                {index === clickColor ? "*" : false}
              </Color>
            ))}
          </Group>
        </>
      )}
    </ModalViewer>
  );
};

export default Creater;
