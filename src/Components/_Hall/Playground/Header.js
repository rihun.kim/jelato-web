import React, { useState } from "react";

import { Viewer, HeaderViewer, HeaderButton, useInput, SearchInput } from "./Styler";
import { InnerSpinner } from "../../Design/Spinner";
import Profile from "../Profile/Container";

const PlaygroundHeader = ({ loading, clickView, setClickView, searchPlaygroundsLazyQuery }) => {
  const [profileModal, setProfileModal] = useState(false);
  const keywordUseInput = useInput("");

  const toggleProfileModal = () => {
    profileModal ? setProfileModal(false) : setProfileModal(true);
  };
  const onSearch = () => {
    try {
      if (keywordUseInput.value !== "") {
        searchPlaygroundsLazyQuery({
          variables: { keyword: keywordUseInput.value },
        });
      }
    } catch (error) {
      alert(error);
    }
  };

  return (
    <Viewer height="80px" padding="21px 0 31px 0">
      {loading ? (
        <HeaderViewer>
          <InnerSpinner />
        </HeaderViewer>
      ) : (
        <HeaderViewer>
          <SearchInput onSearch={onSearch} setClickView={setClickView} {...keywordUseInput} />
          <HeaderButton
            clickView={clickView === "searchView" ? "searchView" : ""}
            type="s"
            onClick={() => {
              onSearch();
              setClickView("searchView");
            }}
          />
          <HeaderButton
            clickView={clickView === "ownView" ? "ownView" : ""}
            type="o"
            onClick={() => setClickView("ownView")}
          />
          <HeaderButton clickView={""} type="p" onClick={toggleProfileModal} />
        </HeaderViewer>
      )}
      {profileModal && <Profile toggleModal={toggleProfileModal} />}
    </Viewer>
  );
};

export default PlaygroundHeader;
