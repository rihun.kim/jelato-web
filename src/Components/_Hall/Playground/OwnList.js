import React, { useState } from "react";

import { GridViewer, Card, Text, CardStyle } from "./Styler";
import { getPositionMark } from "./Tool";
import Creater from "./Creater";
import Updater from "./Updater";

const OwnList = ({ setRouteRefresh, remoteOwnplayers }) => {
  const [clickPlaygroundId, setClickPlaygroundId] = useState("");
  const [createrModal, setCreaterModal] = useState(false);
  const [updaterModal, setUpdaterModal] = useState(false);

  const toggleCreaterModal = () => {
    setRouteRefresh(new Date());
    createrModal ? setCreaterModal(false) : setCreaterModal(true);
  };
  const toggleUpdaterModal = () => {
    setRouteRefresh(new Date());
    updaterModal ? setUpdaterModal(false) : setUpdaterModal(true);
  };

  let players = remoteOwnplayers.readOwnplayers;
  return (
    <GridViewer>
      <Card border="1px dashed #757575" onClick={toggleCreaterModal}>
        <Text fontSize="20px" float="right">
          +
        </Text>
        <Text style={CardStyle.title}>
          새로운
          <br />
          Playground 만들기
        </Text>
      </Card>
      {players.length === 0
        ? false
        : players.map(({ id, name, position, playground }) => (
            <Card
              key={id}
              color={playground.color}
              onClick={() => {
                toggleUpdaterModal();
                setClickPlaygroundId(playground.id);
              }}
            >
              {getPositionMark(playground.status, position)}
              <Text style={CardStyle.code}>{playground.code}</Text>
              <Text color="#ffffff" style={CardStyle.title}>
                {playground.title}
              </Text>
              <Text style={CardStyle.story}>{playground.story}</Text>
              <Text style={CardStyle.name}>{name}</Text>
            </Card>
          ))}
      {createrModal && (
        <Creater
          setRouteRefresh={setRouteRefresh}
          remoteOwnplayers={remoteOwnplayers}
          toggleModal={toggleCreaterModal}
        />
      )}
      {updaterModal && (
        <Updater
          setRouteRefresh={setRouteRefresh}
          clickPlaygroundId={clickPlaygroundId}
          remoteOwnplayers={remoteOwnplayers}
          toggleModal={toggleUpdaterModal}
        />
      )}
    </GridViewer>
  );
};

export default OwnList;
