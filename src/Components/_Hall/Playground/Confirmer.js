import React from "react";

import { useMutation } from "@apollo/react-hooks";

import { getLeftMove, getTopMove, deletePlayerByPlayerId } from "./Tool";
import { ModalViewer, ModalStyle, CloseButton, Text } from "./Styler";
import { DELETE_PLAYER } from "../../../Hooks/PlaygroundHook";

const Confirmer = ({ type, clickPlayer, remoteOwnplayers, toggleModal }) => {
  const [deletePlayerMutation] = useMutation(DELETE_PLAYER);

  const onDelete = () => {
    try {
      deletePlayerMutation({
        variables: {
          playerId: clickPlayer.id,
        },
      });
      deletePlayerByPlayerId(remoteOwnplayers.readOwnplayers, clickPlayer.id);
      toggleModal();
    } catch (error) {
      alert(error);
    }
  };

  if (type === "d") {
    return (
      <ModalViewer height="110px" left={getLeftMove(700)} top={getTopMove(110)}>
        <CloseButton onClick={onDelete} />
        <Text style={ModalStyle.confirm}>{clickPlayer.playground.title} 삭제되었습니다.</Text>
      </ModalViewer>
    );
  } else if (type === "o") {
    return (
      <ModalViewer height="110px" left={getLeftMove(700)} top={getTopMove(110)}>
        <CloseButton onClick={onDelete} />
        <Text style={ModalStyle.confirm}>{clickPlayer.name} 님은 멤버에서 아웃되셨습니다.</Text>
      </ModalViewer>
    );
  } else if (type === "w") {
    return (
      <ModalViewer height="110px" left={getLeftMove(700)} top={getTopMove(110)}>
        <CloseButton onClick={toggleModal} />
        <Text style={ModalStyle.confirm}>아직 멤버로 승인이 나지않은 상태입니다.</Text>
        <Text style={ModalStyle.textbutton} onClick={onDelete}>
          참여취소
        </Text>
      </ModalViewer>
    );
  }
};

export default Confirmer;
