import React from "react";

import { IoMdPlanet, IoMdRocket, IoIosPause, IoMdClose } from "react-icons/io";
import { GiFireBomb } from "react-icons/gi";

export const getPlayerByPlayerId = (players, playerId) => {
  return players[players.findIndex((player) => player.id === playerId)];
};

export const getPlayerByPlaygroundId = (players, playgroundId) => {
  return players[players.findIndex((player) => player.playground.id === playgroundId)];
};

export const getPlaygroundByPlaygroundId = (playgrounds, playgroundId) => {
  return playgrounds[playgrounds.findIndex((playground) => playground.id === playgroundId)];
};

export const deletePlayerByPlayerId = (players, playerId) => {
  for (let key in players) {
    if (playerId === players[key].id) {
      players.splice(key, 1);
      break;
    }
  }
};

export const deletePlaygroundByPlaygroundId = (playgrounds, playgroundId) => {
  for (let key in playgrounds) {
    if (playgroundId === playgrounds[key].id) {
      playgrounds.splice(key, 1);
      break;
    }
  }
};

export const getPositionMark = (playgroundStatus, playerPosition) => {
  const markStyle = { color: "#ffffff", float: "left", width: "25px", height: "25px" };
  if (playgroundStatus === false) {
    return <GiFireBomb style={markStyle} />;
  } else if (playerPosition === "owning") {
    return <IoMdPlanet style={markStyle} />;
  } else if (playerPosition === "waiting") {
    return <IoIosPause style={markStyle} />;
  } else if (playerPosition === "ining") {
    return <IoMdRocket style={markStyle} />;
  } else if (playerPosition === "outing") {
    return <IoMdClose style={markStyle} />;
  } else {
    return false;
  }
};

export const getLeftMove = (width) => {
  return window.innerWidth / 2 + 100 - width / 2 + "px";
};

export const getTopMove = (height) => {
  return window.innerHeight / 2 - height / 2 + "px";
};

export const gradColors = [
  "linear-gradient(to top, #e6261f 0%, #e6261f 100%)",
  "linear-gradient(to top, #eb7532 0%, #eb7532 100%)",
  "linear-gradient(to top, #f7d038 0%, #f7d038 100%)",
  "linear-gradient(to top, #a3e048 0%, #a3e048 100%)",
  "linear-gradient(to top, #49da9a 0%, #49da9a 100%)",
  "linear-gradient(to top, #34bbe6 0%, #34bbe6 100%)",
  "linear-gradient(to top, #4355db 0%, #4355db 100%)",
  "linear-gradient(to top, #d23be7 0%, #d23be7 100%)",
  "linear-gradient(to top, #050f2c 0%, #050f2c 100%)",
  "linear-gradient(-225deg, #A445B2 0%, #D41872 52%, #FF0066 100%)",
  "linear-gradient(20deg,#b8cbb8 0%,#b8cbb8 0%,#b465da 0%,#cf6cc9 33%,#ee609c 66%,#ee609c 100%)",
  "linear-gradient(to right, #ff758c 0%, #ff7eb3 100%)",
  "linear-gradient(120deg, #f6d365 0%, #fda085 100%)",
  "linear-gradient(to top, #50cc7f 0%, #f5d100 100%)",
  "linear-gradient(120deg, #84fab0 0%, #8fd3f4 100%)",
  "linear-gradient(-225deg, #D4FFEC 0%, #57F2CC 48%, #4596FB 100%)",
  "linear-gradient(-225deg, #2CD8D5 0%, #C5C1FF 56%, #FFBAC3 100%)",
  "linear-gradient(-225deg, #5D9FFF 0%, #B8DCFF 48%, #6BBBFF 100%)",
  "linear-gradient(-225deg, #22E1FF 0%, #1D8FE1 48%, #625EB1 100%)",
  "linear-gradient(-225deg, #2CD8D5 0%, #6B8DD6 48%, #8E37D7 100%)",
  "linear-gradient(-225deg, #3D4E81 0%, #5753C9 48%, #6E7FF3 100%)",
  "linear-gradient(-225deg, #231557 0%, #44107A 29%, #FF1361 67%, #FFF800 100%)",
];

export const getGradColorIndex = (color) => {
  return gradColors.findIndex((_color) => _color === color);
};
