import React, { useState } from "react";
import { useMutation } from "react-apollo";

import { UPDATE_SCHEDULE, DELETE_SCHEDULE } from "../../../Hooks/ScheduleHook";
import {
  ModalViewer,
  Group,
  MoreButton,
  LessButton,
  SaveButton,
  CloseButton,
  DeleteButton,
  Color,
  Text,
  DatetimeInput,
  TitleInput,
  useInput,
} from "./Styler";
import { colors, convertKoreaISOTime, getColorIndex, getLeftMove, getTopMove } from "./Tool";

const Updater = ({ clickDate, updatable, remoteSchedules, toggleModal }) => {
  const [clickColor, setClickColor] = useState(getColorIndex(clickDate.color));
  const [moreable, setMoreable] = useState(true);
  const [updateScheduleMutation] = useMutation(UPDATE_SCHEDULE);
  const [deleteScheduleMutation] = useMutation(DELETE_SCHEDULE);
  const startUseInput = useInput(convertKoreaISOTime(clickDate.start));
  const endUseInput = useInput(convertKoreaISOTime(clickDate.end));
  const titleUseInput = useInput(clickDate.title);

  const onSave = () => {
    try {
      if (
        startUseInput.value === "" ||
        endUseInput.value === "" ||
        new Date(startUseInput.value) - new Date(endUseInput.value) >= 0
      ) {
        alert("스케줄 시작날짜 종료날짜 확인해주세요.");
      } else if (titleUseInput.value === "") {
        alert("스케줄 내용 적어주세요.");
      } else {
        updateScheduleMutation({
          variables: {
            scheduleId: clickDate.id,
            start: startUseInput.value,
            end: endUseInput.value,
            title: titleUseInput.value,
            color: colors[clickColor],
          },
        });
        schedules.splice(schedules.indexOf(clickDate), 1, {
          ...clickDate,
          start: new Date(startUseInput.value),
          end: new Date(endUseInput.value),
          title: titleUseInput.value,
          color: colors[clickColor],
        });
      }
      toggleModal();
    } catch (error) {
      alert(error);
    }
  };
  const onDelete = () => {
    try {
      deleteScheduleMutation({
        variables: {
          scheduleId: clickDate.id,
        },
      });
      schedules.splice(schedules.indexOf(clickDate), 1);
      toggleModal();
    } catch (error) {
      alert(error);
    }
  };

  let schedules = remoteSchedules.readSchedules;

  return (
    <ModalViewer height="260px" left={getLeftMove(700)} top={getTopMove(310)}>
      {updatable ? (
        moreable ? (
          <>
            <MoreButton onClick={() => setMoreable(false)} />
            <SaveButton onClick={onSave} />
            <CloseButton onClick={toggleModal} />
          </>
        ) : (
          <>
            <LessButton onClick={() => setMoreable(true)} />
            <DeleteButton onClick={onDelete} />
          </>
        )
      ) : (
        <CloseButton onClick={toggleModal} />
      )}
      <DatetimeInput abled={updatable} {...startUseInput} />
      <DatetimeInput abled={updatable} {...endUseInput} />
      <TitleInput abled={updatable} {...titleUseInput} />
      <Group display="flex">
        {colors.map((color, index) => (
          <Color key={index} color={color} onClick={() => updatable && setClickColor(index)}>
            {index === clickColor ? "*" : false}
          </Color>
        ))}
        <Text>작성자 : {clickDate.player.name}</Text>
      </Group>
    </ModalViewer>
  );
};

export default Updater;
