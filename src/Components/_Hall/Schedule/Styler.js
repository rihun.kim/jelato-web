import React, { useState } from "react";
import styled from "styled-components";
import { IoMdClose, IoMdRadioButtonOff } from "react-icons/io";
import { MdExpandLess, MdExpandMore } from "react-icons/md";
import { GiFireBomb } from "react-icons/gi";

//
export const Viewer = styled.div`
  width: ${(props) => props.width};
  height: 100vh;

  align-items: center;
  display: flex;
  justify-content: center;
  left: ${(props) => props.left};
  position: ${(props) => props.position};
`;

export const CalendarViewer = styled.div`
  width: 95%;
  height: 95%;
`;

export const ModalViewer = styled.div`
  width: 700px;
  height: ${(props) => props.height};

  background-color: #ffffff;
  border-radius: 3px;
  box-shadow: 0 0 4px rgba(0, 0, 0, 0.14), 0 4px 8px rgba(0, 0, 0, 0.28);
  left: ${(props) => props.left};
  padding: 40px 25px 20px 25px;
  position: fixed;
  top: ${(props) => props.top};
  z-index: 4;
`;

//
export const useInput = (prefilled) => {
  const [value, setValue] = useState(prefilled);

  const onChange = (e) => {
    setValue(e.target.value);
  };

  return { onChange, value };
};

const InputViewer = styled.input`
  width: 50%;
  height: 35px;

  border: 0;
  border-bottom: 1px solid #e0e0e0;
  font-size: 14px;
  margin: 0 0 10px 0;
  outline: none;
  padding: 0 15px 0 15px;
`;

const TextareaViewer = styled.textarea`
  width: 100%;
  height: 110px;

  border: 0;
  border-bottom: 1px solid #e0e0e0;
  font-size: 14px;
  margin: 0 0 10px 0;
  outline: none;
  padding: 10px 15px 10px 15px;
  resize: none;
  white-space: pre-wrap;
`;

export const DatetimeInput = ({
  abled = true,
  onChange,
  placeholder,
  type = "datetime-local",
  value,
}) => (
  <InputViewer
    defaultValue={value}
    disabled={!abled}
    onChange={onChange}
    placeholder={placeholder}
    type={type}
  />
);

export const TitleInput = ({ abled = true, onChange, placeholder, type = "text", value }) => (
  <TextareaViewer
    defaultValue={value}
    disabled={!abled}
    onChange={onChange}
    placeholder={placeholder}
    type={type}
  />
);

//
export const Group = styled.div`
  display: ${(props) => props.display};
`;

export const Color = styled.div`
  width: 20px;
  height: 25px;
  background-color: ${(props) => props.color};
  border-radius: 3px;
  color: #ffffff;
  cursor: pointer;
  font-size: 14px;
  margin: 0 auto 0 auto;
  text-align: center;
`;

export const Text = styled.div`
  width: 200px;
  color: #777777;
  float: right;
  font-size: 12px;
  font-weight: 200;
  margin: auto 0 auto auto;
  overflow: hidden;
  text-align: right;
`;

//
export const MoreButton = ({ onClick }) => {
  return (
    <MdExpandMore
      style={{
        width: "15px",
        height: "15px",
        color: "#a5a2a2",
        cursor: "pointer",
        position: "absolute",
        right: 60,
        top: 20,
        zIndex: 3,
      }}
      onClick={onClick}
    />
  );
};

export const LessButton = ({ onClick }) => {
  return (
    <MdExpandLess
      style={{
        width: "15px",
        height: "15px",
        color: "#a5a2a2",
        cursor: "pointer",
        position: "absolute",
        right: 60,
        top: 20,
        zIndex: 3,
      }}
      onClick={onClick}
    />
  );
};

export const SaveButton = ({ onClick }) => {
  return (
    <IoMdRadioButtonOff
      title="저장하기"
      style={{
        width: "14px",
        height: "14px",
        color: "#a5a2a2",
        cursor: "pointer",
        position: "absolute",
        right: 40,
        top: 20,
        zIndex: 3,
      }}
      onClick={onClick}
    />
  );
};

export const CloseButton = ({ onClick }) => {
  return (
    <IoMdClose
      title="닫기"
      style={{
        width: "16px",
        height: "16px",
        color: "#a5a2a2",
        cursor: "pointer",
        position: "absolute",
        right: 20,
        top: 20,
        zIndex: 3,
      }}
      onClick={onClick}
    />
  );
};

export const DeleteButton = ({ onClick }) => {
  return (
    <GiFireBomb
      title="삭제하기"
      style={{
        width: "15px",
        height: "15px",
        color: "#a5a2a2",
        cursor: "pointer",
        position: "absolute",
        right: 20,
        top: 20,
        zIndex: 3,
      }}
      onClick={onClick}
    />
  );
};
