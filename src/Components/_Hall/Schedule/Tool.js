export const isUpdatable = (clickPlayerId, ownplayerId, ownplayerPosition) => {
  if (clickPlayerId === ownplayerId || ownplayerPosition === "owning") {
    return true;
  } else {
    return false;
  }
};

export const getLeftMove = (width) => {
  return window.innerWidth / 2 + 100 - width / 2 + "px";
};

export const getTopMove = (height) => {
  return window.innerHeight / 2 - height / 2 + "px";
};

export const colors = [
  "#9C26B0",
  "#673AB7",
  "#3E51B5",
  "#2396F3",
  "#02A9F4",
  "#00BCD4",
  "#019688",
  "#4CAF51",
  "#8BC34A",
  "#CDDC39",
  "#FFEB3A",
  "#FFC106",
  "#FF9800",
  "#FF5721",
  "#795547",
  "#9E9E9E",
  "#000000",
];

export const getColorIndex = (color) => {
  return colors.findIndex((_color) => _color === color);
};

export const convertKoreaISOTime = (datetime) => {
  return new Date(datetime - -32400000).toISOString().slice(0, -1);
};
