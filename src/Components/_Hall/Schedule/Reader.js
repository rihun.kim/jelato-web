import React, { useState } from "react";
import { useMutation } from "react-apollo";
import moment from "moment";
import { Calendar, momentLocalizer } from "react-big-calendar";
import "react-big-calendar/lib/css/react-big-calendar.css";
import withDragAndDrop from "react-big-calendar/lib/addons/dragAndDrop";
import "react-big-calendar/lib/addons/dragAndDrop/styles.scss";

import Creater from "./Creater";
import Updater from "./Updater";
import { UPDATE_SCHEDULE } from "../../../Hooks/ScheduleHook";
import { CalendarViewer } from "./Styler";
import { isUpdatable } from "./Tool";

const ScheduleReader = ({ player, remoteSchedules }) => {
  const [clickDate, setClickDate] = useState();
  const [clickView, setClickView] = useState();
  const [updatable, setUpdatable] = useState();
  const [createrModal, setCreaterModal] = useState(false);
  const [updaterModal, setUpdaterModal] = useState(false);
  const [updateScheduleMutation] = useMutation(UPDATE_SCHEDULE);
  const DragAndDropCalendar = withDragAndDrop(Calendar);

  const toggleCreaterModal = (e) => {
    if (e !== undefined) setClickDate(e);
    createrModal ? setCreaterModal(false) : setCreaterModal(true);
  };
  const toggleUpdaterModal = (e) => {
    if (e !== undefined && e.player !== undefined) {
      setUpdatable(isUpdatable(e.player.id, player.id, player.position));
      setClickDate(e);
    }
    updaterModal ? setUpdaterModal(false) : setUpdaterModal(true);
  };
  const moveEvent = ({ event, start, end }) => {
    schedules.splice(schedules.indexOf(event), 1, { ...event, start, end });
    updateScheduleMutation({
      variables: {
        scheduleId: event.id,
        start: start,
        end: end,
      },
    });
  };

  let schedules = remoteSchedules.readSchedules;
  return (
    <CalendarViewer>
      <DragAndDropCalendar
        selectable
        resizable
        events={[...schedules]}
        eventPropGetter={(event) => {
          return {
            style: {
              backgroundColor: event.color,
            },
          };
        }}
        localizer={momentLocalizer(moment)}
        style={{ cursor: "pointer", fontSize: "12px" }}
        views={["month", "week"]}
        view={clickView}
        onView={(e) => setClickView(e)}
        onEventDrop={moveEvent}
        onEventResize={moveEvent}
        onSelectEvent={(e) => toggleUpdaterModal(e)}
        onSelectSlot={(e) => toggleCreaterModal(e)}
      />
      {createrModal && (
        <Creater
          clickDate={clickDate}
          player={player}
          remoteSchedules={remoteSchedules}
          toggleModal={toggleCreaterModal}
        />
      )}
      {updaterModal && (
        <Updater
          clickDate={clickDate}
          updatable={updatable}
          remoteSchedules={remoteSchedules}
          toggleModal={toggleUpdaterModal}
        />
      )}
    </CalendarViewer>
  );
};

export default ScheduleReader;
