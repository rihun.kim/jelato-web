import React, { useMemo } from "react";
import { useQuery } from "@apollo/react-hooks";

import Reader from "./Reader";
import { Viewer } from "./Styler";
import { READ_SCHEDULES } from "../../../Hooks/ScheduleHook";
import { Spinner } from "../../Design/Spinner";

const Container = ({ match, remoteOwnplayers }) => {
  let player = remoteOwnplayers.readOwnplayers[Number(match.params.index)];

  const { loading, data: remoteSchedules } = useQuery(READ_SCHEDULES, {
    variables: {
      playgroundId: player.playground.id,
    },
    fetchPolicy: "no-cache",
  });

  useMemo(() => {
    if (remoteSchedules !== undefined) {
      for (const schedule of remoteSchedules.readSchedules) {
        schedule.start = new Date(schedule.start);
        schedule.end = new Date(schedule.end);
      }
    }
  }, [remoteSchedules]);

  if (loading) return <Spinner windowLeft={200} windowWidth={window.innerWidth - 200} />;
  return (
    <Viewer width={window.innerWidth - 200 + "px"} left="200px" position="fixed">
      <Reader player={player} remoteSchedules={remoteSchedules} />
    </Viewer>
  );
};

export default Container;
