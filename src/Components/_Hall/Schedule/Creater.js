import React, { useState } from "react";
import { useMutation } from "react-apollo";

import { CREATE_SCHEDULE } from "../../../Hooks/ScheduleHook";
import { InnerSpinner } from "../../Design/Spinner";
import {
  ModalViewer,
  Group,
  SaveButton,
  CloseButton,
  Color,
  DatetimeInput,
  TitleInput,
  useInput,
  Text,
} from "./Styler";
import { colors, convertKoreaISOTime, getLeftMove, getTopMove } from "./Tool";

const Creater = ({ clickDate, player, remoteSchedules, toggleModal }) => {
  const [modalDisable, setModalDisable] = useState(false);
  const [clickColor, setClickColor] = useState(0);
  const [createScheduleMutation] = useMutation(CREATE_SCHEDULE);
  const startUseInput = useInput(convertKoreaISOTime(clickDate.start));
  const endUseInput = useInput(convertKoreaISOTime(clickDate.end));
  const titleUseInput = useInput("");

  const onCreate = async () => {
    try {
      if (
        startUseInput.value === "" ||
        endUseInput.value === "" ||
        new Date(startUseInput.value) - new Date(endUseInput.value) >= 0
      ) {
        alert("스케줄 시작날짜 종료날짜 확인해주세요.");
      } else if (titleUseInput.value === "") {
        alert("스케줄 내용 적어주세요.");
      } else {
        setModalDisable(true);
        const {
          data: { createSchedule },
        } = await createScheduleMutation({
          variables: {
            playerId: player.id,
            playgroundId: player.playground.id,
            start: startUseInput.value,
            end: endUseInput.value,
            title: titleUseInput.value,
            color: colors[clickColor],
          },
        });
        createSchedule.start = new Date(createSchedule.start);
        createSchedule.end = new Date(createSchedule.end);
        schedules.push(createSchedule);
        toggleModal();
      }
    } catch (error) {
      alert(error);
    }
  };

  let schedules = remoteSchedules.readSchedules;

  return (
    <ModalViewer height="260px" left={getLeftMove(700)} top={getTopMove(310)}>
      {modalDisable ? (
        <InnerSpinner />
      ) : (
        <>
          <SaveButton onClick={onCreate} />
          <CloseButton onClick={toggleModal} />
          <DatetimeInput {...startUseInput} />
          <DatetimeInput {...endUseInput} />
          <TitleInput placeholder={"스케줄 적어주세요."} {...titleUseInput} />
          <Group display="flex">
            {colors.map((color, index) => (
              <Color key={index} color={color} onClick={() => setClickColor(index)}>
                {index === clickColor ? "*" : false}
              </Color>
            ))}
            <Text>작성자 : {player.name}</Text>
          </Group>
        </>
      )}
    </ModalViewer>
  );
};

export default Creater;
