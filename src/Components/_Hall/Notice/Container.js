import React from "react";
import { useQuery } from "@apollo/react-hooks";

import { Viewer } from "./Styler";
import { READ_NOTICES } from "../../../Hooks/NoticeHook";
import { Spinner } from "../../Design/Spinner";
import Reader from "./Reader";

const Container = ({ match, remoteOwnplayers }) => {
  let player = remoteOwnplayers.readOwnplayers[Number(match.params.index)];

  const { loading, fetchMore, data: remoteNotices } = useQuery(READ_NOTICES, {
    variables: {
      playgroundId: player.playground.id,
    },
    fetchPolicy: "no-cache",
  });

  if (loading) return <Spinner windowLeft={200} windowWidth={window.innerWidth - 200} />;
  return (
    <Viewer width={window.innerWidth - 200 + "px"}>
      <Reader player={player} fetchMore={fetchMore} remoteNotices={remoteNotices} />
    </Viewer>
  );
};

export default Container;
