export const convertTime = (date) => {
  const time = new Date(date);

  let hh = time.getHours();
  hh = hh < 10 ? "0" + hh : hh;

  let mm = time.getMinutes();
  mm = mm < 10 ? "0" + mm : mm;

  let ss = time.getSeconds();
  ss = ss < 10 ? "0" + ss : ss;

  const convertedTime =
    "" +
    time.getFullYear() +
    "." +
    (time.getMonth() + 1) +
    "." +
    time.getDate() +
    " / " +
    hh +
    ":" +
    mm +
    ":" +
    ss;

  return convertedTime;
};

export const getLeftMove = (width) => {
  return window.innerWidth / 2 + 100 - width / 2 + "px";
};

export const getTopMove = (height) => {
  return window.innerHeight / 2 - height / 2 + "px";
};

export const isUpdatable = (clickPlayerId, ownplayerId, ownplayerPosition) => {
  if (clickPlayerId === ownplayerId || ownplayerPosition === "owning") {
    return true;
  } else {
    return false;
  }
};

export const getNoticeByNoticeId = (notices, noticeId) => {
  return notices[notices.findIndex((notice) => notice.id === noticeId)];
};
