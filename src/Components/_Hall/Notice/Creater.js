import React, { useState } from "react";
import { useMutation } from "@apollo/react-hooks";

import { InnerSpinner } from "../../Design/Spinner";
import { ModalViewer, TitleInput, StoryInput, useInput, SaveButton, CloseButton } from "./Styler";
import { getLeftMove, getTopMove } from "./Tool";
import { CREATE_NOTICE } from "../../../Hooks/NoticeHook";

const Creater = ({ player, remoteNotices, toggleModal }) => {
  const [modalDisable, setModalDisable] = useState(false);
  const [createNoticeMutation] = useMutation(CREATE_NOTICE);
  const titleUseInput = useInput("");
  const storyUseInput = useInput("");

  const onCreate = async () => {
    try {
      if (titleUseInput.value === "") {
        alert("새 공지사항 제목을 적어주세요.");
      } else if (storyUseInput.value === "") {
        alert("새 공지사항 내용을 적어주세요.");
      } else {
        setModalDisable(true);
        const {
          data: { createNotice },
        } = await createNoticeMutation({
          variables: {
            playerId: player.id,
            playgroundId: player.playground.id,
            title: titleUseInput.value,
            story: storyUseInput.value,
          },
        });
        notices.unshift(createNotice);
        toggleModal();
      }
    } catch (error) {
      alert(error);
    }
  };

  let notices = remoteNotices.readNotices;
  return (
    <ModalViewer left={getLeftMove(700)} top={getTopMove(500)}>
      {modalDisable ? (
        <InnerSpinner />
      ) : (
        <>
          <SaveButton onClick={onCreate} />
          <CloseButton onClick={toggleModal} />
          <TitleInput placeholder={"새 공지사항 제목"} {...titleUseInput} />
          <StoryInput placeholder={"새 공지사항 내용"} {...storyUseInput} />
        </>
      )}
    </ModalViewer>
  );
};

export default Creater;
