import React, { useState } from "react";
import { useMutation } from "@apollo/react-hooks";

import {
  ModalViewer,
  TitleInput,
  StoryInput,
  useInput,
  SaveButton,
  DeleteButton,
  MoreButton,
  CloseButton,
  LessButton,
} from "./Styler";
import { getLeftMove, getTopMove } from "./Tool";
import { UPDATE_NOTICE, DELETE_NOTICE } from "../../../Hooks/NoticeHook";

const Updater = ({ clickNotice, remoteNotices, updatable, toggleModal }) => {
  const [moreable, setMoreable] = useState(true);
  const [updateNoticeMutation] = useMutation(UPDATE_NOTICE);
  const [deleteNoticeMutation] = useMutation(DELETE_NOTICE);
  const titleUseInput = useInput(clickNotice.title);
  const storyUseInput = useInput(clickNotice.story);

  const onSave = () => {
    try {
      if (titleUseInput.value === "") {
        alert("공지사항 제목 적어주세요.");
      } else if (storyUseInput.value === "") {
        alert("공지사항 내용 적어주세요.");
      } else {
        updateNoticeMutation({
          variables: {
            noticeId: clickNotice.id,
            title: titleUseInput.value,
            story: storyUseInput.value,
          },
        });
        clickNotice.title = titleUseInput.value;
        clickNotice.story = storyUseInput.value;
        toggleModal();
      }
    } catch (error) {
      alert(error);
    }
  };
  const onDelete = () => {
    try {
      deleteNoticeMutation({
        variables: {
          noticeId: clickNotice.id,
        },
      });
      notices.splice(notices.indexOf(clickNotice), 1);
      toggleModal();
    } catch (error) {
      alert(error);
    }
  };

  let notices = remoteNotices.readNotices;

  return (
    <ModalViewer left={getLeftMove(700)} top={getTopMove(500)}>
      {updatable ? (
        moreable ? (
          <>
            <MoreButton onClick={() => setMoreable(false)} />
            <SaveButton onClick={onSave} />
            <CloseButton onClick={toggleModal} />
          </>
        ) : (
          <>
            <LessButton onClick={() => setMoreable(true)} />
            <DeleteButton onClick={onDelete} />
          </>
        )
      ) : (
        <CloseButton onClick={toggleModal} />
      )}
      <TitleInput {...titleUseInput} />
      <StoryInput {...storyUseInput} />
    </ModalViewer>
  );
};

export default Updater;
