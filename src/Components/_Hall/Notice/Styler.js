import React, { useState } from "react";
import styled from "styled-components";
import { IoMdClose, IoMdRadioButtonOff } from "react-icons/io";
import { MdExpandLess, MdExpandMore } from "react-icons/md";
import { GiFireBomb } from "react-icons/gi";

//
export const Viewer = styled.div`
  width: ${(props) => props.width};
  height: 100vh;

  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;

  left: 200px;
  position: fixed;
  overflow-y: auto;
`;

export const NoticeViewer = styled.div`
  width: 700px;
  height: ${(props) => props.height};

  display: flex;
  justify-content: center;
  flex-direction: column;
`;

export const ModalViewer = styled.div`
  width: 700px;
  height: 500px;

  background-color: #ffffff;
  border-radius: 3px;
  box-shadow: 0 0 4px rgba(0, 0, 0, 0.14), 0 4px 8px rgba(0, 0, 0, 0.28);
  left: ${(props) => props.left};
  padding: 40px 25px 20px 25px;
  position: fixed;
  top: ${(props) => props.top};
  z-index: 4;
`;

//
export const Notice = styled.div`
  width: 660px;

  flex-shrink: 0;
  background-color: #ffffff;
  border: ${(props) => props.border};
  border-radius: 3px;
  box-shadow: 1px 3px 4px rgba(0, 0, 0, 0.19), 0 1px 0 rgba(0, 0, 0, 0.23);
  cursor: pointer;
  margin: 10px 0 10px 0;
  padding: 20px 20px 20px 20px;
`;

export const Group = styled.div`
  max-height: ${(props) => props.maxHeight};

  margin: ${(props) => props.margin};
`;

export const Text = styled.div`
  color: ${(props) => props.color};
  float: ${(props) => props.float};
  font-size: ${(props) => props.fontSize};
  font-weight: ${(props) => props.fontWeight};
  margin: ${(props) => props.margin};
  overflow: ${(props) => props.overflow};
`;

//
export const FetchButton = ({ onClick }) => {
  return (
    <div style={{ width: "15px", height: "15px", margin: "auto auto 10px auto" }}>
      <MdExpandMore
        style={{
          width: "15px",
          height: "15px",
          color: "#a5a2a2",
          cursor: "pointer",
        }}
        onClick={onClick}
      />
    </div>
  );
};

export const MoreButton = ({ onClick }) => {
  return (
    <MdExpandMore
      style={{
        width: "15px",
        height: "15px",
        color: "#a5a2a2",
        cursor: "pointer",
        position: "absolute",
        right: 60,
        top: 20,
        zIndex: 3,
      }}
      onClick={onClick}
    />
  );
};

export const LessButton = ({ onClick }) => {
  return (
    <MdExpandLess
      style={{
        width: "15px",
        height: "15px",
        color: "#a5a2a2",
        cursor: "pointer",
        position: "absolute",
        right: 60,
        top: 20,
        zIndex: 3,
      }}
      onClick={onClick}
    />
  );
};

export const SaveButton = ({ onClick }) => {
  return (
    <IoMdRadioButtonOff
      title="저장하기"
      style={{
        width: "14px",
        height: "14px",
        color: "#a5a2a2",
        cursor: "pointer",
        position: "absolute",
        right: 40,
        top: 20,
        zIndex: 3,
      }}
      onClick={onClick}
    />
  );
};

export const CloseButton = ({ onClick }) => {
  return (
    <IoMdClose
      title="닫기"
      style={{
        width: "16px",
        height: "16px",
        color: "#a5a2a2",
        cursor: "pointer",
        position: "absolute",
        right: 20,
        top: 20,
        zIndex: 3,
      }}
      onClick={onClick}
    />
  );
};

export const DeleteButton = ({ onClick }) => {
  return (
    <GiFireBomb
      title="삭제하기"
      style={{
        width: "15px",
        height: "15px",
        color: "#a5a2a2",
        cursor: "pointer",
        position: "absolute",
        right: 20,
        top: 20,
        zIndex: 3,
      }}
      onClick={onClick}
    />
  );
};

//
export const useInput = (prefilled) => {
  const [value, setValue] = useState(prefilled);

  const onChange = (e) => {
    setValue(e.target.value);
  };

  return { onChange, value };
};

const InputViewer = styled.input`
  width: 100%;
  height: 35px;

  border: 0;
  border-bottom: 1px solid #e0e0e0;
  font-size: 14px;
  margin: 0 0 10px 0;
  outline: none;
  padding: 0 15px 0 15px;
`;

const TextareaViewer = styled.textarea`
  width: 100%;
  height: 380px;

  border: 0;
  border-bottom: 1px solid #e0e0e0;
  font-size: 14px;
  outline: none;
  padding: 10px 15px 10px 15px;
  resize: none;
  white-space: pre-wrap;
`;

export const TitleInput = ({ onChange, placeholder, type = "text", value }) => (
  <InputViewer defaultValue={value} onChange={onChange} placeholder={placeholder} type={type} />
);

export const StoryInput = ({ onChange, placeholder, type = "text", value }) => (
  <TextareaViewer defaultValue={value} onChange={onChange} placeholder={placeholder} type={type} />
);

//
export const NoticeStyle = {
  title: {
    color: "#000000",
    fontSize: "25px",
    fontWeight: "600",
    margin: "0 0 5px 0",
  },
  story: {
    color: "#000000",
    fontSize: "13px",
    whiteSpace: "pre-wrap",
  },
  name: {
    color: "#777777",
    fontSize: "12px",
    fontWeight: "70",
    margin: "0 0 30px 0",
  },
  guide: {
    color: "#000000",
    fontSize: "15px",
    fontWeight: "600",
  },
};
