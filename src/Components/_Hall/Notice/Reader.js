import React, { useState } from "react";

import { convertTime, isUpdatable } from "./Tool";
import { NoticeViewer, Notice, NoticeStyle, Text, Group, FetchButton } from "./Styler";
import { InnerSpinner } from "../../Design/Spinner";
import Creater from "./Creater";
import Updater from "./Updater";

const Reader = ({ player, fetchMore, remoteNotices }) => {
  const [, setRefresh] = useState();
  const [clickNotice, setClickNotice] = useState();
  const [fetchable, setFetchable] = useState(true);
  const [fetching, setFetching] = useState(false);
  const [updatable, setUpdatable] = useState();
  const [createrModal, setCreaterModal] = useState(false);
  const [updaterModal, setUpdaterModal] = useState(false);

  const toggleCreaterModal = () => {
    createrModal ? setCreaterModal(false) : setCreaterModal(true);
  };
  const toggleUpdaterModal = () => {
    updaterModal ? setUpdaterModal(false) : setUpdaterModal(true);
  };
  const onFetch = () => {
    setFetching(true);
    fetchMore({
      variables: {
        playgroundId: player.playground.id,
        noticeId: notices[notices.length - 1].id,
      },
      updateQuery: (_, { fetchMoreResult }) => {
        if (fetchMoreResult.readNotices.length === 0) {
          setFetchable(false);
        } else {
          notices.push(...fetchMoreResult.readNotices);
        }
      },
    }).then(() => {
      setFetching(false);
      setRefresh(new Date());
    });
  };

  let notices = remoteNotices.readNotices;

  return (
    <NoticeViewer height={window.innerHeight + "px"}>
      <Group maxHeight="100%" margin="auto auto auto auto">
        <Notice border="1px dashed #757575" onClick={toggleCreaterModal}>
          <Text style={NoticeStyle.guide} float="right">
            +
          </Text>
          <Text style={NoticeStyle.guide}>새로운 공지사항 만들기</Text>
        </Notice>
        {notices.map((notice) => (
          <Notice
            key={notice.id}
            onClick={() => {
              setUpdatable(isUpdatable(notice.player.id, player.id, player.position));
              setClickNotice(notice);
              toggleUpdaterModal();
            }}
          >
            <Text style={NoticeStyle.title}>{notice.title}</Text>
            <Text style={NoticeStyle.name}>
              작성자 : {notice.player.name} | 작성일 : {convertTime(notice.createdAt)}
            </Text>
            <Text style={NoticeStyle.story}>{notice.story}</Text>
          </Notice>
        ))}
        {fetchable && notices.length !== 0 ? (
          fetching ? (
            <InnerSpinner height="18px" margin="20px 0 20px 0" />
          ) : (
            <FetchButton onClick={onFetch} />
          )
        ) : (
          false
        )}
      </Group>
      {createrModal && (
        <Creater player={player} remoteNotices={remoteNotices} toggleModal={toggleCreaterModal} />
      )}
      {updaterModal && (
        <Updater
          updatable={updatable}
          clickNotice={clickNotice}
          remoteNotices={remoteNotices}
          toggleModal={toggleUpdaterModal}
        />
      )}
    </NoticeViewer>
  );
};

export default Reader;
