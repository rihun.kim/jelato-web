import { useQuery, useSubscription } from "@apollo/react-hooks";

import {
  READ_CHATS,
  READ_VHATS,
  READ_MESSAGES,
  SUBSCRIBE_MESSAGES,
  SUBSCRIBE_CHATS,
  SUBSCRIBE_VHATS,
} from "./Gql";

export const UseQueryChats = ({ iplayer }) => {
  const { loading, data } = useQuery(READ_CHATS, {
    variables: { playerId: iplayer.id },
    fetchPolicy: "no-cache",
  });

  if (loading || data === undefined) {
    return false;
  }

  return data;
};

export const UseQueryVhats = ({ iplayer }) => {
  const { loading, data } = useQuery(READ_VHATS, {
    variables: { playerId: iplayer.id },
    fetchPolicy: "no-cache",
  });

  if (loading || data === undefined) {
    return false;
  }

  return data;
};

export const UseSubsChats = ({ iplayer }) => {
  const { data } = useSubscription(SUBSCRIBE_CHATS, {
    variables: { playerId: iplayer.id },
  });

  if (data === undefined) {
    return false;
  }

  return data;
};

export const UseSubsVhats = ({ iplayer }) => {
  const { data } = useSubscription(SUBSCRIBE_VHATS, {
    variables: { playerId: iplayer.id },
  });

  if (data === undefined) {
    return false;
  }

  return data;
};

export const UseSubsMessages = ({ iplayer }) => {
  const { data } = useSubscription(SUBSCRIBE_MESSAGES, {
    variables: { playerId: iplayer.id },
  });

  if (data === undefined) {
    return false;
  }

  return data;
};

export const UseFetchMessages = ({ messages, clickChat }) => {
  const { fetchMore } = useQuery(READ_MESSAGES, {
    variables: {
      chatId: clickChat,
      messageId: messages[0] ? messages[0].id : undefined,
    },
  });

  return fetchMore;
};
