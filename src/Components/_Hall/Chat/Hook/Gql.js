import { gql } from "apollo-boost";

export const READ_CHATS = gql`
  query readChats($playerId: String!) {
    readChats(playerId: $playerId) {
      id

      title
      count
      owner
      players {
        id
        name
      }
      messages {
        id
        from {
          id
          name
        }
        text
        type
        createdAt
      }
      createdAt
    }
  }
`;

export const READ_VHATS = gql`
  query readVhats($playerId: String!) {
    readVhats(playerId: $playerId) {
      id

      chatid
      chat {
        id
        title
        players {
          id
          name
        }
      }
      players {
        id
        name
      }
    }
  }
`;

export const CREATE_CHAT = gql`
  mutation createChat($playgroundId: String!, $playerIds: [String!]) {
    createChat(playgroundId: $playgroundId, playerIds: $playerIds) {
      id
      title
      owner
      players {
        id
        name
      }
      messages {
        id
        chat {
          id
        }
        from {
          id
          name
        }
        text
        type
        createdAt
      }
      createdAt
    }
  }
`;

export const UPDATE_CHAT = gql`
  mutation updateChat($action: String!, $chatId: String!, $title: String, $playerId: String) {
    updateChat(action: $action, chatId: $chatId, title: $title, playerId: $playerId) {
      id
      title
      count
      owner
      players {
        id
        name
      }
      createdAt
    }
  }
`;

export const UPDATE_VHAT = gql`
  mutation updateVhat($vhatId: String!, $playerId: String) {
    updateVhat(vhatId: $vhatId, playerId: $playerId) {
      id
    }
  }
`;

export const DELETE_CHAT = gql`
  mutation deleteChat($chatId: String!) {
    deleteChat(chatId: $chatId)
  }
`;

export const SUBSCRIBE_VHATS = gql`
  subscription subscribeVhats($playerId: String!) {
    subscribeVhats(playerId: $playerId) {
      mutation
      node {
        id

        chatid
        chat {
          id
          title
          players {
            id
            name
          }
        }
        players {
          id
          name
        }
        createdAt
      }
      updatedFields
      previousValues {
        id
      }
    }
  }
`;

export const SUBSCRIBE_CHATS = gql`
  subscription subscribeChats($playerId: String!) {
    subscribeChats(playerId: $playerId) {
      mutation
      node {
        id

        count
        title
        owner
        players {
          id
          name
        }
        messages {
          id
          chat {
            id
          }
          from {
            id
            name
          }
          text
          type
          createdAt
        }
        createdAt
      }
      updatedFields
      previousValues {
        id
      }
    }
  }
`;

export const SUBSCRIBE_MESSAGES = gql`
  subscription subscribeMessages($playerId: String!) {
    subscribeMessages(playerId: $playerId) {
      id
      chat {
        id
      }
      from {
        id
        name
      }
      text
      type
      createdAt
    }
  }
`;

export const CREATE_MESSAGE = gql`
  mutation createMessage($chatId: String!, $playerId: String!, $text: String!, $type: String) {
    createMessage(chatId: $chatId, playerId: $playerId, text: $text, type: $type) {
      id
    }
  }
`;

export const READ_MESSAGES = gql`
  query readMessages($chatId: String!, $messageId: String) {
    readMessages(chatId: $chatId, messageId: $messageId) {
      id
      chat {
        id
        title
      }
      from {
        id
        name
      }
      text
      type
      createdAt
    }
  }
`;
