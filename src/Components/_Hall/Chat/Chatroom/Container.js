import React, { useState } from "react";

import Bar from "./Bar";
import Reader from "./Reader/Container";
import Writer from "./Writer";
import * as Style from "../Style/StChatroom";

const Container = ({ iplayer, remoteChats, clickChat, setClickChat, toggleUpdaterModal }) => {
  console.log("DEV:: Chatroom");

  const [, setChatroomFresh] = useState();

  return (
    <Style.ChatroomViewer>
      <Bar setClickChat={setClickChat} toggleUpdaterModal={toggleUpdaterModal} />
      <Reader iplayer={iplayer} remoteChats={remoteChats} clickChat={clickChat} />
      <Writer
        iplayer={iplayer}
        remoteChats={remoteChats}
        clickChat={clickChat}
        setChatroomFresh={setChatroomFresh}
      />
    </Style.ChatroomViewer>
  );
};

export default Container;
