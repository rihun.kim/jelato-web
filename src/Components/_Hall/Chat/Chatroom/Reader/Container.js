import React, { createRef, useEffect, useState } from "react";

import MessageReader from "./MessageReader";
import { UseFetchMessages } from "../../Hook/Hooker";
import { getMessages, onFetch } from "../../Tool/ToChatroom";
import * as RStyle from "../../Style/Styler";
import * as Style from "../../Style/StChatroom";

const Container = ({ iplayer, remoteChats, clickChat }) => {
  console.log("DEV:: Reader");

  let messages = getMessages({ remoteChats, clickChat });
  const viwerendRef = createRef();
  const [fetchable, setFetchable] = useState(true);
  const [fetching, setFetching] = useState(false);
  const fetchMore = UseFetchMessages({ messages, clickChat });

  const onFetchClick = () => {
    onFetch({ messages, clickChat, setFetchable, setFetching, fetchMore });
  };
  useEffect(() => {
    viwerendRef.current.scrollIntoView({ alignToTop: false });
  }, [viwerendRef]);

  return (
    <Style.ChatreaderViewer>
      {fetchable && messages.length ? (
        fetching ? (
          <RStyle.InSpinner height="15px" color="#e4e4e4" margin="0 auto 10px auto" />
        ) : (
          <RStyle.FetchButton onClick={onFetchClick} />
        )
      ) : (
        false
      )}
      <MessageReader iplayer={iplayer} messages={messages} />
      <div ref={viwerendRef} />
    </Style.ChatreaderViewer>
  );
};

export default Container;
