import React from "react";

import { convertMessages, convertMessageTime } from "../../Tool/ToChatroom";
import * as RStyle from "../../Style/Styler";
import * as Style from "../../Style/StChatroom";

const MessageReader = ({ iplayer, messages }) => {
  let filteredMessages = convertMessages(messages);

  return filteredMessages.map((message) => {
    if (message.type === "official") {
      return (
        <Style.Message key={message.id} style={Style.MessageStyle.official}>
          {message.text}
        </Style.Message>
      );
    } else if (message.type === "normal") {
      if (message.from.id === iplayer.id) {
        return (
          <Style.Message key={message.id} style={Style.MessageStyle.i}>
            <RStyle.Text style={RStyle.TextStyle.iText}>{message.text}</RStyle.Text>
            <RStyle.Text style={RStyle.TextStyle.iTime}>
              {convertMessageTime(message.createdAt)}
            </RStyle.Text>
          </Style.Message>
        );
      } else {
        return (
          <Style.Message key={message.id} style={Style.MessageStyle.other}>
            <RStyle.Text style={RStyle.TextStyle.otherName}>{message.from.name}</RStyle.Text>
            <RStyle.Text style={RStyle.TextStyle.otherText}>{message.text}</RStyle.Text>
            <RStyle.Text style={RStyle.TextStyle.otherTime}>
              {convertMessageTime(message.createdAt)}
            </RStyle.Text>
          </Style.Message>
        );
      }
    } else if (message.type === "time") {
      return (
        <Style.Message key={message.id} style={Style.MessageStyle.time}>
          {message.createdAt}
        </Style.Message>
      );
    } else {
      return false;
    }
  });
};

export default MessageReader;
