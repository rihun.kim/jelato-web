import React from "react";

import * as RStyle from "../Style/Styler";
import * as Style from "../Style/StChatroom";

const Bar = ({ setClickChat, toggleUpdaterModal }) => {
  console.log("DEV:: Bar");

  return (
    <Style.ChatbarViewer>
      <RStyle.CloseButton
        style={RStyle.ButtonStyle.closechatroom}
        onClick={() => setClickChat("")}
      />
      <RStyle.SettingButton
        style={RStyle.ButtonStyle.settingchatroom}
        onClick={toggleUpdaterModal}
      />
    </Style.ChatbarViewer>
  );
};

export default Bar;
