import React from "react";
import { useMutation } from "@apollo/react-hooks";

import { CREATE_MESSAGE } from "../Hook/Gql";
import { getMessages, onSend } from "../Tool/ToChatroom";
import * as RStyle from "../Style/Styler";
import * as Style from "../Style/StChatroom";

const Writer = ({ iplayer, remoteChats, clickChat, setChatroomFresh }) => {
  console.log("DEV:: Writer");

  let messages = getMessages({ remoteChats, clickChat });
  const chatUseInput = RStyle.useInput("");
  const [createMessageMutation] = useMutation(CREATE_MESSAGE);

  const onSendClick = () => {
    onSend({
      iplayer,
      messages,
      chatUseInput,
      clickChat,
      createMessageMutation,
      setChatroomFresh,
    });
  };

  return (
    <Style.ChatwriterViewer>
      <RStyle.ChatInput placeholder={"내용을 입력하세요."} onSend={onSendClick} {...chatUseInput} />
      <RStyle.SendButton onClick={onSendClick} />
    </Style.ChatwriterViewer>
  );
};

export default Writer;
