import React from "react";

import OwnUpdater from "./OwnUpdater";
import InUpdater from "./InUpdater";
import { isOwner } from "../Tool/ToUpdater";

const Updater = ({ iplayer, remoteChats, clickChat, toggleUpdaterModal }) => {
  console.log("DEV:: Updater");

  if (isOwner({ iplayer, remoteChats, clickChat })) {
    return (
      <OwnUpdater
        iplayer={iplayer}
        remoteChats={remoteChats}
        clickChat={clickChat}
        toggleModal={toggleUpdaterModal}
      />
    );
  } else {
    return (
      <InUpdater
        iplayer={iplayer}
        remoteChats={remoteChats}
        clickChat={clickChat}
        toggleModal={toggleUpdaterModal}
      />
    );
  }
};

export default Updater;
