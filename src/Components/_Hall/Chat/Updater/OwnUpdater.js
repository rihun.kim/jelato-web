import React, { useState } from "react";
import { Carousel } from "react-responsive-carousel";
import { useMutation } from "@apollo/react-hooks";

import { UPDATE_CHAT, DELETE_CHAT, CREATE_MESSAGE } from "../Hook/Gql";
import {
  getChat,
  getPlayerGroupWithoutI,
  onChatSave,
  onChatIn,
  onChatOut,
} from "../Tool/ToUpdater";
import * as Style from "../Style/StUpdater";
import * as RStyle from "../Style/Styler";

const OwnUpdater = ({ iplayer, remoteChats, clickChat, toggleModal }) => {
  console.log("DEV:: OwnUpdater");

  let chat = getChat({ remoteChats, clickChat });
  let playerGroup = getPlayerGroupWithoutI({ iplayer, chat });
  const titleUseInput = RStyle.useInput("");
  const [mapDisable, setMapDisable] = useState(new Map());
  const [createMessageMutation] = useMutation(CREATE_MESSAGE);
  const [updateChatMutation] = useMutation(UPDATE_CHAT);
  const [deleteChatMutation] = useMutation(DELETE_CHAT);

  const onSaveClick = () => {
    onChatSave({ titleUseInput, clickChat, updateChatMutation, toggleModal });
  };
  const onInClick = ({ player }) => {
    onChatIn({
      iplayer,
      player,
      mapDisable,
      clickChat,
      createMessageMutation,
      updateChatMutation,
      setMapDisable,
    });
  };
  const onOutClick = (outplayer) => {
    onChatOut({
      iplayer,
      player: outplayer,
      chat,
      mapDisable,
      clickChat,
      createMessageMutation,
      updateChatMutation,
      deleteChatMutation,
      setMapDisable,
      toggleModal,
    });
  };

  return (
    <Style.UpdaterViewer>
      <RStyle.SaveButton onClick={onSaveClick} />
      <RStyle.CloseButton style={RStyle.ButtonStyle.closemodal} onClick={toggleModal} />
      <Carousel showArrows={false} showStatus={false} showThumbs={false}>
        <Style.ListViewer>
          <Style.ListTitle>CHATERS</Style.ListTitle>
          <Style.ListMember>
            {iplayer.name} (owner, me)
            <RStyle.Group display="flex" float="right">
              {mapDisable.get(iplayer.id) ? (
                <RStyle.InSpinner />
              ) : (
                <RStyle.Text
                  style={RStyle.TextStyle.textbutton}
                  onClick={() => onOutClick(iplayer)}
                >
                  나가기
                </RStyle.Text>
              )}
            </RStyle.Group>
          </Style.ListMember>
          {playerGroup.ining.length
            ? playerGroup.ining.map((player) => (
                <Style.ListMember key={player.id}>
                  {player.name} (chater)
                  <RStyle.Group display="flex" float="right">
                    {mapDisable.get(player.id) ? (
                      <RStyle.InSpinner />
                    ) : (
                      <RStyle.Text
                        style={RStyle.TextStyle.textbutton}
                        onClick={() => onOutClick(player)}
                      >
                        내보내기
                      </RStyle.Text>
                    )}
                  </RStyle.Group>
                </Style.ListMember>
              ))
            : false}
          {playerGroup.waiting.length
            ? playerGroup.waiting.map((player) => (
                <Style.ListMember key={player.id}>
                  {player.name}
                  <RStyle.Group display="flex" float="right">
                    {mapDisable.get(player.id) ? (
                      <RStyle.InSpinner />
                    ) : (
                      <RStyle.Text
                        style={RStyle.TextStyle.textbutton}
                        onClick={() => onInClick({ player })}
                      >
                        초대하기
                      </RStyle.Text>
                    )}
                  </RStyle.Group>
                </Style.ListMember>
              ))
            : false}
        </Style.ListViewer>
        <Style.UpdaterInViewer>
          <RStyle.Input
            placeholder={chat.title === "none" ? "새 채팅방 이름" : chat.title}
            {...titleUseInput}
          />
        </Style.UpdaterInViewer>
      </Carousel>
    </Style.UpdaterViewer>
  );
};

export default OwnUpdater;
