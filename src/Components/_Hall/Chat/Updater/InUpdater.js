import React, { useState } from "react";
import { useMutation } from "@apollo/react-hooks";

import { UPDATE_CHAT, CREATE_MESSAGE } from "../Hook/Gql";
import { getChat, getPlayerGroupWithoutI, onChatIn, onChatOut } from "../Tool/ToUpdater";
import * as RStyle from "../Style/Styler";
import * as Style from "../Style/StUpdater";

const InUpdater = ({ iplayer, remoteChats, clickChat, toggleModal }) => {
  console.log("DEV:: IningUpdater");

  let chat = getChat({ remoteChats, clickChat });
  let playerGroup = getPlayerGroupWithoutI({ iplayer, chat });
  const [mapDisable, setMapDisable] = useState(new Map());
  const [createMessageMutation] = useMutation(CREATE_MESSAGE);
  const [updateChatMutation] = useMutation(UPDATE_CHAT);

  const onInClick = ({ player }) => {
    onChatIn({
      iplayer,
      player,
      mapDisable,
      clickChat,
      createMessageMutation,
      updateChatMutation,
      setMapDisable,
    });
  };
  const onOutClick = (outplayer) => {
    onChatOut({
      iplayer,
      player: outplayer,
      chat,
      mapDisable,
      clickChat,
      createMessageMutation,
      updateChatMutation,
      setMapDisable,
      toggleModal,
    });
  };

  return (
    <Style.UpdaterViewer>
      <RStyle.CloseButton style={RStyle.ButtonStyle.closemodal} onClick={toggleModal} />
      <Style.ListViewer>
        <Style.ListTitle>CHATERS</Style.ListTitle>
        <Style.ListMember>
          {iplayer.name} (me)
          <RStyle.Group display="flex" float="right">
            {mapDisable.get(iplayer.id) ? (
              <RStyle.InSpinner />
            ) : (
              <RStyle.Text style={RStyle.TextStyle.textbutton} onClick={() => onOutClick(iplayer)}>
                나가기
              </RStyle.Text>
            )}
          </RStyle.Group>
        </Style.ListMember>
        <Style.ListMember>{playerGroup.owning.name} (owner)</Style.ListMember>
        {playerGroup.ining.length
          ? playerGroup.ining.map((player) => (
              <Style.ListMember key={player.id}>{player.name} (chater)</Style.ListMember>
            ))
          : false}
        {playerGroup.waiting.length
          ? playerGroup.waiting.map((player) => (
              <Style.ListMember key={player.id}>
                {player.name}
                <RStyle.Group display="flex" float="right">
                  {mapDisable.get(player.id) ? (
                    <RStyle.InSpinner />
                  ) : (
                    <RStyle.Text
                      style={RStyle.TextStyle.textbutton}
                      onClick={() => onInClick({ player })}
                    >
                      초대하기
                    </RStyle.Text>
                  )}
                </RStyle.Group>
              </Style.ListMember>
            ))
          : false}
      </Style.ListViewer>
    </Style.UpdaterViewer>
  );
};

export default InUpdater;
