import React, { useMemo, useState } from "react";

import Chatlist from "./Chatlist/Container";
import Chatroom from "./Chatroom/Container";
import Creater from "./Creater/Container";
import Updater from "./Updater/Container";
import {
  UseQueryChats,
  UseQueryVhats,
  UseSubsChats,
  UseSubsVhats,
  UseSubsMessages,
} from "./Hook/Hooker";
import { memoChats, memoVhats, memoMessages, isChatExist } from "./Tool/ToArbiter";
import { Spinner } from "../../Design/Spinner";
import * as RStyle from "./Style/Styler";

const Container = ({ match, remoteOwnplayers }) => {
  console.log("DEV:: Container");

  let iplayer = remoteOwnplayers.readOwnplayers[Number(match.params.index)];
  let remoteChats = UseQueryChats({ iplayer });
  let remoteVhats = UseQueryVhats({ iplayer });
  let realtimeChats = UseSubsChats({ iplayer });
  let realtimeVhats = UseSubsVhats({ iplayer });
  let realtimeMessages = UseSubsMessages({ iplayer });
  const [clickChat, setClickChat] = useState("");
  const [createrModal, setCreaterModal] = useState(false);
  const [updaterModal, setUpdaterModal] = useState(false);

  const toggleCreaterModal = () => {
    createrModal ? setCreaterModal(false) : setCreaterModal(true);
  };
  const toggleUpdaterModal = () => {
    updaterModal ? setUpdaterModal(false) : setUpdaterModal(true);
  };
  useMemo(() => {
    if (remoteChats && realtimeChats) {
      memoChats({ remoteChats, realtimeChats, remoteVhats });
      if (!isChatExist(remoteChats.readChats, clickChat)) setClickChat("");
    }
  }, [remoteChats, realtimeChats]); // eslint-disable-line react-hooks/exhaustive-deps
  useMemo(() => {
    if (remoteVhats && realtimeVhats) {
      memoVhats({ remoteVhats, realtimeVhats, remoteChats });
      if (!isChatExist(remoteChats.readChats, clickChat)) setClickChat("");
    }
  }, [remoteVhats, realtimeVhats]); // eslint-disable-line react-hooks/exhaustive-deps
  useMemo(() => {
    if (remoteChats && realtimeMessages) memoMessages({ remoteChats, realtimeMessages, iplayer });
  }, [remoteChats, realtimeMessages]); // eslint-disable-line react-hooks/exhaustive-deps

  if (!(remoteChats && remoteVhats))
    return <Spinner windowLeft={200} windowWidth={window.innerWidth - 200} />;
  return (
    <RStyle.Viewer>
      <RStyle.InViewer>
        <Chatlist
          iplayer={iplayer}
          remoteChats={remoteChats}
          remoteVhats={remoteVhats}
          clickChat={clickChat}
          setClickChat={setClickChat}
          toggleCreaterModal={toggleCreaterModal}
        />
        {clickChat === "" ? (
          false
        ) : (
          <Chatroom
            iplayer={iplayer}
            remoteChats={remoteChats}
            clickChat={clickChat}
            setClickChat={setClickChat}
            toggleUpdaterModal={toggleUpdaterModal}
          />
        )}
        {createrModal && <Creater iplayer={iplayer} toggleCreaterModal={toggleCreaterModal} />}
        {updaterModal && (
          <Updater
            iplayer={iplayer}
            remoteChats={remoteChats}
            clickChat={clickChat}
            toggleUpdaterModal={toggleUpdaterModal}
          />
        )}
      </RStyle.InViewer>
    </RStyle.Viewer>
  );
};

export default Container;
