import styled from "styled-components";

//
export const HEIGHT = window.innerHeight + "px";
export const CHATREADER_HEIGHT = window.innerHeight - 130 + "px";

//
export const ChatroomViewer = styled.div`
  width: 690px;
  height: ${HEIGHT};

  display: flex;
  flex-direction: column;

  border-left: 1px solid #d8d8d8;
  border-right: 1px solid #d8d8d8;
  flex-shrink: 0;
  order: 1;
`;

//
export const ChatbarViewer = styled.div`
  width: 100%;
  height: 40px;

  background-color: #9bbad8;
`;

//
export const ChatreaderViewer = styled.div`
  width: 100%;
  height: ${CHATREADER_HEIGHT};

  display: flex;
  flex-direction: column;

  background-color: #9bbad8;
  overflow-y: auto;
  padding: 10px 20px 10px 20px;
`;

//
export const ChatwriterViewer = styled.div`
  width: 100%;
  height: 100px;

  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: space-evenly;
`;

//
export const Message = styled.div`
  background-color: ${(props) => props.backgroundColor}
  color: ${(props) => props.color};
  font-size: ${(props) => props.fontSize};
  margin: ${(props) => props.margin};
  padding: ${(props) => props.padding};
`;

export const MessageStyle = {
  official: {
    color: "#ececec",
    fontSize: "11px",
    margin: "0 auto 15px auto",
    padding: "2px 7px 3px 7px",
  },
  time: {
    color: "#ececec",
    fontSize: "11px",
    margin: "0 auto 15px auto",
    padding: "2px 7px 3px 7px",
  },
  i: {
    float: "right",
    margin: "0 0 15px 0",
  },
  other: {
    float: "left",
    margin: "0 0 15px 0",
  },
};
