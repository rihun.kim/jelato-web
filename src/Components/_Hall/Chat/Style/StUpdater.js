import styled from "styled-components";

import { getLeftMove, getTopMove } from "./Styler";

//
export const UpdaterViewer = styled.div`
  width: 700px;
  height: 340px;

  background-color: #ffffff;
  border-radius: 3px;
  box-shadow: 0 0 4px rgba(0, 0, 0, 0.14), 0 4px 8px rgba(0, 0, 0, 0.28);
  left: ${getLeftMove(700)};
  padding: 40px 25px 20px 25px;
  position: fixed;
  top: ${getTopMove(340)};
  z-index: 3;
`;

//
export const UpdaterInViewer = styled.div`
  width: 100%;
  height: 280px;

  background-color: #ffffff;
  overflow-y: auto;
`;

//
export const ConfirmViewer = styled.div`
  width: 700px;
  height: 110px;

  background-color: #ffffff;
  border-radius: 3px;
  box-shadow: 0 0 4px rgba(0, 0, 0, 0.14), 0 4px 8px rgba(0, 0, 0, 0.28);
  left: ${getLeftMove(700)};
  padding: 40px 25px 20px 25px;
  position: fixed;
  top: ${getTopMove(110)};
  z-index: 3;
`;

//
export const ListViewer = styled.div`
  width: 100%;
  height: 280px;

  text-align: left;
  background-color: #ffffff;
  overflow-y: auto;
`;

//
export const ListTitle = styled.div`
  width: 100%;
  height: 15px;

  font-size: 14px;
  font-weight: 500;
  margin: 0 0 20px 0;
`;

export const ListMember = styled.div`
  width: 100%;
  height: 25px;

  font-size: 12px;
  font-weight: 400;
`;
