import styled from "styled-components";

//
export const HEIGHT = window.innerHeight + "px";

//
export const ChatlistViewer = styled.div`
  width: 370px;
  height: ${HEIGHT};

  display: flex;
  flex-direction: column;
  justify-content: center;

  flex-shrink: 0;
  order: 0;
  overflow-y: auto;
`;

//
export const ChatlistInViewer = styled.div`
  max-height: 100%;

  margin: 5px auto 5px auto;
`;

//
export const ListChat = styled.div`
  width: 320px;
  height: 80px;

  background: ${(props) => (props.clickList === "" ? "#ffffff" : "#fffc9f")};
  border: ${(props) => props.border};
  box-shadow: 1px 3px 4px rgba(0, 0, 0, 0.19), 0 1px 0 rgba(0, 0, 0, 0.23);
  cursor: pointer;
  margin: 5px auto 5px auto;
  padding: 10px 10px 10px 10px;
`;

export const ListVhat = styled.div`
  width: 320px;
  height: 80px;

  background-color: #ececec;
  border: ${(props) => props.border};
  box-shadow: 1px 3px 4px rgba(0, 0, 0, 0.19), 0 1px 0 rgba(0, 0, 0, 0.23);
  cursor: pointer;
  margin: 5px auto 5px auto;
  padding: 10px 10px 10px 10px;
`;
