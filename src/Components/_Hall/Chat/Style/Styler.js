import React, { useState } from "react";
import styled from "styled-components";
import Loader from "react-loader-spinner";
import { IoMdClose, IoMdSettings, IoIosSend, IoMdRadioButtonOff } from "react-icons/io";
import { MdExpandMore } from "react-icons/md";

//
export const WIDTH = window.innerWidth - 200 + "px";
export const HEIGHT = window.innerHeight + "px";

//
export const getLeftMove = (width) => window.innerWidth / 2 + 100 - width / 2 + "px";
export const getTopMove = (height) => window.innerHeight / 2 - height / 2 + "px";

//
export const Viewer = styled.div`
  width: ${WIDTH};
  height: 100vh;

  left: 200px;
  position: fixed;
`;
export const InViewer = styled.div`
  width: 1060px;

  display: flex;
  flex-direction: row;
  align-items: center;

  margin: 0 auto 0 auto;
`;

//
export const Group = styled.div`
  max-height: ${(props) => props.maxHeight};

  display: ${(props) => props.display};
  float: ${(props) => props.float};
  margin: ${(props) => props.margin};
`;

//
export const SendButton = ({ onClick }) => (
  <div
    onClick={onClick}
    style={{
      width: "70px",
      height: "40px",
      backgroundColor: "#fff977",
      cursor: "pointer",
      display: "flex",
      flexDirection: "row",
      alignItems: "center",
      justifyContent: "center",
    }}
  >
    <IoIosSend
      title="보내기"
      style={{
        width: "16px",
        height: "16px",
        color: "#391c1b",
        flexShrink: 0,
      }}
    />
  </div>
);
export const FetchButton = ({ onClick }) => (
  <div style={{ width: "15px", height: "15px", margin: "0 auto 10px auto" }}>
    <MdExpandMore
      title="더보기"
      style={{
        width: "15px",
        height: "15px",
        color: "#e4e4e4",
        cursor: "pointer",
      }}
      onClick={onClick}
    />
  </div>
);
export const SaveButton = ({ onClick }) => (
  <IoMdRadioButtonOff
    title="저장하기"
    style={{
      width: "14px",
      height: "14px",
      color: "#a5a2a2",
      cursor: "pointer",
      position: "absolute",
      right: 40,
      top: 20,
      zIndex: 3,
    }}
    onClick={onClick}
  />
);
export const SettingButton = ({ style, onClick }) => (
  <IoMdSettings title="설정" style={style} onClick={onClick} />
);
export const CloseButton = ({ style, onClick }) => (
  <IoMdClose title="닫기" style={style} onClick={onClick} />
);

//
export const ButtonStyle = {
  closemodal: {
    width: "16px",
    height: "16px",
    color: "#a5a2a2",
    cursor: "pointer",
    position: "absolute",
    right: 20,
    top: 20,
    zIndex: 3,
  },
  closechatroom: {
    width: "18px",
    height: "100%",

    display: "inline-block",
    verticalAlign: "middle",

    color: "#ffffff",
    cursor: "pointer",
    float: "right",
    margin: "0 10px 0 0",
  },
  settingchatroom: {
    width: "18px",
    height: "100%",

    display: "inline-block",
    verticalAlign: "middle",

    color: "#ffffff",
    cursor: "pointer",
    float: "right",
    margin: "0 5px 0 0",
  },
};

//
export const Text = styled.div`
  width: ${(props) => props.width};
  max-width: ${(props) => props.maxWidth};
  height: ${(props) => props.height};

  color: ${(props) => props.color};
  background-color: ${(props) => props.backgroundColor};
  border-radius: ${(props) => props.borderRadius};
  display: ${(props) => props.display};
  float: ${(props) => props.float};
  font-size: ${(props) => props.fontSize};
  font-weight: ${(props) => props.fontWeight};
  line-height: ${(props) => props.lineHeight};
  margin: ${(props) => props.margin};
  overflow: ${(props) => props.overflow};
  padding: ${(props) => props.padding};
  text-align: ${(props) => props.textAlign};
  text-overflow: ${(props) => props.textOverflow};
  vertical-align: ${(props) => props.verticalAlign};
  white-space: ${(props) => props.whiteSpace};
  word-wrap: ${(props) => props.wordWrap};
`;

export const TextStyle = {
  textbutton: {
    color: "#a0a0a0",
    cursor: "pointer",
    fontSize: "10px",
    textAlign: "center",
  },
  iText: {
    maxWidth: "250px",
    backgroundColor: "#fff977",
    borderRadius: "5px",
    color: "#000000",
    fontSize: "15px",
    fontWeight: "400",
    float: "right",
    padding: "8px 8px 8px 8px",
    whiteSpace: "pre-wrap",
    wordWrap: "break-word",
  },
  iTime: {
    color: "#ffffff",
    fontSize: "10px",
    fontWeight: "300",
    float: "right",
    padding: "4px 5px 4px 5px",
  },
  otherName: {
    maxWidth: "140px",
    height: "34px",
    backgroundColor: "#ffffff",
    borderRadius: "3px",
    color: "#000000",
    fontSize: "15px",
    fontWeight: "400",
    float: "left",
    padding: "8px 8px 8px 8px",
    whiteSpace: "nowrap",
    overflow: "hidden",
    textOverflow: "ellipsis",
  },
  otherText: {
    maxWidth: "250px",
    backgroundColor: "#ffffff",
    borderRadius: "3px",
    color: "#000000",
    fontSize: "15px",
    fontWeight: "400",
    float: "left",
    padding: "8px 8px 8px 8px",
    whiteSpace: "pre-wrap",
  },
  otherTime: {
    color: "#ffffff",
    fontSize: "10px",
    fontWeight: "300",
    float: "left",
    padding: "4px 5px 4px 5px",
  },
  barText: {
    color: "#ffffff",
    maxWidth: "450px",
    float: "left",
    fontSize: "15px",
    fontWeight: "400",
    margin: "0 0 0 10px",
    overflow: "hidden",
    lineHeight: "40px",
  },
  chaters: {
    width: "230px",
    height: "18px",
    color: "#000000",
    fontSize: "15px",
    fontWeight: "400",
    float: "left",
    overflow: "hidden",
    textOverflow: "ellipsis",
  },
  confirm: {
    width: "100%",
    color: "#797979",
    fontSize: "14px",
    textAlign: "center",
  },
  outchaters: {
    width: "230px",
    height: "18px",
    color: "#9a9a9a",
    fontSize: "15px",
    fontWeight: "400",
    float: "left",
    overflow: "hidden",
    textOverflow: "ellipsis",
  },
  lastdate: {
    width: "70px",
    height: "18px",
    color: "#bcbcbc",
    fontSize: "10px",
    lineHeight: "18px",
    fontWeight: "348",
    float: "right",
    textAlign: "right",
  },
  lastmessage: {
    height: "16px",
    color: "#bcbcbc",
    fontSize: "13px",
    fontWeight: "400",
    margin: "40px 0 0 0",
    overflow: "hidden",
    whiteSpace: "nowrap",
    textOverflow: "ellipsis",
  },
  guide: {
    color: "#000000",
    float: "left",
    fontSize: "15px",
    fontWeight: "600",
    margin: "20px 0 auto 0",
  },
};

//
export const useCheck = (useCheckMap) => {
  let map = useCheckMap;
  const onChange = (e) => {
    map.set(e.target.name, e.target.checked);
  };
  return { onChange, map };
};
const CheckboxViewer = styled.input`
  width: 12px;
  height: 12px;

  float: right;
`;
export const Checkbox = ({ name, onChange, type = "checkbox" }) => (
  <CheckboxViewer name={name} onChange={onChange} type={type} />
);

//
export const useInput = (prefilled) => {
  const [value, setValue] = useState(prefilled);
  const onChange = (e) => {
    setValue(e.target.value);
  };
  const reFresh = () => {
    setValue("");
  };
  return { onChange, reFresh, setValue, value };
};
const InputViewer = styled.input`
  width: 100%;
  height: 35px;

  border: 0;
  border-bottom: 1px solid #e0e0e0;
  font-size: 14px;
  margin: 0 0 10px 0;
  outline: none;
  padding: 0 15px 0 15px;
`;
export const Input = ({ onChange, placeholder, value }) => (
  <InputViewer defaultValue={value} onChange={onChange} placeholder={placeholder} type="text" />
);

//
const onKeyDown = (e, onSend) => {
  if (e.keyCode === 13 && e.shiftKey) {
  } else if (e.keyCode === 13) {
    onSend();
    e.preventDefault();
  }
};
const TextareaViewer = styled.textarea`
  width: 580px;
  height: 80px;

  border: 0;
  font-size: 14px;

  outline: none;
  resize: none;
  text-align: left;
  white-space: pre-wrap;
`;
export const ChatInput = ({ onChange, onSend, placeholder, value }) => (
  <TextareaViewer
    value={value}
    onChange={onChange}
    onKeyDown={(e) => onKeyDown(e, onSend)}
    placeholder={placeholder}
  />
);

//
const InSpinnerViewer = styled.div`
  width: 100%;
  height: ${(props) => props.height};

  align-items: center;
  display: flex;
  justify-content: center;
  margin: ${(props) => props.margin};
`;
export const InSpinner = ({ height = "100%", color = "#2b2b28", margin = "0 0 0 0" }) => (
  <InSpinnerViewer height={height} margin={margin}>
    <Loader type="ThreeDots" color={color} height={18} width={18} />
  </InSpinnerViewer>
);
