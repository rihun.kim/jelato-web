import React, { useState } from "react";
import { useMutation } from "@apollo/react-hooks";

import { CREATE_CHAT } from "../Hook/Gql";
import { getPlayersWithoutI, onCreate } from "../Tool/ToCreater";
import * as Style from "../Style/StCreater";
import * as RStyle from "../Style/Styler";

const Container = ({ iplayer, toggleCreaterModal }) => {
  console.log("DEV:: Creater");

  let players = getPlayersWithoutI(iplayer);
  let playerUseCheck = RStyle.useCheck(new Map());
  const [createChatMutation] = useMutation(CREATE_CHAT);
  const [modalAble, setModalAble] = useState(true);

  const onCreatelick = () => {
    onCreate({
      iplayer,
      playerUseCheck,
      createChatMutation,
      setModalAble,
      toggleModal: toggleCreaterModal,
    });
  };

  return (
    <Style.CreaterViewer>
      {modalAble ? (
        <>
          <RStyle.SaveButton onClick={onCreatelick} />
          <RStyle.CloseButton style={RStyle.ButtonStyle.closemodal} onClick={toggleCreaterModal} />
          <Style.ListViewer>
            <Style.ListTitle>채팅을 함께할 멤버를 선택해보세요.</Style.ListTitle>
            {players.length
              ? players.map(({ id, name }) => (
                  <RStyle.Group key={id} display="flex">
                    <Style.ListMember>{name}</Style.ListMember>
                    <RStyle.Checkbox name={id} {...playerUseCheck} />
                  </RStyle.Group>
                ))
              : false}
          </Style.ListViewer>
        </>
      ) : (
        <RStyle.InSpinner />
      )}
    </Style.CreaterViewer>
  );
};

export default Container;
