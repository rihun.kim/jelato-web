//
export const getChat = ({ remoteChats, clickChat }) => {
  return remoteChats.readChats.filter((chat) => (chat.id === clickChat ? true : false))[0];
};

export const getPlayerGroupWithoutI = ({ iplayer, chat }) => {
  let players = iplayer.playground.players;
  let chaters = chat.players;
  let filteredPlayers = { owning: {}, ining: [], waiting: [] };

  for (const chater of chaters) {
    if (chater.id === iplayer.id) {
      continue;
    } else if (chater.id === chat.owner) {
      filteredPlayers.owning = chater;
    } else {
      filteredPlayers.ining.push(chater);
    }
  }

  for (const player of players) {
    for (let i = 0; i < chaters.length; ++i) {
      if (player.id === chaters[i].id) {
        break;
      } else if (i === chaters.length - 1) {
        filteredPlayers.waiting.push(player);
      }
    }
  }

  return filteredPlayers;
};

export const isOwner = ({ iplayer, remoteChats, clickChat }) => {
  return iplayer.id === getChat({ remoteChats, clickChat }).owner ? true : false;
};

//
export const onChatSave = ({ titleUseInput, clickChat, updateChatMutation, toggleModal }) => {
  try {
    if (titleUseInput.value !== "") {
      updateChatMutation({
        variables: {
          action: "",
          chatId: clickChat,
          title: titleUseInput.value,
        },
      });
      toggleModal();
    }
  } catch (error) {
    alert(error);
  }
};

export const onChatIn = async ({
  iplayer,
  player,
  mapDisable,
  clickChat,
  createMessageMutation,
  updateChatMutation,
  setMapDisable,
}) => {
  try {
    setMapDisable(new Map(mapDisable.set(player.id, true)));
    await updateChatMutation({
      variables: {
        action: "onIn",
        chatId: clickChat,
        playerId: player.id,
      },
    });
    createMessageMutation({
      variables: {
        chatId: clickChat,
        playerId: iplayer.id,
        text: iplayer.name + " 님이 " + player.name + " 님을 초대하였습니다.",
        type: "official",
      },
    });
    setMapDisable(new Map(mapDisable.set(player.id, false)));
  } catch (error) {
    alert(error);
  }
};

export const onChatOut = async ({
  iplayer,
  player,
  chat,
  mapDisable,
  clickChat,
  createMessageMutation,
  updateChatMutation,
  deleteChatMutation,
  setMapDisable,
  toggleModal,
}) => {
  try {
    if (iplayer.id === player.id) {
      setMapDisable(new Map(mapDisable.set(iplayer.id, true)));
      if (iplayer.id === chat.owner) {
        if (chat.players.length === 1) {
          deleteChatMutation({
            variables: {
              chatId: clickChat,
            },
          });
        } else {
          updateChatMutation({
            variables: {
              action: "onOwnerout",
              chatId: clickChat,
              playerId: iplayer.id,
            },
          });
          createMessageMutation({
            variables: {
              chatId: clickChat,
              playerId: iplayer.id,
              text: iplayer.name + " 님이 나갔습니다.",
              type: "official",
            },
          });
        }
      } else {
        updateChatMutation({
          variables: {
            action: "onOut",
            chatId: clickChat,
            playerId: iplayer.id,
          },
        });
        createMessageMutation({
          variables: {
            chatId: clickChat,
            playerId: iplayer.id,
            text: iplayer.name + " 님이 나갔습니다.",
            type: "official",
          },
        });
      }
      toggleModal();
    } else {
      setMapDisable(new Map(mapDisable.set(player.id, true)));
      await updateChatMutation({
        variables: {
          action: "onOut",
          chatId: clickChat,
          playerId: player.id,
        },
      });
      createMessageMutation({
        variables: {
          chatId: clickChat,
          playerId: iplayer.id,
          text: player.name + " 님을 내보냈습니다.",
          type: "official",
        },
      });
      setMapDisable(new Map(mapDisable.set(player.id, false)));
    }
  } catch (error) {
    alert(error);
  }
};
