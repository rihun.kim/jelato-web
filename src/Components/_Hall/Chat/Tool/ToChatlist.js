//
export const convertReceiveTime = (time) => {
  const rTime = new Date(time);
  const rYear = rTime.getFullYear(),
    rMonth = rTime.getMonth(),
    rDate = rTime.getDate(),
    rHour = rTime.getHours(),
    rMin = rTime.getMinutes();

  const cTime = new Date();
  const cYear = cTime.getFullYear(),
    cMonth = cTime.getMonth(),
    cDate = cTime.getDate();

  if (rYear === cYear)
    if (rMonth === cMonth)
      if (rDate === cDate)
        return rHour <= 12 ? "오전 " + rHour + ":" + rMin : "오후 " + (rHour - 12) + ":" + rMin;
      else if (rDate + 1 === cDate) return "어제";
      else return rMonth + 1 + "월 " + rDate + "일 ";
    else return rYear + "." + (rMonth + 1) + "." + rDate;
};

export const getCommaList = (players) => {
  let commaList = "";
  for (const player of players) commaList += player.name + ", ";
  return commaList.slice(0, -2);
};

export const getCommaListWithLast = (players, iplayer) => {
  let commaList = "";
  for (const player of players) if (player.id !== iplayer.id) commaList += player.name + ", ";
  commaList = commaList + iplayer.name;
  return commaList;
};

//
export const onVhatOut = async ({
  iplayer,
  vhats,
  clickVhat,
  setClickVhat,
  updateVhatMutation,
  toggleModal,
}) => {
  try {
    updateVhatMutation({
      variables: {
        vhatId: clickVhat,
        playerId: iplayer.id,
      },
    });
    vhats.splice(
      vhats.findIndex((vhat) => vhat.id === clickVhat),
      1
    );
    toggleModal();
    setClickVhat("");
  } catch (error) {
    alert(error);
  }
};
