import { cloneDeep } from "lodash";

//
export const isChatExist = (chats, chatId) => {
  for (const chat of chats) if (chat.id === chatId) return true;
  return false;
};

//
// 제목 바꾸는 경우 --> 커버됨
// 내가 초대되는 경우 --> 커버됨, vhat 에 있으면 삭제함
// 딴 사람이 초대되는 경우 --> 커버됨
// 강퇴되는 경우 --> 여기서 제어 하지 않음
export const memoChats = ({ remoteChats, realtimeChats, remoteVhats }) => {
  const mutation = realtimeChats.subscribeChats.mutation;
  const realtimeChat = realtimeChats.subscribeChats.node;

  if (mutation === "CREATED") {
    remoteChats.readChats.push(cloneDeep(realtimeChat));
  } else if (mutation === "UPDATED") {
    let isChatExist = false;

    for (const chat of remoteChats.readChats) {
      if (chat.id === realtimeChat.id) {
        chat.count = realtimeChat.count;
        chat.title = realtimeChat.title;
        chat.owner = realtimeChat.owner;
        chat.players = cloneDeep(realtimeChat.players);

        isChatExist = true;
        break;
      }
    }

    if (!isChatExist) {
      const vhatIndex = remoteVhats.readVhats.findIndex((vhat) => vhat.chatid === realtimeChat.id);
      if (vhatIndex > -1) remoteVhats.readVhats.splice(vhatIndex, 1);
      remoteChats.readChats.push(cloneDeep(realtimeChat));
    }
  } else if (mutation === "DELETED") {
    const chatIndex = remoteChats.readChats.findIndex(
      (chat) => chat.id === realtimeChats.subscribeChats.previousValues.id
    );
    remoteChats.readChats.splice(chatIndex, 1);
  }
};

// 내가 강퇴되는 경우 --> vhat 과의 커넥션이 생김 --> 커버됨, chat 이 있으면 삭제함
// 딴 사람이 강퇴되는 경우 --> 커버됨
export const memoVhats = ({ remoteVhats, realtimeVhats, remoteChats }) => {
  const mutation = realtimeVhats.subscribeVhats.mutation;
  const realtimeVhat = realtimeVhats.subscribeVhats.node;

  if (mutation === "UPDATED") {
    let isVhatExist = false;

    for (const vhat of remoteVhats.readVhats) {
      if (vhat.id === realtimeVhats.id) {
        vhat.players = cloneDeep(realtimeVhat.players);

        isVhatExist = true;
        break;
      }
    }

    if (!isVhatExist) {
      const chatIndex = remoteChats.readChats.findIndex((chat) => chat.id === realtimeVhat.chatid);

      if (chatIndex > -1) remoteChats.readChats.splice(chatIndex, 1);

      remoteVhats.readVhats.push(cloneDeep(realtimeVhat));
    }
  } else if (mutation === "DELETED") {
    const vhatIndex = remoteVhats.readVhats.findIndex(
      (vhat) => vhat.id === realtimeVhats.subscribeVhats.previousValues.id
    );
    realtimeVhats.readVhats.splice(vhatIndex, 1);
  }
};

// 로컬 메시지를 리모트 메시지로 교체 필요 // 현재 자신의 메시지는 안 넣고 있음!
// subscibing 이 런타임시에 되므로, 새 메시지가 올때마다 container 리프레시 발생
export const memoMessages = ({ remoteChats, realtimeMessages, iplayer }) => {
  const realtimeMessage = realtimeMessages.subscribeMessages;

  for (const chat of remoteChats.readChats) {
    if (chat.id === realtimeMessage.chat.id) {
      if (realtimeMessage.type === "official" || realtimeMessage.from.id !== iplayer.id) {
        chat.messages.push(cloneDeep(realtimeMessage));
        break;
      }
    }
  }
};
