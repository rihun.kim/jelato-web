//
const convertChatTime = (date) => {
  const time = new Date(date);
  const week = ["일요일", "월요일", "화요일", "수요일", "목요일", "금요일", "토요일"];
  const convertedTime =
    "" +
    time.getFullYear() +
    "년 " +
    (time.getMonth() + 1) +
    "월 " +
    time.getDate() +
    "일 " +
    week[new Date(time).getDay()];

  return convertedTime;
};

export const convertMessageTime = (date) => {
  const time = new Date(date);
  let hh = time.getHours();

  if (hh === 12) {
    hh = "오후 " + hh;
  } else if (hh === 24) {
    hh = "오전 " + (hh - 12);
  } else {
    hh = hh < 12 ? "오전 " + hh : "오후 " + (hh - 12);
  }

  let mm = time.getMinutes();
  mm = mm < 10 ? "0" + mm : mm;
  const convertedTime = hh + ":" + mm;

  return convertedTime;
};

export const convertMessages = (messages) => {
  let previousChatTime, currentChatTime;
  let convertedMessages = [];

  for (const message of messages) {
    currentChatTime = message.createdAt.substr(0, 10);

    if (previousChatTime === currentChatTime) {
      convertedMessages.push(message);
    } else {
      previousChatTime = currentChatTime;

      convertedMessages.push(
        { id: currentChatTime, createdAt: convertChatTime(message.createdAt), type: "time" },
        message
      );
    }
  }

  return convertedMessages;
};

//
export const getMessages = ({ remoteChats, clickChat }) => {
  return remoteChats.readChats.filter((chat) => (chat.id === clickChat ? true : false))[0].messages;
};

//
export const onFetch = ({ messages, clickChat, setFetchable, setFetching, fetchMore }) => {
  setFetching(true);
  fetchMore({
    variables: {
      chatId: clickChat,
      messageId: messages[0].id,
    },
    updateQuery: (_, { fetchMoreResult }) => {
      if (fetchMoreResult.readMessages.length === 0) {
        setFetchable(false);
      } else {
        messages.unshift(...fetchMoreResult.readMessages);
      }
    },
  }).then(() => {
    setFetching(false);
  });
};

export const onSend = ({
  iplayer,
  messages,
  chatUseInput,
  clickChat,
  createMessageMutation,
  setChatroomFresh,
}) => {
  try {
    if (!chatUseInput.value) return;

    chatUseInput.reFresh();
    messages.push({
      id: new Date().toISOString(),
      from: iplayer,
      text: chatUseInput.value,
      type: "normal",
      createdAt: new Date().toISOString(),
    });
    createMessageMutation({
      variables: {
        chatId: clickChat,
        playerId: iplayer.id,
        text: chatUseInput.value,
        type: "normal",
      },
    });
    setChatroomFresh(new Date());
  } catch (error) {
    alert(error);
  }
};
