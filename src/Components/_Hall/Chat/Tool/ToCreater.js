//
export const getPlayersWithoutI = (iplayer) => {
  let players = iplayer.playground.players;
  let filteredPlayers = [];
  players.map((player) => (player.id === iplayer.id ? false : filteredPlayers.push(player)));
  return filteredPlayers;
};

const getPlayerIds = (iplayer, playerUseCheck) => {
  let playerIds = [];
  for (let [key, value] of playerUseCheck.map.entries()) if (value) playerIds.push(key);
  playerIds.unshift(iplayer.id);
  return playerIds;
};

//
export const onCreate = async ({
  iplayer,
  playerUseCheck,
  createChatMutation,
  setModalAble,
  toggleModal,
}) => {
  try {
    const playerIds = getPlayerIds(iplayer, playerUseCheck);

    if (playerIds.length === 1) {
      alert("채팅 멤버 선택하세요.");
    } else {
      setModalAble(false);
      await createChatMutation({
        variables: {
          playgroundId: iplayer.playground.id,
          playerIds: playerIds,
        },
      });
      toggleModal();
    }
  } catch (error) {
    alert(error);
  }
};
