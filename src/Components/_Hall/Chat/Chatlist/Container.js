import React, { useState, useEffect } from "react";

import Confirmer from "./Confirmer";
import { convertReceiveTime, getCommaList, getCommaListWithLast } from "../Tool/ToChatlist";
import * as RStyle from "../Style/Styler";
import * as Style from "../Style/StChatlist";

const Container = ({
  iplayer,
  remoteChats,
  remoteVhats,
  clickChat,
  setClickChat,
  toggleCreaterModal,
}) => {
  console.log("DEV:: ChatList");

  let chats = remoteChats.readChats;
  let vhats = remoteVhats.readVhats;
  const [clickList, setClickList] = useState("");
  const [clickVhat, setClickVhat] = useState("");
  const [confirmerModal, setConfirmerModal] = useState(false);

  const toggleConfirmerModal = () => {
    confirmerModal ? setConfirmerModal(false) : setConfirmerModal(true);
  };
  useEffect(() => {
    setClickList(clickChat);
  }, [clickChat]);

  return (
    <Style.ChatlistViewer>
      <Style.ChatlistInViewer>
        <Style.ListChat border="1px dashed #757575" clickList="" onClick={toggleCreaterModal}>
          <RStyle.Text style={RStyle.TextStyle.guide}>새로운 채팅방 만들기</RStyle.Text>
        </Style.ListChat>
        {chats.length
          ? chats.map(({ id, title, players, messages, createdAt }) => (
              <Style.ListChat
                key={id}
                clickList={id === clickList ? id : ""}
                onClick={() => {
                  setClickChat(id);
                  setClickList(id);
                }}
              >
                <RStyle.Text style={RStyle.TextStyle.chaters}>
                  {title === "none" ? getCommaListWithLast(players, iplayer) : title}
                </RStyle.Text>
                <RStyle.Text style={RStyle.TextStyle.lastdate}>
                  {messages.length
                    ? convertReceiveTime(messages[messages.length - 1].createdAt)
                    : convertReceiveTime(createdAt)}
                </RStyle.Text>
                <RStyle.Text style={RStyle.TextStyle.lastmessage}>
                  {messages[messages.length - 1].text}
                </RStyle.Text>
              </Style.ListChat>
            ))
          : false}
        {vhats.length
          ? vhats.map(({ id, chat }) => (
              <Style.ListVhat
                key={id}
                onClick={() => {
                  setClickVhat(id);
                  toggleConfirmerModal();
                }}
              >
                <RStyle.Text style={RStyle.TextStyle.outchaters}>
                  {chat.title === "none" ? getCommaList(chat.players) : chat.title}
                </RStyle.Text>
              </Style.ListVhat>
            ))
          : false}
        {confirmerModal && (
          <Confirmer
            iplayer={iplayer}
            vhats={vhats}
            clickVhat={clickVhat}
            setClickVhat={setClickVhat}
            toggleModal={toggleConfirmerModal}
          />
        )}
      </Style.ChatlistInViewer>
    </Style.ChatlistViewer>
  );
};

export default Container;
