import React from "react";
import { useMutation } from "@apollo/react-hooks";

import { UPDATE_VHAT } from "../Hook/Gql";
import { onVhatOut } from "../Tool/ToChatlist";
import * as RStyle from "../Style/Styler";
import * as Style from "../Style/StUpdater";

const Confirmer = ({ iplayer, vhats, clickVhat, setClickVhat, toggleModal }) => {
  console.log("DEV:: Confirmer");

  const [updateVhatMutation] = useMutation(UPDATE_VHAT);

  const onOutClick = () => {
    onVhatOut({ iplayer, vhats, clickVhat, setClickVhat, updateVhatMutation, toggleModal });
  };

  return (
    <Style.ConfirmViewer>
      <RStyle.CloseButton style={RStyle.ButtonStyle.closemodal} onClick={onOutClick} />
      <RStyle.Text style={RStyle.TextStyle.confirm}>채팅에서 아웃되셨습니다.</RStyle.Text>
    </Style.ConfirmViewer>
  );
};

export default Confirmer;
