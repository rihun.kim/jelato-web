export const getLeftMove = (width) => {
  return window.innerWidth / 2 + 100 - width / 2 + "px";
};

export const getTopMove = (height) => {
  return window.innerHeight / 2 - height / 2 + "px";
};
