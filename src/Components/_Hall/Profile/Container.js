import React, { useState } from "react";

import { getLeftMove, getTopMove } from "./Tool";
import { Viewer, CloseButton } from "./Styler";
import LogoutForm from "./LogoutForm";
import SignoutForm from "./SignoutForm";
import UpdateForm from "./UpdateForm";

const Container = ({ toggleModal }) => {
  const [clickView, setClickView] = useState("logoutView");

  return (
    <Viewer width="700px" height="300px" left={getLeftMove(700)} top={getTopMove(300)}>
      <CloseButton toggleModal={toggleModal} />
      {clickView === "logoutView" ? <LogoutForm setClickView={setClickView} /> : false}
      {clickView === "signoutView" ? <SignoutForm setClickView={setClickView} /> : false}
      {clickView === "updateView" ? <UpdateForm setClickView={setClickView} /> : false}
    </Viewer>
  );
};

export default Container;
