import React, { useState } from "react";

import { useMutation } from "@apollo/react-hooks";

import { InnerSpinner } from "../../Design/Spinner";
import { UPDATE_USER } from "../../../Hooks/ProfileHook";
import { InnerViewer, Button, Text, FormStyle, Input, useInput } from "./Styler";

const UpdateForm = ({ setClickView }) => {
  const [buttonDisable, setButtonDisable] = useState(false);
  const [updateUserMutation] = useMutation(UPDATE_USER);
  const passwordUseInput = useInput("");
  const passwordCheckseInput = useInput("");

  const onComplete = async () => {
    try {
      if (passwordUseInput.value === "" || passwordCheckseInput.value === "") {
        alert("비밀번호 입력해주세요.");
      } else if (passwordUseInput.value !== passwordCheckseInput.value) {
        alert("변경하려는 비밀번호 동일하게 입력해주세요.");
      } else {
        setButtonDisable(true);
        const {
          data: { updateUser: message },
        } = await updateUserMutation({
          variables: {
            password: passwordUseInput.value,
          },
        });
        alert(message);
        setClickView("logoutView");
      }
    } catch (error) {
      alert(error);
    }
  };

  return (
    <InnerViewer width="300px" height="240px">
      <Input margin="0 0 10px 0" placeholder="새 비밀번호" type="password" {...passwordUseInput} />
      <Input
        margin="0 0 10px 0"
        placeholder="새 비밀번호 확인"
        type="password"
        {...passwordCheckseInput}
      />
      <Button disabled={buttonDisable} margin="0 0 10px 0" onClick={onComplete}>
        {buttonDisable === false ? "완료" : <InnerSpinner />}
      </Button>
      <Text style={FormStyle.textbutton} onClick={() => setClickView("logoutView")}>
        취소
      </Text>
    </InnerViewer>
  );
};

export default UpdateForm;
