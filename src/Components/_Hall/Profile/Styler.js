import React, { useState } from "react";

import styled from "styled-components";

import { IoMdClose } from "react-icons/io";

//
export const Viewer = styled.div`
  width: ${(props) => props.width};
  height: ${(props) => props.height};

  background-color: #2b2b28;
  border-radius: 3px;
  box-shadow: 1px 3px 4px rgba(0, 0, 0, 0.19), 0 1px 0 rgba(0, 0, 0, 0.23);
  display: flex;
  justify-content: center;
  left: ${(props) => props.left};
  padding: 30px 30px 30px 30px;
  position: fixed;
  top: ${(props) => props.top};
  z-index: 3;
`;

export const InnerViewer = styled.div`
  width: ${(props) => props.width};
  height: ${(props) => props.height};

  align-items: center;
  display: flex;
  flex-direction: column;
  justify-content: center;
`;

//
export const Button = styled.button`
  width: 200px;
  height: 40px;

  background-color: #fbfbfb;
  border: 0;
  border-radius: 3px;
  box-shadow: 1px 3px 4px rgba(0, 0, 0, 0.19), 0 5px 0 rgba(0, 0, 0, 0.23);
  color: #000000;
  cursor: pointer;
  display: block
  font-size: 14px;
  margin: ${(props) => props.margin};
  text-align: center;

  &:hover {
    background-color: #fdf0f6;
  }
`;

export const CloseButton = ({ toggleModal }) => {
  return (
    <IoMdClose
      style={{
        color: "#ffffff",
        cursor: "pointer",
        position: "absolute",
        right: 10,
        top: 10,
        zIndex: 3,
      }}
      onClick={toggleModal}
    />
  );
};

export const Group = styled.div`
  display: ${(props) => props.display};
`;

export const Text = styled.div`
  color: #ffffff;
  font-size: ${(props) => props.fontSize};
  line-height: 1.6;
  text-align: center;
  margin: ${(props) => props.margin};
`;

//
const InputViewer = styled.input`
  width: 200px;
  height: 35px;

  border: 0 solid #bcbcbc;
  border-radius: 3px;
  box-shadow: 1px 3px 4px rgba(0, 0, 0, 0.19), 0 5px 0 rgba(0, 0, 0, 0.23);
  box-sizing: border-box;
  font-size: 14px;
  margin: ${(props) => props.margin};
  padding: 0 15px 0 15px;
  text-align: center;
`;

export const Input = ({ disabled, margin, onChange, placeholder, type = "text", value }) => (
  <InputViewer
    disabled={disabled}
    defaultValue={value}
    margin={margin}
    onChange={onChange}
    placeholder={placeholder}
    type={type}
  />
);

export const useInput = (prefilled) => {
  const [value, setValue] = useState(prefilled);

  const onChange = (e) => {
    setValue(e.target.value);
  };

  return { onChange, value };
};

//
export const FormStyle = {
  message: {
    fontSize: "14px",
    margin: "0 0 20px 0",
  },
  textbutton: {
    color: "#a0a0a0",
    cursor: "pointer",
    fontSize: "10px",
    textAlign: "center",
  },
};
