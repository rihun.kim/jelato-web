import React, { useState } from "react";

import { useMutation } from "@apollo/react-hooks";

import { InnerSpinner } from "../../Design/Spinner";
import { DELETE_USER } from "../../../Hooks/ProfileHook";
import { DO_LOGOUT } from "../../../Hooks/LobbyHook";
import { InnerViewer, Button, FormStyle, Text, Input, useInput } from "./Styler";

const SignoutForm = ({ setClickView }) => {
  const [buttonDisable, setButtonDisable] = useState(false);
  const [deleteUserMutation] = useMutation(DELETE_USER);
  const [doLogoutMutation] = useMutation(DO_LOGOUT);
  const passwordUseInput = useInput("");

  const onSignout = async () => {
    try {
      if (passwordUseInput.value === "") {
        alert("회원탈퇴 하시려면 비밀번호 입력해주세요.");
      } else {
        setButtonDisable(true);
        const {
          data: { deleteUser: message },
        } = await deleteUserMutation({
          variables: {
            password: passwordUseInput.value,
          },
        });
        if (message.indexOf("[ALERT]") === -1) {
          alert(message);
          await doLogoutMutation();
          window.location.reload();
        } else {
          setButtonDisable(false);
          alert(message);
        }
      }
    } catch (error) {
      alert(error);
    }
  };

  return (
    <InnerViewer width="300px" height="240px">
      <Text style={FormStyle.message}>정말 회원탈퇴 하실건가요?</Text>
      <Input margin="0 0 10px 0" placeholder="비밀번호" type="password" {...passwordUseInput} />
      <Button disabled={buttonDisable} margin="0 0 10px 0" onClick={onSignout}>
        {buttonDisable === false ? "회원탈퇴" : <InnerSpinner />}
      </Button>
      <Text style={FormStyle.textbutton} onClick={() => setClickView("logoutView")}>
        취소
      </Text>
    </InnerViewer>
  );
};

export default SignoutForm;
