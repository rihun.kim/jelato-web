import React, { useState } from "react";

import { useMutation, useQuery } from "@apollo/react-hooks";

import { InnerViewer, FormStyle, Button, Group, Text } from "./Styler";
import { DO_LOGOUT } from "../../../Hooks/LobbyHook";
import { READ_USER } from "../../../Hooks/ProfileHook";

const LogoutForm = ({ setClickView }) => {
  const [buttonDisable, setButtonDisable] = useState(false);
  const { loading, data: remoteUserData } = useQuery(READ_USER);
  const [doLogoutMutation] = useMutation(DO_LOGOUT);

  const onLogout = async () => {
    try {
      setButtonDisable(true);
      await doLogoutMutation();
    } catch (error) {
      alert(error);
      setButtonDisable(false);
    }
  };

  return (
    <InnerViewer width="300px" height="240px">
      <Text style={FormStyle.message}>
        {loading ? false : remoteUserData.readUser.email}
        <br />
        다음에 또 봤으면 좋겠어요.
      </Text>
      <Button disabled={buttonDisable} margin="0 0 10px 0" onClick={onLogout}>
        logout
      </Button>
      <Group display="flex">
        <Text
          style={FormStyle.textbutton}
          margin="0 10px 0 0"
          onClick={() => setClickView("signoutView")}
        >
          회원탈퇴
        </Text>
        <Text style={FormStyle.textbutton} onClick={() => setClickView("updateView")}>
          비밀번호 변경
        </Text>
      </Group>
    </InnerViewer>
  );
};

export default LogoutForm;
