import React, { useState } from "react";
import { Link } from "react-router-dom";

import { Viewer, InnerViewer, Text, ListViewer, ListTitle, List } from "./Styler";

const Container = ({ remoteOwnplayers }) => {
  const players = remoteOwnplayers.readOwnplayers.filter(
    (player) => player.position === "owning" || player.position === "ining"
  );
  const [listVisible, setListVisible] = useState(new Array(players.length).fill(true));
  const list = [
    { name: "스케줄러", url: "/schedule" },
    { name: "공지사항", url: "/notice" },
    { name: "채팅", url: "/chat" },
  ];

  const toggleListVisible = (index) => {
    listVisible[index] = listVisible[index] ? false : true;
    setListVisible([...listVisible]);
  };

  return (
    <Viewer>
      <InnerViewer>
        <Link to="/">
          <Text>[ PLAYGROUND ]</Text>
        </Link>
      </InnerViewer>
      {players.map(({ playground: { title } }, index) => (
        <ListViewer key={index}>
          <ListTitle onClick={() => toggleListVisible(index)}>{title}</ListTitle>
          {listVisible[index]
            ? list.map(({ name, url }) => (
                <Link key={name} to={`${url}/${index}`}>
                  <List>{name}</List>
                </Link>
              ))
            : false}
        </ListViewer>
      ))}
    </Viewer>
  );
};

export default Container;
