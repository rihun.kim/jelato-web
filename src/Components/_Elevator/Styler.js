import styled from "styled-components";

//
export const Viewer = styled.div`
  width: 200px;
  height: 100vh;

  background-color: #2b2b28;
  left: 0;
  overflow-y: auto;
  position: fixed;
`;

export const InnerViewer = styled.div`
  width: 100%;
  height: 200px;

  align-items: center;
  display: flex;
  flex-direction: column;
  justify-content: center;
`;

export const ListViewer = styled.div`
  width: 100%;
  height: ${(props) => props.height};

  margin: 0 0 15px 0;
`;

//
export const ListTitle = styled.div`
  width: 150px;
  height: 25px;

  color: #ffffff;
  cursor: pointer;
  font-size: 13px;
  margin: auto 0 auto 20px;
  overflow: hidden;
  text-overflow: ellipsis;
  white-space: nowrap;
`;

export const List = styled.div`
  height: 22px;

  color: #cacaca;
  cursor: pointer;
  font-size: 13px;
  margin: auto 0 auto 30px;

  &:hover {
    background-color: #b19fa7;
  }
`;

export const Text = styled.div`
  color: #ffffff;
  font-size: 13px;
`;
