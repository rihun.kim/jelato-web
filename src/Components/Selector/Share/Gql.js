import { gql } from '@apollo/client';

export const READ_USER = gql`
  query {
    readUser {
      id
      email
      notifiers {
        id
        notifications {
          id
          addressee {
            id
          }
          title
          story
          createdAt
          updatedAt
        }
      }
      players {
        id
        name
        position
        playground {
          id
          barcode
          color
          status
          title
          story
        }
        createdAt
      }
    }
  }
`;

export const SUBSCRIBE_NOTIFICATIONS = gql`
  subscription subscribeNotifications($notifierIds: [String]) {
    subscribeNotifications(notifierIds: $notifierIds) {
      mutationType
      notifierId
      node {
        id
        addressee {
          id
        }
        title
        story
        createdAt
        updatedAt
      }
    }
  }
`;
