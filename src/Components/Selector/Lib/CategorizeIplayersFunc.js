export const categorizeIplayers = _iplayers => {
  let categorizedIplayers = { own: [], in: [], wait: [] };

  for (const iplayer of _iplayers) {
    if (iplayer.position === 'in') categorizedIplayers.in.push(iplayer);
    else if (iplayer.position === 'own') categorizedIplayers.own.push(iplayer);
    else if (iplayer.position === 'wait') categorizedIplayers.wait.push(iplayer);
  }

  return categorizedIplayers;
};
