import React from 'react';

import Elevator from '../Screen/Elevator/Container/Main';
import Npc from '../Screen/Npc/Container/Main';
import { GlobalStyle, Viewer } from './Style/StRender';

const Render = ({ main }) => {
  return (
    <>
      <GlobalStyle />
      <Viewer>
        <Npc />
        <Elevator />
        {main}
      </Viewer>
    </>
  );
};

export default Render;
