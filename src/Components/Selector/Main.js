import React from 'react';

import Fetcher from './Fetcher/Main';
import { UseQueryUser } from './Fetcher/Request';

const Main = ({ main }) => {
  const remoteUser = UseQueryUser();

  return remoteUser && <Fetcher main={main} remoteUser={remoteUser} />;
};

export default Main;
