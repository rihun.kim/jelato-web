import { cloneDeep } from '@apollo/client/utilities';

export const memoRealtimeNotifications = (remoteNotifiers, realtimeNotifications) => {
  const realtimeNotifierId = realtimeNotifications.subscribeNotifications.notifierId;
  const realtimeNotification = realtimeNotifications.subscribeNotifications.node;

  for (const remoteNotifier of remoteNotifiers) {
    if (remoteNotifier.id === realtimeNotifierId) {
      remoteNotifier.notifications.push(cloneDeep(realtimeNotification));
    }
  }
};
