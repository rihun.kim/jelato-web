import React, { useMemo } from 'react';

import { NotificationContext } from '../../Share/Context';
import { memoRealtimeNotifications } from './Memo';
import { UseSubscribeNotifications } from './Request';

const Subscribe = ({ children, remoteNotifiers }) => {
  const notifierIds = remoteNotifiers.map(notifier => notifier.id);
  const realtimeNotifications = UseSubscribeNotifications(notifierIds);

  useMemo(() => {
    if (remoteNotifiers && realtimeNotifications) memoRealtimeNotifications(remoteNotifiers, realtimeNotifications);
  }, [remoteNotifiers, realtimeNotifications]);

  return <NotificationContext.Provider value={{ remoteNotifiers }}>{children}</NotificationContext.Provider>;
};

export default Subscribe;
