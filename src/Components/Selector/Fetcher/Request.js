import { useSubscription, useQuery } from '@apollo/client';
import { cloneDeep } from '@apollo/client/utilities';

import { READ_USER, SUBSCRIBE_NOTIFICATIONS } from '../Share/Gql';

export const UseQueryUser = () => {
  const { loading, data } = useQuery(READ_USER, {
    fetchPolicy: 'no-cache',
  });

  return loading || data === undefined ? false : cloneDeep(data);
};

export const UseSubscribeNotifications = notifierIds => {
  const { loading, data } = useSubscription(SUBSCRIBE_NOTIFICATIONS, {
    variables: { notifierIds: notifierIds },
  });

  return loading || data === undefined ? false : data;
};
