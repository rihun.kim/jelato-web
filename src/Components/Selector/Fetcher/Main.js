import React from 'react';

import Render from '../Render';
import Subscribe from './Subscribe';
import { categorizeIplayers } from '../Lib/CategorizeIplayersFunc';
import { MainContext } from '../../Share/Context';

const Fetcher = ({ main, remoteUser }) => {
  const user = remoteUser.readUser;

  const email = user.email;
  const notifiers = user.notifiers;
  const iplayers = user.players;
  const fiplayers = categorizeIplayers(iplayers);

  return (
    <Subscribe remoteNotifiers={notifiers}>
      <MainContext.Provider value={{ USER: { email }, IPLAYERS: { iplayers, fiplayers } }}>
        <Render main={main} />
      </MainContext.Provider>
    </Subscribe>
  );
};

export default Fetcher;
