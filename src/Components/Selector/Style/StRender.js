import styled, { createGlobalStyle } from 'styled-components';

export const GlobalStyle = createGlobalStyle`
  body {
    margin: 0 0 0 0;
  }
  a {
    cursor: pointer;
    text-decoration: none;
  }
  ul {
    list-style-type: none;
    padding: 0;
  }
  button,
  input,
  textarea,
  li {
    outline: none;
  }
  * {
    box-sizing: border-box;
    font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Oxygen, Ubuntu, Cantarell, 'Open Sans', 'Helvetica Neue', sans-serif;
  }
  button {
    cursor: pointer;
  }
`;

export const Viewer = styled.div`
  width: 100vw;
  height: 100vh;

  display: flex;
  flex-direction: row;
`;
