import React from "react";
import { BrowserRouter } from "react-router-dom";
import { useQuery } from "@apollo/react-hooks";

import { IS_LOGIN } from "../../Hooks/LobbyHook";
import { LobbyRoute, PlaygroundRoute } from "./Routes";
import { Spinner } from "../_Design/Spinner";

const AppRouter = () => {
  const { loading, data } = useQuery(IS_LOGIN);

  if (loading) return <Spinner />;

  return <BrowserRouter>{data.isLogin ? <PlaygroundRoute /> : <LobbyRoute />}</BrowserRouter>;
};

export default AppRouter;
