import React, { useState } from "react";
import { Redirect, Route, Switch } from "react-router-dom";
import { useQuery } from "react-apollo";

import Lobby from "../Lobby/Container";
import Elevator from "../_Elevator/Container";
import Playground from "../Hall/Playground/Container";
import Schedule from "../Hall/Schedule/Container";
import Notice from "../Hall/Notice/Container";
import Chat from "../Hall/Chat/Arbiter";
// import Memo from "../Hall/Memo/Container";

import { Spinner } from "../_Design/Spinner";
import { READ_OWNPLAYERS } from "../../Hooks/PlaygroundHook";

export const LobbyRoute = () => {
  return (
    <Switch>
      <Route exact path="/" component={Lobby} />
    </Switch>
  );
};

export const PlaygroundRoute = () => {
  const [, setRouteRefresh] = useState("");
  const { loading, data: remoteOwnplayers } = useQuery(READ_OWNPLAYERS, {
    fetchPolicy: "no-cache",
  });

  if (loading) return <Spinner />;
  return (
    <>
      <Elevator remoteOwnplayers={remoteOwnplayers} />
      <Switch>
        <Route
          exact
          path="/"
          render={(props) => (
            <Playground
              {...props}
              setRouteRefresh={setRouteRefresh}
              remoteOwnplayers={remoteOwnplayers}
            />
          )}
        />
        <Route
          exact
          path="/schedule/:index"
          render={(props) => <Schedule {...props} remoteOwnplayers={remoteOwnplayers} />}
        />
        <Route
          exact
          path="/notice/:index"
          render={(props) => <Notice {...props} remoteOwnplayers={remoteOwnplayers} />}
        />
        <Route
          exact
          path="/chat/:index"
          render={(props) => <Chat {...props} remoteOwnplayers={remoteOwnplayers} />}
        />
        <Redirect from="*" to="/" />
      </Switch>
    </>
  );
};
