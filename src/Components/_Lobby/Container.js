import React, { useState } from "react";

import { Viewer } from "./Styler";
import LoginForm from "./LoginForm";
import SignupForm from "./SignupForm";
import FindForm from "./FindForm";

const Container = () => {
  const [clickView, setClickView] = useState("loginView");

  return (
    <Viewer width="100vw" height="100vh">
      {clickView === "loginView" ? <LoginForm setClickView={setClickView} /> : false}
      {clickView === "signupView" ? <SignupForm setClickView={setClickView} /> : false}
      {clickView === "findView" ? <FindForm setClickView={setClickView} /> : false}
    </Viewer>
  );
};

export default Container;
