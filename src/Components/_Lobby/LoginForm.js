import React, { useState } from "react";
import { useMutation } from "@apollo/react-hooks";

import { Button, Text, Group, FormStyle, Viewer, Input, useInput } from "./Styler";
import { InnerSpinner } from "../_Design/Spinner";
import { DO_LOGIN, REQUEST_TOKEN } from "../../_Hooks/LobbyHook";
import { validateEmailAddress, encryptPassword } from "./Tool";

const LoginForm = ({ setClickView }) => {
  const [buttonDisable, setButtonDisable] = useState(false);
  const [requestTokenMutation] = useMutation(REQUEST_TOKEN);
  const [doLoginMutation] = useMutation(DO_LOGIN);
  const emailUseInput = useInput("");
  const passwordUseInput = useInput("");

  const onLogin = async () => {
    try {
      if ((emailUseInput.value === "") | !validateEmailAddress(emailUseInput.value)) {
        alert("유효한 이메일 입력해주세요.");
      } else if (passwordUseInput.value === "") {
        alert("비밀번호 입력해주세요.");
      } else {
        setButtonDisable(true);
        const {
          data: { requestUserToken: token },
        } = await requestTokenMutation({
          variables: {
            email: emailUseInput.value,
            password: await encryptPassword(passwordUseInput.value),
          },
        });

        if (token.indexOf("[ALERT]") === -1) {
          await doLoginMutation({ variables: { token } });
          window.location.reload();
        } else {
          setButtonDisable(false);
          alert(token);
        }
      }
    } catch (error) {
      alert(error);
    }
  };

  return (
    <Viewer width="200px">
      <Input margin="0 0 10px 0" placeholder="email" {...emailUseInput} />
      <Input margin="0 0 30px 0" placeholder="password" type="password" {...passwordUseInput} />
      <Button disabled={buttonDisable} margin="0 0 10px 0" onClick={onLogin}>
        {buttonDisable ? <InnerSpinner /> : "login"}
      </Button>
      <Group display="flex">
        <Text
          style={FormStyle.textbutton}
          margin="0 10px 0 0"
          onClick={() => setClickView("signupView")}
        >
          회원가입
        </Text>
        <Text style={FormStyle.textbutton} onClick={() => setClickView("findView")}>
          비밀번호 찾기
        </Text>
      </Group>
    </Viewer>
  );
};

export default LoginForm;
