import React, { useState } from "react";
import { useMutation } from "@apollo/react-hooks";

import { InnerSpinner } from "../Design/Spinner";
import { CREATE_USER, CREATE_VSER } from "../../_Hooks/LobbyHook";
import { Button, Text, Viewer, Input, useInput, FormStyle } from "./Styler";
import { validateEmailAddress, encryptPassword } from "./Tool";

const SignupForm = ({ setClickView }) => {
  const [buttonDisable, setButtonDisable] = useState(false);
  const [inputDisable, setInputDisable] = useState(false);
  const [moreView, setMoreView] = useState(false);
  const [createUserMutation] = useMutation(CREATE_USER);
  const [createVserMutation] = useMutation(CREATE_VSER);
  const emailUseInput = useInput("");
  const codeUseInput = useInput("");
  const passwordUseInput = useInput("");
  const passwordConfirmUseInput = useInput("");

  const onComplete = async () => {
    try {
      if (passwordUseInput.value === "" || passwordConfirmUseInput.value === "") {
        alert("비밀번호 입력해주세요.");
      } else if (passwordUseInput.value !== passwordConfirmUseInput.value) {
        alert("비밀번호 동일하게 입력해주세요.");
      } else {
        setButtonDisable(true);
        let password = await encryptPassword(passwordUseInput.value);
        const {
          data: { createUser: message },
        } = await createUserMutation({
          variables: {
            email: emailUseInput.value,
            password: password,
            code: codeUseInput.value,
          },
        });
        alert(message);
        if (message.indexOf("[ALERT]") === -1) {
          setClickView("loginView");
          window.location.reload();
        }
      }
    } catch (error) {
      alert(error);
    }
  };
  const onSend = async () => {
    if (!validateEmailAddress(emailUseInput.value)) {
      alert("유효한 이메일 입력하세요.");
    } else {
      setButtonDisable(true);
      const {
        data: { createVser: message },
      } = await createVserMutation({
        variables: {
          email: emailUseInput.value,
        },
      });
      if (message.indexOf("[ALERT]") === -1) {
        setMoreView(true);
        setInputDisable(true);
      }
      setButtonDisable(false);
      alert(message);
    }
  };

  return (
    <Viewer width="200px">
      <Input
        disabled={inputDisable}
        margin="0 0 10px 0"
        placeholder="가입하려는 이메일"
        {...emailUseInput}
      />
      {moreView ? (
        <>
          <Input margin="0 0 10px 0" placeholder="인증코드" {...codeUseInput} />
          <Input margin="0 0 10px 0" placeholder="비밀번호" type="password" {...passwordUseInput} />
          <Input
            margin="0 0 30px 0"
            placeholder="비밀번호 확인"
            type="password"
            {...passwordConfirmUseInput}
          />
          <Button disabled={buttonDisable} margin="0 0 10px 0" onClick={onComplete}>
            {buttonDisable === false ? "완료" : <InnerSpinner />}
          </Button>
        </>
      ) : (
          <Button disabled={buttonDisable} margin="20px 0 10px 0" onClick={onSend}>
            {buttonDisable === false ? "인증코드 발송" : <InnerSpinner />}
          </Button>
        )}
      <Text style={FormStyle.textbutton} onClick={() => setClickView("loginView")}>
        취소
      </Text>
    </Viewer>
  );
};

export default SignupForm;
