import React, { useState } from "react";

import { useMutation } from "@apollo/react-hooks";

import { Button, Text, FormStyle, Viewer, Input, useInput } from "./Styler";
import { InnerSpinner } from "../Design/Spinner";
import { VERIFY_CODE, VERIFY_USER } from "../../_Hooks/LobbyHook";
import { validateEmailAddress } from "./Tool";

const FindForm = ({ setClickView }) => {
  const [buttonDisable, setButtonDisable] = useState(false);
  const [inputDisable, setInputDisable] = useState(false);
  const [moreView, setMoreView] = useState(false);
  const [verifyUserMutation] = useMutation(VERIFY_USER);
  const [verifyCodeMutation] = useMutation(VERIFY_CODE);
  const emailUseInput = useInput("");
  const codeUseInput = useInput("");
  const passwordUseInput = useInput("");
  const passwordConfirmUseInput = useInput("");

  const onComplete = async () => {
    try {
      if (passwordUseInput.value === "" || passwordConfirmUseInput.value === "") {
        alert("비밀번호 입력해주세요.");
      } else if (passwordUseInput.value !== passwordConfirmUseInput.value) {
        alert("비밀번호 동일하게 입력해주세요.");
      } else {
        setButtonDisable(true);
        const {
          data: { verifyCode: message },
        } = await verifyCodeMutation({
          variables: {
            email: emailUseInput.value,
            password: passwordUseInput.value,
            code: codeUseInput.value,
          },
        });
        alert(message);
        if (message.indexOf("[ALERT]") === -1) {
          setClickView("loginView");
          window.location.reload();
        }
      }
    } catch (error) {
      alert(error);
    }
  };
  const onSend = async () => {
    if (!validateEmailAddress(emailUseInput.value)) {
      alert("유효한 이메일 입력하세요.");
    } else {
      setButtonDisable(true);
      const {
        data: { verifyUser: message },
      } = await verifyUserMutation({
        variables: {
          email: emailUseInput.value,
        },
      });
      if (message.indexOf("[ALERT]") === -1) {
        setMoreView(true);
        setInputDisable(true);
      }
      setButtonDisable(false);
      alert(message);
    }
  };

  return (
    <Viewer width="200px">
      <Input
        disabled={inputDisable}
        margin="0 0 10px 0"
        placeholder="비밀번호 찾으려는 이메일"
        {...emailUseInput}
      />
      {moreView ? (
        <>
          <Input margin="0 0 10px 0" placeholder="인증코드" {...codeUseInput} />
          <Input
            margin="0 0 10px 0"
            placeholder="새 비밀번호"
            type="password"
            {...passwordUseInput}
          />
          <Input
            margin="0 0 30px 0"
            placeholder="새 비밀번호 확인"
            type="password"
            {...passwordConfirmUseInput}
          />
          <Button margin="0 0 10px 0" disabled={buttonDisable} onClick={onComplete}>
            {buttonDisable ? <InnerSpinner /> : "변경"}
          </Button>
        </>
      ) : (
          <Button margin="20px 0 10px 0" disabled={buttonDisable} onClick={onSend}>
            {buttonDisable ? <InnerSpinner /> : "인증코드 발송"}
          </Button>
        )}
      <Text style={FormStyle.textbutton} onClick={() => setClickView("loginView")}>
        취소
      </Text>
    </Viewer>
  );
};

export default FindForm;
