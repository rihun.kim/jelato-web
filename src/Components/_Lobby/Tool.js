import crypto from "crypto";

import "../../env";

export const validateEmailAddress = (email) => {
  //eslint-disable-next-line
  const expression = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return expression.test(String(email).toLowerCase());
};

export const encryptPassword = (password) => {
  return new Promise((resolve, reject) => {
    resolve(crypto.createHash("sha512").update(password).digest("hex"));
  });
};
