import React, { useState } from "react";

import styled from "styled-components";

//
export const Viewer = styled.div`
  width: ${(props) => props.width};
  height: ${(props) => props.height};

  align-items: center;
  background-color: #2b2b28;
  display: flex;
  flex-direction: column;
  justify-content: center;
`;

//
export const Button = styled.button`
  width: 200px;
  height: 40px;

  background-color: #fbfbfb;
  border: 0;
  border-radius: 3px;
  box-shadow: 1px 3px 4px rgba(0, 0, 0, 0.19), 0 5px 0 rgba(0, 0, 0, 0.23);
  color: #000000;
  cursor: pointer;
  display: block
  font-size: 14px;
  margin: ${(props) => props.margin};
  text-align: center;

  &:hover {
    background-color: #fdf0f6;
  }
`;

export const Group = styled.div`
  display: ${(props) => props.display};
`;

export const Text = styled.div`
  font-size: ${(props) => props.fontSize};
  font-weight: ${(props) => props.fontWeight};

  margin: ${(props) => props.margin};
`;

//
const InputViewer = styled.input`
  width: 200px;
  height: 35px;

  border: 0 solid #bcbcbc;
  border-radius: 3px;
  box-shadow: 1px 3px 4px rgba(0, 0, 0, 0.19), 0 5px 0 rgba(0, 0, 0, 0.23);
  box-sizing: border-box;
  font-size: 14px;
  margin: ${(props) => props.margin};
  padding: 0 15px 0 15px;
  text-align: center;
`;

export const Input = ({ disabled, margin, onChange, placeholder, type = "text", value }) => (
  <InputViewer
    disabled={disabled}
    defaultValue={value}
    margin={margin}
    onChange={onChange}
    placeholder={placeholder}
    type={type}
  />
);

export const useInput = (prefilled) => {
  const [value, setValue] = useState(prefilled);

  const onChange = (e) => {
    setValue(e.target.value);
  };

  return { onChange, value };
};

//
export const FormStyle = {
  textbutton: {
    color: "#a0a0a0",
    cursor: "pointer",
    fontSize: "10px",
    textAlign: "center",
  },
};
