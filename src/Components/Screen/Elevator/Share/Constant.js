export const ELEVATOR_DEFAULT_WIDTH = '110';
export const ELEVATOR_DEFAULT_NAME = '[ PLAYGROUND ]';
export const ELEVATOR_SHORT_NAME = '[ * ]';

export const SCHEDULE_NAME = '스케줄러';
export const NOTICE_NAME = '공지사항';
export const CHAT_NAME = '채팅';

export const SCHEDULE_URL = '/schedule';
export const NOTICE_URL = '/notice';
export const CHAT_URL = '/chat';
