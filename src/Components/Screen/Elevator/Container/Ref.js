import { createRef } from 'react';

import { ELEVATOR_DEFAULT_WIDTH, ELEVATOR_DEFAULT_NAME, ELEVATOR_SHORT_NAME } from '../Share/Constant';

export const elevatorRef = createRef();

export const reconcileElevator = (ref, setLogo) => {
  if (ref.current) {
    if (+ref.current.offsetWidth <= +ELEVATOR_DEFAULT_WIDTH) setLogo(ELEVATOR_SHORT_NAME);
    else setLogo(ELEVATOR_DEFAULT_NAME);
  }
};
