import React, { useEffect, useContext, useState } from 'react';
import EQ from 'css-element-queries';

import { HomeList, PlaygroundList } from './List';
import { elevatorRef, reconcileElevator } from './Ref';
import { MainContext } from '../../../Share/Context';
import { Viewer } from './Style/StMain';
import { ELEVATOR_DEFAULT_NAME } from '../Share/Constant';

const Main = () => {
  const { IPLAYERS } = useContext(MainContext);
  const [logo, setLogo] = useState(ELEVATOR_DEFAULT_NAME);
  const playgrounds = IPLAYERS.fiplayers.own.map(fiplayer => fiplayer.playground);
  playgrounds.push(...IPLAYERS.fiplayers.in.map(fiplayer => fiplayer.playground));

  useEffect(() => {
    EQ.ResizeSensor(elevatorRef.current, () => reconcileElevator(elevatorRef, setLogo));
  });

  return (
    <Viewer ref={elevatorRef}>
      <HomeList logo={logo} />
      <PlaygroundList playgrounds={playgrounds} />
    </Viewer>
  );
};

export default Main;
