import styled from 'styled-components';

export const HomeListViewer = styled.div`
  width: 100%;
  height: 200px;

  display: flex;
  align-items: center;
  flex-direction: column;
  justify-content: center;

  a {
    color: #ffffff;
    font-size: 13px;
    text-overflow: ellipsis;
    white-space: nowrap;
  }
`;

export const PlaygroundListViewer = styled.div`
  width: 100%;
  height: ${props => props.height};

  margin: 0 0 15px 0;
`;

export const Title = styled.div`
  max-width: 270px;
  height: 25px;

  color: #ffffff;
  cursor: pointer;
  font-size: 13px;
  margin: auto 0 auto 20px;
  overflow-y: hidden;
  text-overflow: ellipsis;
  white-space: nowrap;
`;

export const Module = styled.div`
  max-width: 270px;
  height: 22px;

  color: #cacaca;
  cursor: pointer;
  font-size: 13px;
  margin: auto 0 auto 30px;
  overflow-y: hidden;
  text-overflow: ellipsis;
  white-space: nowrap;

  &:hover {
    background-color: rgb(177, 159, 167, 0.5);
  }
`;
