import styled from 'styled-components';

import { MAIN_CHOCO_COLOR } from '../../Share/StConstant';

export const Viewer = styled.div`
  width: 200px;
  min-width: 60px;
  max-width: 300px;
  height: 100%;

  background-color: ${MAIN_CHOCO_COLOR};
  overflow-y: auto;
  resize: horizontal;
`;
