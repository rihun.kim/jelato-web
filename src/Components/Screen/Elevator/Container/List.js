import React, { useState } from 'react';
import { Link } from 'react-router-dom';
import { CHAT_NAME, CHAT_URL, NOTICE_NAME, NOTICE_URL, SCHEDULE_NAME, SCHEDULE_URL } from '../Share/Constant';

import { HomeListViewer, PlaygroundListViewer, Title, Module } from './Style/StList';

export const HomeList = ({ logo }) => {
  return (
    <HomeListViewer>
      <Link to="/">{logo}</Link>
    </HomeListViewer>
  );
};

export const PlaygroundList = ({ playgrounds }) => {
  const [listVisible, setListVisible] = useState(new Map(playgrounds.map(playground => [playground.barcode, true])));
  const list = [
    { name: SCHEDULE_NAME, url: SCHEDULE_URL },
    { name: NOTICE_NAME, url: NOTICE_URL },
    { name: CHAT_NAME, url: CHAT_URL },
  ];

  const toggleListVisible = barcode => {
    listVisible.set(barcode, !listVisible.get(barcode));
    setListVisible(new Map(listVisible));
  };

  return (
    <>
      {playgrounds.map(playground => (
        <PlaygroundListViewer key={playground.id}>
          <Title onClick={() => toggleListVisible(playground.barcode)}>{playground.title}</Title>
          {listVisible.get(playground.barcode) &&
            list.map(({ name, url }) => (
              <Link key={name} to={`${url}/${playground.barcode}`}>
                <Module>{name}</Module>
              </Link>
            ))}
        </PlaygroundListViewer>
      ))}
    </>
  );
};
