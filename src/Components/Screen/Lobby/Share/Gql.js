import { gql } from '@apollo/client';

export const REQUEST_USERTOKEN = gql`
  mutation requestUserToken($email: String!, $password: String!) {
    requestUserToken(email: $email, password: $password)
  }
`;

export const CREATE_VSER = gql`
  mutation createVser($email: String!) {
    createVser(email: $email)
  }
`;

export const CREATE_USER = gql`
  mutation createUser($email: String!, $password: String!, $authword: String!) {
    createUser(email: $email, password: $password, authword: $authword)
  }
`;

export const VERIFY_USER = gql`
  mutation verifyUser($email: String!) {
    verifyUser(email: $email)
  }
`;

export const UPDATE_USERPASSWORD = gql`
  mutation updateUserPassword($email: String!, $password: String!, $authword: String!) {
    updateUserPassword(email: $email, password: $password, authword: $authword)
  }
`;
