import crypto from 'crypto';

export const encryptPassword = password => {
  return new Promise((resolve, reject) => {
    resolve(crypto.createHash('sha512').update(password).digest('hex'));
  });
};
