export const MAIN_CHOCO_COLOR = '#2b2b28';
export const MAIN_WHITE_COLOR = '#fbfbfb';
export const MAIN_PINK_COLOR = '#fdf0f6';

export const MAIN_PINK_HOVER_COLOR = '#ffe6f1';
