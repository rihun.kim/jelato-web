import React, { useState } from 'react';
import { useMutation } from '@apollo/client';

import { Viewer } from './Resource/Style/StMain';
import { useInput, Input } from './Input';
import { onSend, onComplete, BackButton, SendButton } from './Button';
import { CREATE_VSER, CREATE_USER } from '../Share/Gql';

const Main = ({ setClickView }) => {
  const [emailInputAble, setEmailInputAble] = useState(true);
  const [etcInputAble, setEtcInputAble] = useState(true);
  const [buttonAble, setButtonAble] = useState(true);
  const [moreView, setMoreView] = useState(false);
  const [createVserMutation] = useMutation(CREATE_VSER);
  const [createUserMutation] = useMutation(CREATE_USER);

  const inputProps = {
    emailUseInput: useInput('가입하려는 email'),
    authwordUseInput: useInput('인증코드'),
    passwordUseInput: useInput('비밀번호'),
    passwordConfirmUseInput: useInput('비밀번호 확인'),
  };

  const onBackClick = () => {
    setClickView('loginView');
  };
  const onSendClick = () => {
    onSend(inputProps, setEmailInputAble, setButtonAble, setMoreView, createVserMutation);
  };
  const onCompleteClick = () => {
    onComplete(inputProps, setEtcInputAble, setButtonAble, setClickView, createUserMutation);
  };

  return (
    <Viewer>
      <Input type="text" inputAble={emailInputAble} {...inputProps.emailUseInput} />
      {moreView ? (
        <>
          <Input type="text" inputAble={etcInputAble} {...inputProps.authwordUseInput} />
          <Input type="password" inputAble={etcInputAble} {...inputProps.passwordUseInput} />
          <Input type="password" inputAble={etcInputAble} {...inputProps.passwordConfirmUseInput} />
          <SendButton buttonAble={buttonAble} placeholder="완료" onClick={onCompleteClick} />
        </>
      ) : (
        <SendButton buttonAble={buttonAble} placeholder="인증코드 발송" onClick={onSendClick} />
      )}
      <BackButton onClick={onBackClick} />
    </Viewer>
  );
};

export default Main;
