import React from 'react';

import { BackButtonViewer, SendButtonViewer } from './Resource/Style/StButton';
import { validateEmail } from '../Share/Validate';

export const onSend = async (props, setEmailInputAble, setButtonAble, setMoreView, createVserMutation) => {
  try {
    if (!validateEmail(props.emailUseInput.value)) {
      alert('유효한 이메일 입력하세요.');
      return;
    }

    setEmailInputAble(false);
    setButtonAble(false);

    const {
      data: { createVser: vser },
    } = await createVserMutation({ variables: { email: props.emailUseInput.value } });

    if (vser.includes('[ALERT]')) {
      alert(vser);
      setEmailInputAble(true);
    } else setMoreView(true);

    setButtonAble(true);
  } catch (error) {
    alert(error);
  }
};

export const onComplete = async (props, setEtcInputAble, setButtonAble, setClickView, createUserMutation) => {
  try {
    if (props.passwordUseInput.value === '') {
      alert('비밀번호 입력해주세요.');
      return;
    } else if (props.passwordUseInput.value !== props.passwordConfirmUseInput.value) {
      alert('비밀번호 동일하게 입력해주세요.');
      return;
    }

    setEtcInputAble(false);
    setButtonAble(false);

    const {
      data: { createUser: user },
    } = await createUserMutation({
      variables: {
        email: props.emailUseInput.value,
        password: props.passwordUseInput.value,
        authword: props.authwordUseInput.value,
      },
    });

    alert(user);
    if (user.includes('[ALERT]')) {
      setEtcInputAble(true);
      setButtonAble(true);
    } else setClickView('loginView');
  } catch (e) {
    alert(e);
  }
};

export const BackButton = ({ onClick }) => <BackButtonViewer onClick={onClick}>뒤로가기</BackButtonViewer>;

export const SendButton = ({ buttonAble, placeholder, onClick }) => (
  <SendButtonViewer onClick={onClick} disabled={!buttonAble}>
    {placeholder}
  </SendButtonViewer>
);
