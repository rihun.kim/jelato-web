import React from 'react';

import { LoginButtonViewer, SignupButtonViewer, FindButtonViewer, ButtonGroupViewer } from './Resource/Style/StButton';
import { validateEmail } from '../Share/Validate';
// import { encryptPassword } from "../Share/Encrypt";

export const onLogin = async (props, setInputAble, requestUserTokenMutation) => {
  try {
    if (props.emailUseInput.value === '' || !validateEmail(props.emailUseInput.value)) {
      alert('유효한 이메일 입력해주세요.');
      return;
    } else if (props.passwordUseInput.value === '') {
      alert('비밀번호 입력해주세요.');
      return;
    }

    setInputAble(false);

    const {
      data: { requestUserToken: userToken },
    } = await requestUserTokenMutation({
      variables: {
        email: props.emailUseInput.value,
        password: props.passwordUseInput.value,
      },
    });

    if (userToken.includes('[ALERT]')) {
      alert(userToken);
      setInputAble(true);
    } else {
      props.doLogin(props.emailUseInput.value, userToken);
      window.location.reload();
    }
  } catch (e) {
    alert(e);
  }
};

export const LoginButton = ({ buttonAble, onClick }) => (
  <LoginButtonViewer disabled={!buttonAble} onClick={onClick}>
    login
  </LoginButtonViewer>
);

export const SignupButton = ({ onClick }) => <SignupButtonViewer onClick={onClick}>회원 가입</SignupButtonViewer>;

export const FindButton = ({ onClick }) => <FindButtonViewer onClick={onClick}>ID/PW 찾기</FindButtonViewer>;

export const ButtonGroup = ({ children }) => <ButtonGroupViewer>{children}</ButtonGroupViewer>;
