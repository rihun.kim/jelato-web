import React, { useState } from 'react';
import { useMutation } from '@apollo/client';

import { Viewer } from './Resource/Style/StMain';
import { useInput, Input } from './Input';
import { onLogin, LoginButton, SignupButton, FindButton, ButtonGroup } from './Button';
import { REQUEST_USERTOKEN } from '../Share/Gql';
import { useDoLogin } from '../../../../Auth/Context';

const Main = ({ setClickView }) => {
  const [inputAble, setInputAble] = useState(true);
  const [requestUserTokenMutation] = useMutation(REQUEST_USERTOKEN);

  const inputProps = {
    emailUseInput: useInput('email'),
    passwordUseInput: useInput('password'),
    doLogin: useDoLogin(),
  };

  const onSignupClick = () => {
    setClickView('signupView');
  };
  const onFindClick = () => {
    setClickView('findView');
  };
  const onLoginClick = () => {
    onLogin(inputProps, setInputAble, requestUserTokenMutation);
  };

  return (
    <Viewer>
      <Input type="text" inputAble={inputAble} {...inputProps.emailUseInput} />
      <Input type="password" inputAble={inputAble} {...inputProps.passwordUseInput} onClick={onLoginClick} />
      <LoginButton buttonAble={inputAble} onClick={onLoginClick} />
      <ButtonGroup>
        <SignupButton onClick={onSignupClick} />
        <FindButton onClick={onFindClick} />
      </ButtonGroup>
    </Viewer>
  );
};

export default Main;
