import styled from 'styled-components';

import { MAIN_WHITE_COLOR } from '../../../Share/StConstant';

export const InputViewer = styled.input`
  width: 250px;
  height: 40px;

  border: 0;
  background-color: ${MAIN_WHITE_COLOR};
  border-radius: 2px;
  font-size: 14px;
  margin: 0 0 1px 0;
  padding: 0 15px 0 15px;
  text-align: center;
`;
