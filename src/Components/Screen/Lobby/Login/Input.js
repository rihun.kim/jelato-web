import React, { useState } from 'react';

import { InputViewer } from './Resource/Style/StInput';

export const useInput = prefill => {
  const [value, setValue] = useState('');
  const placeholder = prefill;

  const onChange = e => setValue(e.target.value);

  return { placeholder, value, onChange };
};

const onKeyDown = (e, onClick) => {
  if (e.keyCode === 13) onClick();
};

export const Input = ({ inputAble = true, onChange, onClick, placeholder, type, value }) => (
  <InputViewer
    disabled={!inputAble}
    defaultValue={value}
    onChange={onChange}
    onKeyDown={e => onKeyDown(e, onClick)}
    placeholder={placeholder}
    type={type}
  />
);
