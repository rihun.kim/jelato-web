import React, { useState } from 'react';

import Login from '../Login/Main';
import Signup from '../Signup/Main';
import Find from '../Find/Main';
import { GlobalStyle, Viewer } from './Style/StMain';

const Main = () => {
  const [clickView, setClickView] = useState('loginView');

  return (
    <>
      <GlobalStyle />
      <Viewer>
        {clickView === 'loginView' ? <Login setClickView={setClickView} /> : false}
        {clickView === 'signupView' ? <Signup setClickView={setClickView} /> : false}
        {clickView === 'findView' ? <Find setClickView={setClickView} /> : false}
      </Viewer>
    </>
  );
};

export default Main;
