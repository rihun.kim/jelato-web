import React from 'react';

import { BackButtonViewer, SendButtonViewer } from './Resource/Style/StButton';
import { validateEmail } from '../Share/Validate';

export const onSend = async (props, setEmailInputAble, setButtonAble, setMoreView, verifyUserMutation) => {
  try {
    if (!validateEmail(props.value)) {
      alert('유효한 이메일 입력하세요.');
      return;
    }

    setEmailInputAble(false);
    setButtonAble(false);

    const {
      data: { verifyUser: user },
    } = await verifyUserMutation({ variables: { email: props.value } });

    if (user.includes('[ALERT]')) {
      alert(user);
      setEmailInputAble(true);
    } else setMoreView(true);
    setButtonAble(true);
  } catch (e) {
    alert(e);
  }
};

export const onComplete = async (props, setEtcInputAble, setButtonAble, setClickView, updateUserPasswordMutation) => {
  try {
    if (props.passwordUseInput.value === '') {
      alert('비밀번호 입력해주세요.');
      return;
    } else if (props.passwordUseInput.value !== props.passwordConfirmUseInput.value) {
      alert('비밀번호 동일하게 입력해주세요.');
      return;
    }

    setEtcInputAble(false);
    setButtonAble(false);

    const {
      data: { updateUserPassword: userPassword },
    } = await updateUserPasswordMutation({
      variables: {
        email: props.emailUseInput.value,
        password: props.passwordUseInput.value,
        authword: props.authwordUseInput.value,
      },
    });

    alert(userPassword);
    if (userPassword.includes('[ALERT]')) {
      setEtcInputAble(true);
      setButtonAble(true);
    } else setClickView('loginView');
  } catch (e) {
    alert(e);
  }
};

export const BackButton = ({ onClick }) => <BackButtonViewer onClick={onClick}>뒤로가기</BackButtonViewer>;

export const SendButton = ({ buttonAble, placeholder, onClick }) => (
  <SendButtonViewer onClick={onClick} disabled={!buttonAble}>
    {placeholder}
  </SendButtonViewer>
);
