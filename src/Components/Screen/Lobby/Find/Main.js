import React, { useState } from 'react';
import { useMutation } from '@apollo/client';

import { Viewer } from './Resource/Style/StMain';
import { useInput, Input } from './Input';
import { onSend, onComplete, SendButton, BackButton } from './Button';
import { VERIFY_USER, UPDATE_USERPASSWORD } from '../Share/Gql';

const Main = ({ setClickView }) => {
  const [emailInputAble, setEmailInputAble] = useState(true);
  const [etcInputAble, setEtcInputAble] = useState(true);
  const [buttonAble, setButtonAble] = useState(true);
  const [moreView, setMoreView] = useState(false);
  const [verifyUserMutation] = useMutation(VERIFY_USER);
  const [updateUserPasswordMutation] = useMutation(UPDATE_USERPASSWORD);

  const inputProps = {
    emailUseInput: useInput('찾으려는 email'),
    authwordUseInput: useInput('인증코드'),
    passwordUseInput: useInput('새 비밀번호'),
    passwordConfirmUseInput: useInput('새 비밀번호 확인'),
  };

  const onBackClick = () => {
    setClickView('loginView');
  };
  const onSendClick = () => {
    onSend(inputProps.emailUseInput, setEmailInputAble, setButtonAble, setMoreView, verifyUserMutation);
  };
  const onCompleteClick = () => {
    onComplete(inputProps, setEtcInputAble, setButtonAble, setClickView, updateUserPasswordMutation);
  };

  return (
    <Viewer>
      <Input type="text" inputAble={emailInputAble} {...inputProps.emailUseInput} />
      {moreView ? (
        <>
          <Input type="text" inputAble={etcInputAble} {...inputProps.authwordUseInput} />
          <Input type="password" inputAble={etcInputAble} {...inputProps.passwordUseInput} />
          <Input type="password" inputAble={etcInputAble} {...inputProps.passwordConfirmUseInput} />
          <SendButton buttonAble={buttonAble} placeholder="완료" onClick={onCompleteClick} />
        </>
      ) : (
        <SendButton buttonAble={buttonAble} placeholder="인증코드 발송" onClick={onSendClick} />
      )}
      <BackButton onClick={onBackClick} />
    </Viewer>
  );
};

export default Main;
