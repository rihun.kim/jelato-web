import styled from 'styled-components';

import { MAIN_CHOCO_COLOR, MAIN_PINK_COLOR, MAIN_PINK_HOVER_COLOR } from '../../../Share/StConstant';

export const SendButtonViewer = styled.button`
  width: 250px;
  height: 40px;

  background-color: ${MAIN_PINK_COLOR};
  border: 0;
  border-radius: 2px;
  font-size: 14px;
  text-align: center;

  &:hover {
    background-color: ${MAIN_PINK_HOVER_COLOR};
  }
`;

export const BackButtonViewer = styled.button`
  height: 30px;

  background-color: ${MAIN_CHOCO_COLOR};
  border: none;
  color: #cccccc;
  font-size: 11px;
  margin: 20px 0 0 0;
  outline: 0;

  &:hover {
    color: ${MAIN_PINK_COLOR};
  }
`;
