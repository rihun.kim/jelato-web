import { createContext } from 'react';

export const PlaygroundContext = createContext();
export const ScreenContext = createContext();
