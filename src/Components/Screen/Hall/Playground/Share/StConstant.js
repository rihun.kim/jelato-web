export const MAIN_CHOCO_COLOR = '#2b2b28';
export const MAIN_WHITE_COLOR = '#fbfbfb';
export const MAIN_PINK_COLOR = '#fdf0f6';
export const TAB_INACTIVE_COLOR = 'gray';

export const GRID_GAP = '20px';
export const GRID_COLUMNS_NUMS = 4;
export const GRID_COLUMNS_WIDTH = '250px';
export const GRID_ROWS_HEIGHT = '340px';
