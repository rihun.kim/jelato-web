import { gql } from '@apollo/client';

export const CREATE_PLAYGROUND = gql`
  mutation createPlayground($color: String!, $title: String!, $story: String!) {
    createPlayground(color: $color, title: $title, story: $story) {
      id
      barcode
      color
      status
      title
      story
    }
  }
`;

export const SEARCH_PLAYGROUNDS = gql`
  query searchPlaygrounds($keyword: String!) {
    searchPlaygrounds(keyword: $keyword) {
      id
      barcode
      color
      status
      title
      story
    }
  }
`;

export const READ_PLAYERS_IN_PLAYGROUND = gql`
  query readPlayersInPlayground($playgroundId: String!) {
    readPlayersInPlayground(playgroundId: $playgroundId) {
      id
      title
      players {
        id
        name
        position
      }
    }
  }
`;

export const UPDATE_PLAYGROUND = gql`
  mutation updatePlayground($playgroundId: String!, $status: Boolean, $color: String, $title: String, $story: String) {
    updatePlayground(playgroundId: $playgroundId, status: $status, color: $color, title: $title, story: $story) {
      id
      barcode
      color
      status
      title
      story
    }
  }
`;

export const DELETE_PLAYGROUND = gql`
  mutation deletePlayground($playgroundId: String!) {
    deletePlayground(playgroundId: $playgroundId)
  }
`;

export const CREATE_PLAYER = gql`
  mutation createPlayer($name: String!, $position: String!, $playgroundId: String!) {
    createPlayer(name: $name, position: $position, playgroundId: $playgroundId) {
      id
      name
      position
      playground {
        id
        barcode
        color
        status
        title
        story
      }
    }
  }
`;

export const UPDATE_PLAYER = gql`
  mutation updatePlayer($playerId: String!, $name: String, $position: String) {
    updatePlayer(playerId: $playerId, name: $name, position: $position) {
      id
      name
      position
    }
  }
`;

export const DELETE_PLAYER = gql`
  mutation deletePlayer($playerId: String!) {
    deletePlayer(playerId: $playerId)
  }
`;

export const CREATE_NOTICE = gql`
  mutation createNotice($playerId: String!, $playgroundId: String!, $story: String!, $title: String!) {
    createNotice(playerId: $playerId, playgroundId: $playgroundId, story: $story, title: $title) {
      id
    }
  }
`;
