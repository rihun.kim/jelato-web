import React, { useContext } from 'react';

import Fetcher from './Fetcher';
import { MainContext } from '../../.../../../../Share/Context';

const Main = () => {
  const { IPLAYERS } = useContext(MainContext);

  return <Fetcher iplayers={IPLAYERS.iplayers} fiplayers={IPLAYERS.fiplayers} />;
};

export default Main;
