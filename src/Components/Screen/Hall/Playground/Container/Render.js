import React, { useState, useRef, forwardRef, useEffect } from 'react';
import { useLazyQuery } from '@apollo/client';

import Header from '../Header/Main';
import Lockerroom from '../Room/Lockerroom/Main';
import Showroom from '../Room/Showroom/Main';
import Profile from '../../Profile/Container/Main';
import { Viewer, InnerViewer } from './Resource/Style/StRender';
import { SEARCH_PLAYGROUNDS } from '../Share/Gql';
import { ScreenContext } from '../Share/Context';

const Render = () => {
  const viewerRef = useRef();
  const [clickView, setClickView] = useState('lockerroomView');
  const [profileModal, setProfileModal] = useState(false);
  const [searchPgsLQuery, { data: remotePlaygrounds }] = useLazyQuery(SEARCH_PLAYGROUNDS, {
    fetchPolicy: 'network-only',
  });

  const toggleProfileModal = () => {
    profileModal ? setProfileModal(false) : setProfileModal(true);
  };

  const headerProps = {
    clickView: clickView,
    setClickView: setClickView,
    searchPgsLQuery: searchPgsLQuery,
    toggleProfileModal: toggleProfileModal,
  };

  return (
    <Viewer ref={viewerRef}>
      <InnerViewer>
        <Fowarder ref={viewerRef}>
          <Header props={headerProps} />
          {clickView === 'lockerroomView' && <Lockerroom />}
          {clickView === 'showroomView' && <Showroom remotePlaygrounds={remotePlaygrounds} />}
          {profileModal && <Profile toggleModal={toggleProfileModal} />}
        </Fowarder>
      </InnerViewer>
    </Viewer>
  );
};

const Fowarder = forwardRef(({ children }, ref) => {
  const [screenRef, setScreenRef] = useState(null);

  useEffect(() => {
    if (ref) setScreenRef(ref);
  }, [ref]);

  return screenRef && <ScreenContext.Provider value={{ screenRef, setScreenRef }}>{children}</ScreenContext.Provider>;
});

export default Render;
