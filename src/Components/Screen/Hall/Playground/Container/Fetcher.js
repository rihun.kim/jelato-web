import React from 'react';

import Render from './Render';
import { PlaygroundContext } from '../Share/Context';

const Fetcher = ({ iplayers, fiplayers }) => {
  return (
    <PlaygroundContext.Provider value={{ iplayers, fiplayers }}>
      <Render />
    </PlaygroundContext.Provider>
  );
};

export default Fetcher;
