export const deduplicatePlayground = (iplayers, remotePlaygrounds) => {
  const iplaygroundIds = iplayers.map(iplayer => iplayer.playground.id);
  const searchPlaygrounds = remotePlaygrounds.searchPlaygrounds;

  let filteredPlaygrounds = { own: [], in: [], wait: [], etc: [] };

  for (const searchPlayground of searchPlaygrounds) {
    const index = iplaygroundIds.indexOf(searchPlayground.id);

    if (index !== -1) {
      if (iplayers[index].position === 'in') filteredPlaygrounds.in.push(iplayers[index]);
      else if (iplayers[index].position === 'own') filteredPlaygrounds.own.push(iplayers[index]);
      else filteredPlaygrounds.wait.push(iplayers[index]);
    } else filteredPlaygrounds.etc.push(searchPlayground);
  }

  return filteredPlaygrounds;
};
