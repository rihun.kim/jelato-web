import styled from 'styled-components';

export const Viewer = styled.div`
  width: 100%;

  display: grid;
  grid-gap: 20px;
  grid-template-columns: repeat(4, 250px);
  grid-template-rows: 340px;
`;
