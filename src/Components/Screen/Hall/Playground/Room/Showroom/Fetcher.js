import React, { useContext } from 'react';

import Render from './Render';
import { deduplicatePlayground } from './Resource/Filter';
import { PlaygroundContext } from '../../Share/Context';

const Fetcher = ({ remotePlaygrounds }) => {
  const { iplayers } = useContext(PlaygroundContext);

  return <Render playgrounds={deduplicatePlayground(iplayers, remotePlaygrounds)} />;
};

export default Fetcher;
