import React from 'react';

import Fetcher from './Fetcher';

const Main = ({ remotePlaygrounds }) => {
  return !!remotePlaygrounds && <Fetcher remotePlaygrounds={remotePlaygrounds} />;
};

export default Main;
