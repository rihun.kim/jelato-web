import React, { useState } from 'react';

import Update from '../../Modal/Update/Main';
import Join from '../../Modal/Join/Main';
import { EtcCard, Card } from '../Card/Main';
import { Viewer } from './Resource/Style/StRender';

const Render = ({ playgrounds }) => {
  const [clickIplayer, setClickIplayer] = useState();
  const [clickPlayground, setClickPlayground] = useState();
  const [updateModal, setUpdateModal] = useState(false);
  const [joinModal, setJoinModal] = useState(false);

  const toggleUpdateModal = iplayer => {
    setClickIplayer(iplayer);
    updateModal ? setUpdateModal(false) : setUpdateModal(true);
  };
  const toggleJoinModal = playground => {
    setClickPlayground(playground);
    joinModal ? setJoinModal(false) : setJoinModal(true);
  };

  return (
    <Viewer>
      {playgrounds.own.map(iplayer => (
        <Card key={iplayer.id} iplayer={iplayer} onClick={() => toggleUpdateModal(iplayer)} />
      ))}
      {playgrounds.in.map(iplayer => (
        <Card key={iplayer.id} iplayer={iplayer} onClick={() => toggleUpdateModal(iplayer)} />
      ))}
      {playgrounds.wait.map(iplayer => (
        <Card key={iplayer.id} iplayer={iplayer} onClick={() => toggleUpdateModal(iplayer)} />
      ))}
      {playgrounds.etc.map(playground => (
        <EtcCard key={playground.id} playground={playground} onClick={() => toggleJoinModal(playground)} />
      ))}
      {updateModal && <Update toggleModal={toggleUpdateModal} clickIplayer={clickIplayer} />}
      {joinModal && <Join toggleModal={toggleJoinModal} clickPlayground={clickPlayground} />}
    </Viewer>
  );
};

export default Render;
