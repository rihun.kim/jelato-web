import React, { useContext, useState } from 'react';
import { useMutation } from '@apollo/client';

import Create from '../../Modal/Create/Main';
import Update from '../../Modal/Update/Main';
import Confirm from '../../Modal/Confirm/Main';
import { GuideCard, Card } from '../Card/Main';
import { Viewer } from './Resource/Style/StMain';
import { DELETE_PLAYER } from '../../Share/Gql';
import { PlaygroundContext } from '../../Share/Context';

const Main = () => {
  const { fiplayers } = useContext(PlaygroundContext);
  const [clickIplayer, setClickIplayer] = useState();
  const [createModal, setCreateModal] = useState(false);
  const [updateModal, setUpdateModal] = useState(false);
  const [deletePlayerMutation] = useMutation(DELETE_PLAYER);

  const toggleCreateModal = () => {
    createModal ? setCreateModal(false) : setCreateModal(true);
  };
  const toggleUpdateModal = iplayer => {
    setClickIplayer(iplayer);
    updateModal ? setUpdateModal(false) : setUpdateModal(true);
  };
  const toggleConfirmModal = iplayer => {
    Confirm(iplayer, deletePlayerMutation);
  };

  return (
    <Viewer>
      <GuideCard onClick={toggleCreateModal} />
      {fiplayers.own.map(iplayer => (
        <Card key={iplayer.id} iplayer={iplayer} onClick={() => toggleUpdateModal(iplayer)} />
      ))}
      {fiplayers.in.map(iplayer => (
        <Card key={iplayer.id} iplayer={iplayer} onClick={() => toggleUpdateModal(iplayer)} />
      ))}
      {fiplayers.wait.map(iplayer => (
        <Card key={iplayer.id} iplayer={iplayer} onClick={() => toggleConfirmModal(iplayer)} />
      ))}
      {createModal && <Create toggleModal={toggleCreateModal} />}
      {updateModal && <Update toggleModal={toggleUpdateModal} clickIplayer={clickIplayer} />}
    </Viewer>
  );
};

export default Main;
