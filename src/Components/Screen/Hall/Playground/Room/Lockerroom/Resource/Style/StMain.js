import styled from 'styled-components';

import { GRID_GAP, GRID_COLUMNS_NUMS, GRID_ROWS_HEIGHT, GRID_COLUMNS_WIDTH } from '../../../../Share/StConstant';

export const Viewer = styled.div`
  width: 100%;

  display: grid;
  grid-gap: ${GRID_GAP};
  grid-template-columns: repeat(${GRID_COLUMNS_NUMS}, ${GRID_COLUMNS_WIDTH});
  grid-template-rows: ${GRID_ROWS_HEIGHT};
`;
