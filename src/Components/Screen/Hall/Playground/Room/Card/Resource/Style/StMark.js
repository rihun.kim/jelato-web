import styled from 'styled-components';

export const MarkViewer = styled.div`
  width: 210px;
  height: 17px;

  bottom: 20px;
  color: #ffffff;
  font-size: 13px;
  font-weight: 200;
  overflow: hidden;
  position: absolute;
`;
