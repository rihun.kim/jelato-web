import styled from 'styled-components';
import { CARD_DEFAULT_WIDTH, CARD_DEFAULT_HEIGHT } from '../StConstant';

export const Text = styled.div`
  width: ${props => props.width};
  height: ${props => props.height};

  color: ${props => props.color};
  float: ${props => props.float};
  font-size: ${props => props.fontSize};
  font-weight: ${props => props.fontWeight};
  margin: ${props => props.margin};
  white-space: pre-wrap;
`;

export const GuideCardViewer = styled.div`
  width: ${CARD_DEFAULT_WIDTH};
  height: ${CARD_DEFAULT_HEIGHT};

  background-color: #ffffff;
  border-radius: 3px;
  border: 1px dashed #757575;
  box-shadow: 1px 3px 4px rgba(0, 0, 0, 0.19), 0 1px 0 rgba(0, 0, 0, 0.23);
  cursor: pointer;
  padding: 20px 20px 20px 20px;
  position: relative;
`;

export const GuideCardStyle = {
  barcode: {
    fontSize: '20px',
    float: 'right',
  },
  title: {
    width: '210px',
    fontSize: '18px',
    fontWeight: '600',
    margin: '70px 0 0 0',
    overflow: 'hidden',
  },
};

export const CardViewer = styled.div`
  width: ${CARD_DEFAULT_WIDTH};
  height: ${CARD_DEFAULT_HEIGHT};

  background-image: ${props => props.color};
  border-radius: 3px;
  border: ${props => props.border};
  box-shadow: 1px 3px 4px rgba(0, 0, 0, 0.19), 0 1px 0 rgba(0, 0, 0, 0.23);
  cursor: pointer;
  padding: 20px 20px 20px 20px;
  position: relative;
`;

export const CardStyle = {
  barcode: {
    color: '#ffffff',
    fontSize: '15px',
    float: 'right',
  },
  title: {
    width: '210px',
    height: '44px',
    color: '#ffffff',
    fontSize: '18px',
    fontWeight: '600',
    margin: '70px 0 0 0',
    overflow: 'hidden',
  },
  story: {
    width: '210px',
    height: '110px',
    color: '#ffffff',
    fontSize: '13px',
    fontWeight: '300',
    margin: '15px 0 0 0',
    overflow: 'hidden',
  },
};
