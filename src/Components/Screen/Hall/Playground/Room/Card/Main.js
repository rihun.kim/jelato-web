import React from 'react';

import { CARD_GUIDE_TITLE } from './Resource/Constant';
import { Mark } from './Mark';
import { Text, GuideCardStyle, GuideCardViewer, CardStyle, CardViewer } from './Resource/Style/StMain';

export const GuideCard = ({ onClick }) => (
  <GuideCardViewer onClick={onClick}>
    <Text style={GuideCardStyle.barcode}>+</Text>
    <Text style={GuideCardStyle.title}>{CARD_GUIDE_TITLE}</Text>
  </GuideCardViewer>
);

export const EtcCard = ({ playground, onClick }) => (
  <CardViewer color={playground.color} onClick={onClick}>
    <Text style={CardStyle.barcode}>{playground.barcode}</Text>
    <Text style={CardStyle.title}>{playground.title}</Text>
    <Text style={CardStyle.story}>{playground.story}</Text>
  </CardViewer>
);

export const Card = ({ iplayer, onClick }) => {
  const playground = iplayer.playground;

  return (
    <CardViewer color={playground.color} onClick={onClick}>
      <Text style={CardStyle.barcode}>{playground.barcode}</Text>
      <Text style={CardStyle.title}>{playground.title}</Text>
      <Text style={CardStyle.story}>{playground.story}</Text>
      <Mark playgroundStatus={playground.status} iplayer={iplayer} />
    </CardViewer>
  );
};
