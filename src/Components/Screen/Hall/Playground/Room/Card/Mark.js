import React from 'react';

import { MarkViewer } from './Resource/Style/StMark';
import { CARD_MARK_DESTORYED, CARD_MARK_WAIT, CARD_MARK_OUT } from './Resource/Constant';

export const Mark = ({ playgroundStatus, iplayer }) => {
  if (playgroundStatus === false) return <MarkViewer>{CARD_MARK_DESTORYED}</MarkViewer>;
  else if (iplayer.position === 'own' || iplayer.position === 'in') return <MarkViewer>{iplayer.name}</MarkViewer>;
  else if (iplayer.position === 'wait') return <MarkViewer>{CARD_MARK_WAIT}</MarkViewer>;
  else if (iplayer.position === 'out') return <MarkViewer>{CARD_MARK_OUT}</MarkViewer>;
};
