import React, { useState } from 'react';

import { InputViewer } from './Resource/Style/StInput';

export const useInput = prefill => {
  const [value, setValue] = useState('');

  const onChange = e => setValue(e.target.value);

  return { value, onChange };
};

const onKeyDown = (e, onClick) => {
  if (e.keyCode !== 13) return;

  e.preventDefault();
  onClick();
};

export const Input = ({ inputAble = true, value, onChange, onClick }) => (
  <InputViewer
    autoFocus="autofocus"
    disabled={!inputAble}
    defaultValue={value}
    onChange={onChange}
    onKeyDown={e => onKeyDown(e, onClick)}
    type="text"
  />
);
