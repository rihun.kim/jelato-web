import styled from 'styled-components';

export const ShowroomButtonViewer = styled.div`
  width: 16px;
  height: 16px;

  color: ${props => (props.clicked ? '#e095b8' : '#929191')};
  cursor: pointer;
  margin: auto 4px auto 4px;
`;

export const LockerroomButtonViewer = styled.div`
  width: 16px;
  height: 16px;

  color: ${props => (props.clicked ? '#e095b8' : '#929191')};
  cursor: pointer;
  margin: auto 4px auto 4px;
`;

export const PowerButtonViewer = styled.div`
  width: 16px;
  height: 16px;

  color: #a7a7a7;
  cursor: pointer;
  margin: auto 4px auto 4px;
`;
