import React from 'react';
import { IoIosSearch, IoMdHappy, IoIosPower } from 'react-icons/io';

import { ShowroomButtonViewer, LockerroomButtonViewer, PowerButtonViewer } from './Resource/Style/StButton';

export const ShowroomButton = ({ clickView, onClick }) => (
  <ShowroomButtonViewer clicked={clickView[0] === 's'} onClick={onClick}>
    <IoIosSearch title="showroom" />
  </ShowroomButtonViewer>
);

export const LockerroomButton = ({ clickView, onClick }) => (
  <LockerroomButtonViewer clicked={clickView[0] === 'l'} onClick={onClick}>
    <IoMdHappy title="lockerroom" />
  </LockerroomButtonViewer>
);

export const PowerButton = ({ onClick }) => (
  <PowerButtonViewer onClick={onClick}>
    <IoIosPower title="poweroff" />
  </PowerButtonViewer>
);

export const onShowroom = (searchUseInput, setClickView, searchPgsLQuery) => {
  try {
    if (searchUseInput.value === '') return;

    searchPgsLQuery({ variables: { keyword: searchUseInput.value } });
    setClickView('showroomView');
  } catch (error) {
    alert(error);
  }
};

export const onLockerroom = setClickView => {
  setClickView('lockerroomView');
};
