import React from 'react';

import { Viewer, InnerViewer } from './Resource/Style/StMain';
import { useInput, Input } from './Input';
import { ShowroomButton, LockerroomButton, PowerButton, onShowroom, onLockerroom } from './Button';

const Main = ({ props }) => {
  const searchUseInput = useInput('');

  const onShowroomClick = () => {
    onShowroom(searchUseInput, props.setClickView, props.searchPgsLQuery);
  };
  const onLockerroomClick = () => {
    onLockerroom(props.setClickView);
  };

  return (
    <Viewer>
      <InnerViewer>
        <Input {...searchUseInput} onClick={onShowroomClick} />
        <ShowroomButton clickView={props.clickView} onClick={onShowroomClick} />
        <LockerroomButton clickView={props.clickView} onClick={onLockerroomClick} />
        <PowerButton onClick={props.toggleProfileModal} />
      </InnerViewer>
    </Viewer>
  );
};

export default Main;
