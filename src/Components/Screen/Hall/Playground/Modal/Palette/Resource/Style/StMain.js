import styled from 'styled-components';

export const ColorPaletteViewer = styled.div`
  width: 100%;
  height: 25px;

  display: flex;
`;

export const Color = styled.div`
  width: 20px;
  height: 25px;

  background-image: ${props => props.color};
  border-radius: 1px;
  color: #ffffff;
  cursor: pointer;
  font-size: 14px;
  margin: 0 auto 0 auto;
  text-align: center;
`;
