export const filter = (players, clickIplayer) => {
  let filteredPlayers = { i: {}, own: {}, in: [], wait: [] };

  for (const player of players) {
    if (player.id === clickIplayer.id) filteredPlayers.i = clickIplayer;
    else if (player.position === 'in') filteredPlayers.in.push(player);
    else if (player.position === 'own') filteredPlayers.own = player;
    else if (player.position === 'wait') filteredPlayers.wait.push(player);
  }

  return filteredPlayers;
};
