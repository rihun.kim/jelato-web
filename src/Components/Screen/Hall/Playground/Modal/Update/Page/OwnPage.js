import React from 'react';

import OwnITable from '../Table/OwnITable';
import WaitTable from '../Table/WaitTable';
import InTable from '../Table/InTable';

const OwnPage = ({ players }) => {
  return (
    <>
      <OwnITable player={players.i} />
      <WaitTable players={players.wait} />
      <InTable players={players.in} />
    </>
  );
};

export default OwnPage;
