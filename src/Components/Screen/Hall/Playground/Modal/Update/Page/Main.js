import React from 'react';
import { useMutation } from '@apollo/client';

import OwnPage from './OwnPage';
import InPage from './InPage';

import { DELETE_PLAYER, DELETE_PLAYGROUND, UPDATE_PLAYER } from '../../../Share/Gql';
import { filter } from './Resource/Filter';
import { UpdateContext } from '../Share/Context';

const Main = ({ props }) => {
  const [updatePlayerMutation] = useMutation(UPDATE_PLAYER);
  const [deletePlayerMutation] = useMutation(DELETE_PLAYER);
  const [deletePlaygroundMutation] = useMutation(DELETE_PLAYGROUND);

  if (props.loading) return false;

  const contextProps = {
    updatePlayerMutation: updatePlayerMutation,
    deletePlayerMutation: deletePlayerMutation,
    deletePlaygroundMutation: deletePlaygroundMutation,
    toggleModal: props.toggleModal,
    __origin: props,
  };

  console.log('PAGE MAIN');

  const players = filter(props.remotePlayground.readPlayersInPlayground.players, props.clickIplayer);
  return (
    <UpdateContext.Provider value={contextProps}>
      {props.clickIplayer.position === 'own' ? <OwnPage players={players} /> : <InPage players={players} />}
    </UpdateContext.Provider>
  );
};

export default Main;
