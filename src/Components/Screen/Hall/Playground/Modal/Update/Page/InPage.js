import React from 'react';

import OwnTable from '../Table/OwnTable';
import ITable from '../Table/ITable';
import InTable from '../Table/InTable';

const InPage = ({ players }) => {
  return (
    <>
      <OwnTable player={players.own} />
      <ITable player={players.i} />
      <InTable players={players.in} />
    </>
  );
};

export default InPage;
