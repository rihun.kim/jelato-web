import React, { useState } from 'react';

import { InputViewer, TextareaViewer } from './Resource/Style/StInput';

export const useInput = prefill => {
  const [value, setValue] = useState(prefill);
  const onChange = e => setValue(e.target.value);

  return { value, onChange };
};

export const Input = ({ inputAble = true, onChange, value }) => (
  <InputViewer disabled={!inputAble} defaultValue={value} onChange={onChange} />
);

export const TextareaInput = ({ onChange, value }) => <TextareaViewer defaultValue={value} onChange={onChange} type="text" />;
