import React from 'react';
import { IoMdClose } from 'react-icons/io';

import { MODAL_UPDATE_CHECK_TITLE, MODAL_UPDATE_CHECK_STORY } from './Resource/Constant';
import { getColorFromIndex } from '../Palette/Main';
import { BUTTON_CLOSE_STYLE, CloseButtonViewer } from './Resource/Style/StButton';

export const CloseButton = ({ onClick }) => (
  <CloseButtonViewer>
    <IoMdClose title="close" style={BUTTON_CLOSE_STYLE} onClick={onClick} />
  </CloseButtonViewer>
);

const checkBlanks = props => {
  if (props.titleUseInput.value === '') alert(MODAL_UPDATE_CHECK_TITLE);
  else if (props.storyUseInput.value === '') alert(MODAL_UPDATE_CHECK_STORY);
  else return true;
};

const checkChanges = (prevProps, props) => {
  if (prevProps.color !== getColorFromIndex(props.colorUsePalette.value)) return true;
  else if (prevProps.title !== props.titleUseInput.value) return true;
  else if (prevProps.story !== props.storyUseInput.value) return true;
  else return false;
};

export const onComplete = props => {
  try {
    const prevProps = props.clickIplayer.playground;

    if (checkBlanks(props)) {
      if (checkChanges(prevProps, props)) {
        props.updatePlaygroundMutation({
          variables: {
            playgroundId: props.clickIplayer.playground.id,
            color: getColorFromIndex(props.colorUsePalette.value),
            title: props.titleUseInput.value,
            story: props.storyUseInput.value,
          },
        });

        props.clickIplayer.playground.color = getColorFromIndex(props.colorUsePalette.value);
        props.clickIplayer.playground.title = props.titleUseInput.value;
        props.clickIplayer.playground.story = props.storyUseInput.value;
      }

      props.toggleModal();
    }
  } catch (e) {
    console.error(e);
  }
};
