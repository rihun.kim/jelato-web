import React from 'react';

import { TEXT_PLAYER_POSITION_OWN } from './Resource/Constant';
import { TableViewer } from './Resource/Style/StTable';
import { Text, TextStyle } from './Resource/Style/StText';

const OwnTable = ({ player }) => {
  return (
    <TableViewer>
      <Text style={TextStyle.position.own}>{TEXT_PLAYER_POSITION_OWN}</Text>
      <Text style={TextStyle.name}>{player.name}</Text>
    </TableViewer>
  );
};

export default OwnTable;
