import React, { useContext } from 'react';

import { UpdateContext } from '../Share/Context';
import { onDelete } from './Button';
import { TEXT_BUTTON_KICKBACK, TEXT_PLAYER_POSITION_IN } from './Resource/Constant';
import { TableViewer, MessageGroup } from './Resource/Style/StTable';
import { Text, TextStyle } from './Resource/Style/StText';

const InTable = ({ players }) => {
  const { deletePlayerMutation, __origin } = useContext(UpdateContext);

  const onDeleteClick = player => {
    onDelete(player, deletePlayerMutation, __origin);
  };

  return (
    <>
      {players.map(player => (
        <TableViewer key={player.id}>
          <Text style={TextStyle.position.in}>{TEXT_PLAYER_POSITION_IN}</Text>
          <Text style={TextStyle.name}>{player.name}</Text>
          <MessageGroup>
            <Text style={TextStyle.button} onClick={() => onDeleteClick(player)}>
              {TEXT_BUTTON_KICKBACK}
            </Text>
          </MessageGroup>
        </TableViewer>
      ))}
    </>
  );
};

export default InTable;
