import styled from 'styled-components';

export const Text = styled.div`
  width: ${props => props.width};
  height: ${props => props.height};

  color: ${props => props.color};
  cursor: ${props => props.cursor};
  font-size: ${props => props.fontSize};
  font-weight: ${props => props.fontWeight};
  margin: ${props => props.margin};
  white-space: pre-wrap;
`;

export const TextStyle = {
  title: {
    fontSize: '14px',
    fontWeight: '600',
    margin: '0 0 20px 0',
  },
  name: {
    width: '280px',
    color: '#000000',
    fontSize: '13px',
    fontWeight: '400',
  },
  message: {
    color: '#989898',
    fontSize: '10px',
  },
  button: {
    color: '#989898',
    cursor: 'pointer',
    fontSize: '10px',
  },
  position: {
    own: {
      width: '120px',
      color: '#4260f7',
      fontSize: '13px',
      margin: '0 0 0 15px',
    },
    wait: {
      width: '120px',
      color: '#e23333',
      fontSize: '13px',
      margin: '0 0 0 15px',
    },
    in: {
      width: '120px',
      color: '#35d274',
      fontSize: '13px',
      margin: '0 0 0 15px',
    },
  },
};
