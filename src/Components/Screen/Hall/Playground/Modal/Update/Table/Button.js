export const onIn = (player, updatePlayerMutation, __origin) => {
  try {
    updatePlayerMutation({ variables: { playerId: player.id, position: 'in' } });
    const origin = __origin.remotePlayground.readPlayersInPlayground.players;
    origin[origin.indexOf(player)].position = 'in';
  } catch (e) {
    console.error(e);
  }
};

export const onDelete = (player, deletePlayerMutation, __origin) => {
  try {
    deletePlayerMutation({ variables: { playerId: player.id } });
    const origin = __origin.remotePlayground.readPlayersInPlayground.players;
    origin.splice(origin.indexOf(player), 1);
  } catch (e) {
    console.error(e);
  }
};

export const onDeleteReload = async (player, deletePlayerMutation) => {
  try {
    await deletePlayerMutation({ variables: { playerId: player.id } });

    window.location.reload();
  } catch (e) {
    console.error(e);
  }
};

export const onDestroy = async (deletePlaygroundMutation, __origin) => {
  try {
    const origin = __origin.remotePlayground.readPlayersInPlayground;
    if (window.confirm('정말 ' + origin.title + ' 을 삭제하시겠습니까?') === true) {
      await deletePlaygroundMutation({ variables: { playgroundId: origin.id } });

      window.location.reload();
    }
  } catch (e) {
    console.error(e);
  }
};
