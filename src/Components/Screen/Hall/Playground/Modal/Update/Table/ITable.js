import React, { useContext } from 'react';

import { UpdateContext } from '../Share/Context';
import { onDeleteReload } from './Button';
import { TEXT_BUTTON_RESIGN, TEXT_PLAYER_POSITION_IN } from './Resource/Constant';
import { TableViewer, MessageGroup } from './Resource/Style/StTable';
import { Text, TextStyle } from './Resource/Style/StText';

const ITable = ({ player }) => {
  const { deletePlayerMutation } = useContext(UpdateContext);

  const onDeleteClick = player => {
    onDeleteReload(player, deletePlayerMutation);
  };

  return (
    <TableViewer>
      <Text style={TextStyle.position.in}>{TEXT_PLAYER_POSITION_IN}</Text>
      <Text style={TextStyle.name}>{player.name}</Text>
      <MessageGroup>
        <Text style={TextStyle.button} onClick={() => onDeleteClick(player)}>
          {TEXT_BUTTON_RESIGN}
        </Text>
      </MessageGroup>
    </TableViewer>
  );
};

export default ITable;
