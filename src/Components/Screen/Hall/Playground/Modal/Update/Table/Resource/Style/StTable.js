import styled from 'styled-components';

export const MessageGroup = styled.div`
  width: 200px;

  display: flex;
  flex-direction: row;
  justify-content: flex-end;

  *:not(:last-child) {
    margin: 0 8px 0 0;
  }
  *:first-child {
    margin: 0 12px 0 0;
  }
  *:last-child {
    margin: 0 0 0 0;
  }
`;

export const TableViewer = styled.div`
  width: 100%;
  height: 35px;

  display: flex;
  flex-direction: row;
  align-items: center;

  border-bottom: 1px solid #e8e8e8;
`;
