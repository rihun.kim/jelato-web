import React, { useContext } from 'react';

import { UpdateContext } from '../Share/Context';
import { onIn, onDelete } from './Button';
import { TEXT_BUTTON_GRANT, TEXT_BUTTON_DENY, TEXT_PLAYER_POSITION_WAIT, TEXT_QUERY_PLAYER_IN } from './Resource/Constant';
import { TableViewer, MessageGroup } from './Resource/Style/StTable';
import { Text, TextStyle } from './Resource/Style/StText';

const WaitTable = ({ players }) => {
  console.log('Wait table ');

  const { updatePlayerMutation, deletePlayerMutation, __origin } = useContext(UpdateContext);

  const onInClick = player => {
    onIn(player, updatePlayerMutation, __origin);
  };
  const onDeleteClick = player => {
    onDelete(player, deletePlayerMutation, __origin);
  };

  return (
    <>
      {players.map(player => (
        <TableViewer key={player.id}>
          <Text style={TextStyle.position.wait}>{TEXT_PLAYER_POSITION_WAIT}</Text>
          <Text style={TextStyle.name}>{player.name}</Text>
          <MessageGroup>
            <Text style={TextStyle.message}>{TEXT_QUERY_PLAYER_IN}</Text>
            <Text style={TextStyle.button} onClick={() => onInClick(player)}>
              {TEXT_BUTTON_GRANT}
            </Text>
            <Text style={TextStyle.button} onClick={() => onDeleteClick(player)}>
              {TEXT_BUTTON_DENY}
            </Text>
          </MessageGroup>
        </TableViewer>
      ))}
    </>
  );
};

export default WaitTable;
