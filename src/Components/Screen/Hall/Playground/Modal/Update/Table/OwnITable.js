import React, { useContext } from 'react';

import { UpdateContext } from '../Share/Context';
import { onDestroy } from './Button';
import { TEXT_BUTTON_DESTROY, TEXT_PLAYER_POSITION_OWN } from './Resource/Constant';
import { TableViewer, MessageGroup } from './Resource/Style/StTable';
import { Text, TextStyle } from './Resource/Style/StText';

const OwnITable = ({ player }) => {
  const { deletePlaygroundMutation, __origin } = useContext(UpdateContext);

  const onDestroyClick = () => {
    onDestroy(deletePlaygroundMutation, __origin);
  };

  return (
    <TableViewer>
      <Text style={TextStyle.position.own}>{TEXT_PLAYER_POSITION_OWN}</Text>
      <Text style={TextStyle.name}>{player.name}</Text>
      <MessageGroup>
        <Text style={TextStyle.button} onClick={onDestroyClick}>
          {TEXT_BUTTON_DESTROY}
        </Text>
      </MessageGroup>
    </TableViewer>
  );
};

export default OwnITable;
