import React from 'react';

import { Input, TextareaInput } from './Input';
import { ColorPalette } from '../Palette/Main';

export const Form = ({ props }) => {
  return (
    <>
      <Input {...props.titleUseInput} />
      <TextareaInput {...props.storyUseInput} />
      <ColorPalette {...props.colorUsePalette} />
    </>
  );
};
