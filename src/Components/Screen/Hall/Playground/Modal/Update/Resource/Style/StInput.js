import styled from 'styled-components';

export const InputViewer = styled.input`
  width: 100%;
  height: 35px;

  border: 0;
  border-bottom: 1px solid #e0e0e0;
  font-size: 14px;
  margin: 0 0 10px 0;
  padding: 0 15px 0 15px;
`;

export const TextareaViewer = styled.textarea`
  width: 100%;
  height: 160px;

  border: 0;
  border-bottom: 1px solid #e0e0e0;
  font-size: 14px;
  margin: 0 0 10px 0;
  padding: 10px 15px 10px 15px;
  resize: none;
  white-space: pre-wrap;
`;
