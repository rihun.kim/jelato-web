import React, { useContext } from 'react';
import { useMutation, useQuery } from '@apollo/client';

import Carousel from '../Carousel/Main';
import Page from './Page/Main';
import { Form } from './Form';
import { READ_PLAYERS_IN_PLAYGROUND, UPDATE_PLAYGROUND } from '../../Share/Gql';
import { useInput } from './Input';
import { CloseButton, onComplete } from './Button';
import { usePalette, getIndexFromColor } from '../Palette/Main';
import { Viewer } from './Resource/Style/StMain';
import { reconcileModalTop } from '../Share/Reconcile';
import { ScreenContext } from '../../Share/Context';

const Main = ({ toggleModal, clickIplayer }) => {
  const { screenRef } = useContext(ScreenContext);
  const { loading, data: remotePlayground } = useQuery(READ_PLAYERS_IN_PLAYGROUND, {
    variables: { playgroundId: clickIplayer.playground.id },
    fetchPolicy: 'no-cache',
  });
  const [updatePlaygroundMutation] = useMutation(UPDATE_PLAYGROUND);

  const inputProps = {
    titleUseInput: useInput(clickIplayer.playground.title),
    storyUseInput: useInput(clickIplayer.playground.story),
    colorUsePalette: usePalette(getIndexFromColor(clickIplayer.playground.color)),
    clickIplayer: clickIplayer,
    updatePlaygroundMutation: updatePlaygroundMutation,
    toggleModal: toggleModal,
  };
  const pageProps = {
    clickIplayer: clickIplayer,
    remotePlayground: remotePlayground,
    loading: loading,
    toggleModal: toggleModal,
  };

  const onCompleteClick = () => {
    onComplete(inputProps);
  };

  return (
    <Viewer top={() => reconcileModalTop(screenRef)}>
      <CloseButton onClick={onCompleteClick} />
      <Carousel>
        {clickIplayer.position === 'own' && <Form props={inputProps} />}
        <Page props={pageProps} />
      </Carousel>
    </Viewer>
  );
};

export default Main;
