import React, { useContext } from 'react';

import Form from './Form';
import { ScreenContext } from '../../Share/Context';
import { reconcileModalTop } from '../Share/Reconcile';
import { Viewer } from './Resource/Style/StMain';
import { useInput } from './Input';
import { onComplete, CloseButton, SaveButton } from './Button';
import { useMutation } from '@apollo/client';
import { CREATE_PLAYER } from '../../Share/Gql';

const Main = ({ clickPlayground, toggleModal }) => {
  const { screenRef } = useContext(ScreenContext);
  const [createPlayerMutation] = useMutation(CREATE_PLAYER);

  const inputProps = {
    clickPlayground: clickPlayground,
    nameUseInput: useInput(),
    titleUseInput: useInput(),
    storyUseInput: useInput(),
    createPlayerMutation: createPlayerMutation,
  };

  const onCompleteClick = () => {
    onComplete(inputProps);
  };

  return (
    <Viewer top={() => reconcileModalTop(screenRef)}>
      <CloseButton onClick={toggleModal} />
      <SaveButton onClick={onCompleteClick} />
      <Form props={inputProps} />
    </Viewer>
  );
};

export default Main;
