import React, { useState } from 'react';

import { InputViewer, TextareaViewer } from './Resource/Style/StInput';

export const useInput = prefill => {
  const [value, setValue] = useState('');
  const placeholder = prefill;

  const onChange = e => setValue(e.target.value);

  return { placeholder, value, onChange };
};

export const Input = ({ inputAble = true, onChange, placeholder, value }) => (
  <InputViewer disabled={!inputAble} defaultValue={value} onChange={onChange} placeholder={placeholder} />
);

export const TextareaInput = ({ inputAble = true, onChange, placeholder, value }) => (
  <TextareaViewer disabled={!inputAble} defaultValue={value} onChange={onChange} placeholder={placeholder} type="text" />
);
