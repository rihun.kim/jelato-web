import React from 'react';

import { MODAL_JOIN_NAME } from './Resource/Constant';
import { Input, TextareaInput } from './Input';

const Form = ({ props }) => {
  return (
    <>
      <Input {...props.nameUseInput} placeholder={props.clickPlayground.title + MODAL_JOIN_NAME} />
      <Input {...props.titleUseInput} inputAble={false} value={props.clickPlayground.title} />
      <TextareaInput {...props.storyUseInput} inputAble={false} value={props.clickPlayground.story} />
    </>
  );
};

export default Form;
