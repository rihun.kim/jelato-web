import React from 'react';
import { IoMdClose, IoMdRadioButtonOff } from 'react-icons/io';

import { MODAL_JOIN_CHECK_NAME } from './Resource/Constant';
import { BUTTON_CLOSE_STYLE, BUTTON_SAVE_STYLE, CloseButtonViewer, SaveButtonViewer } from './Resource/Style/StButton';

export const CloseButton = ({ onClick }) => (
  <CloseButtonViewer>
    <IoMdClose title="close" style={BUTTON_CLOSE_STYLE} onClick={onClick} />
  </CloseButtonViewer>
);

export const SaveButton = ({ onClick }) => (
  <SaveButtonViewer>
    <IoMdRadioButtonOff title="save" style={BUTTON_SAVE_STYLE} onClick={onClick} />
  </SaveButtonViewer>
);

const checkBlanks = nameUseInput => {
  if (nameUseInput.value === '') alert(MODAL_JOIN_CHECK_NAME);
  else return true;
};

export const onComplete = async props => {
  try {
    if (checkBlanks(props.nameUseInput)) {
      await props.createPlayerMutation({
        variables: {
          name: props.nameUseInput.value,
          playgroundId: props.clickPlayground.id,
          position: 'wait',
        },
      });

      window.location.reload();
    }
  } catch (e) {
    console.error(e);
  }
};
