const onDelete = (player, deletePlayerMutation) => {
  try {
    deletePlayerMutation({ variables: { playerId: player.id } });
  } catch (e) {
    console.error(e);
  }
};

const Main = (iplayer, deletePlayerMutation) => {
  const response = window.prompt('현재 방장의 승인을 기다리고 있습니다.\n참여를 취소하시려면, 참여취소 를 입력하세요.');

  if (response === '참여취소') {
    onDelete(iplayer, deletePlayerMutation);

    window.location.reload();
  }
};

export default Main;
