import { MODAL_DEFAULT_HEIGHT } from './StConstant';

export const reconcileModalTop = screenRef => {
  const screen = screenRef.current;

  return screen.clientHeight > MODAL_DEFAULT_HEIGHT
    ? screen.scrollTop + screen.clientHeight / 2 - MODAL_DEFAULT_HEIGHT / 2
    : screen.scrollTop + 20;
};
