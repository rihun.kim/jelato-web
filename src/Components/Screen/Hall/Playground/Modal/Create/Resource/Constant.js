export const MODAL_CREATE_NAME = '새 Playground 에서 사용할 닉네임';
export const MODAL_CREATE_TITLE = '새 Playground 이름';
export const MODAL_CREATE_STORY = '새 Playground 소개';
export const MODAL_CREATE_DEFAULT_COLOR = 0;

export const MODAL_CREATE_CHECK_NAME = '새 Playground 에서 사용할 닉네임을 적어주세요.';
export const MODAL_CREATE_CHECK_TITLE = '새 Playground 이름을 적어주세요.';
export const MODAL_CREATE_CHECK_STORY = '새 Playground 소개해주세요.';
