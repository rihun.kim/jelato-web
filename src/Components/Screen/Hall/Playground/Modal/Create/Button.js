import React from 'react';
import { IoMdClose, IoMdRadioButtonOff } from 'react-icons/io';

import { getColorFromIndex } from '../Palette/Main';
import { MODAL_CREATE_CHECK_NAME, MODAL_CREATE_CHECK_TITLE, MODAL_CREATE_CHECK_STORY } from './Resource/Constant';
// import { NOTICE_GUIDE_TITLE, NOTICE_GUIDE_STORY } from '../../../Notice/Share/Constant';
import { CloseButtonViewer, SaveButtonViewer, BUTTON_CLOSE_STYLE, BUTTON_SAVE_STYLE } from './Resource/Style/StButton';

export const CloseButton = ({ onClick }) => (
  <CloseButtonViewer>
    <IoMdClose title="close" style={BUTTON_CLOSE_STYLE} onClick={onClick} />
  </CloseButtonViewer>
);

export const SaveButton = ({ onClick }) => (
  <SaveButtonViewer>
    <IoMdRadioButtonOff title="save" style={BUTTON_SAVE_STYLE} onClick={onClick} />
  </SaveButtonViewer>
);

const checkChanges = props => {
  if (props.nameUseInput.value === '') alert(MODAL_CREATE_CHECK_NAME);
  else if (props.titleUseInput.value === '') alert(MODAL_CREATE_CHECK_TITLE);
  else if (props.storyUseInput.value === '') alert(MODAL_CREATE_CHECK_STORY);
  else return true;
};

export const onComplete = async props => {
  try {
    if (checkChanges(props)) {
      const {
        data: { createPlayground },
      } = await props.createPlaygroundMutation({
        variables: {
          color: getColorFromIndex(props.colorUsePalette.value),
          title: props.titleUseInput.value,
          story: props.storyUseInput.value,
        },
      });

      await props.createPlayerMutation({
        variables: {
          name: props.nameUseInput.value,
          playgroundId: createPlayground.id,
          position: 'own',
        },
      });

      // 서버쪽에서 담당해야 함
      // props.createNoticeMutation({
      //   variables: {
      //     playerId: createPlayer.id,
      //     playgroundId: createPlayground.id,
      //     title: NOTICE_GUIDE_TITLE,
      //     story: NOTICE_GUIDE_STORY,
      //   },
      // });

      window.location.reload();
    }
  } catch (e) {
    console.error(e);
  }
};
