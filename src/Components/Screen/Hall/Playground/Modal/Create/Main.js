import React, { useContext } from 'react';
import { useMutation } from '@apollo/client';

import { useInput, Input, TextareaInput } from './Input';
import { usePalette, ColorPalette } from '../Palette/Main';
import { onComplete, CloseButton, SaveButton } from './Button';
import { MODAL_CREATE_NAME, MODAL_CREATE_TITLE, MODAL_CREATE_STORY, MODAL_CREATE_DEFAULT_COLOR } from './Resource/Constant';
import { Viewer } from './Resource/Style/StMain';
import { CREATE_PLAYER, CREATE_PLAYGROUND, CREATE_NOTICE } from '../../Share/Gql';
import { reconcileModalTop } from '../Share/Reconcile';
import { ScreenContext } from '../../Share/Context';

const Main = ({ toggleModal }) => {
  const { screenRef } = useContext(ScreenContext);
  const [createNoticeMutation] = useMutation(CREATE_NOTICE);
  const [createPlayerMutation] = useMutation(CREATE_PLAYER);
  const [createPlaygroundMutation] = useMutation(CREATE_PLAYGROUND);

  const inputProps = {
    nameUseInput: useInput(MODAL_CREATE_NAME),
    titleUseInput: useInput(MODAL_CREATE_TITLE),
    storyUseInput: useInput(MODAL_CREATE_STORY),
    colorUsePalette: usePalette(MODAL_CREATE_DEFAULT_COLOR),
    createPlaygroundMutation: createPlaygroundMutation,
    createPlayerMutation: createPlayerMutation,
    createNoticeMutation: createNoticeMutation,
  };

  const onCompleteClick = () => {
    onComplete(inputProps);
  };

  return (
    <Viewer top={() => reconcileModalTop(screenRef)}>
      <CloseButton onClick={toggleModal} />
      <SaveButton onClick={onCompleteClick} />
      <Input {...inputProps.nameUseInput} />
      <Input {...inputProps.titleUseInput} />
      <TextareaInput {...inputProps.storyUseInput} />
      <ColorPalette {...inputProps.colorUsePalette} />
    </Viewer>
  );
};

export default Main;
