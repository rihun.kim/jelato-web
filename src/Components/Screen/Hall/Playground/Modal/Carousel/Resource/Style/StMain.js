import styled from 'styled-components';

export const Viewer = styled.div`
  width: 100%;
  height: 100%;
`;

export const InnerViewer = styled.div`
  width: 100%;
  height: 90%;

  overflow-y: auto;
`;
