import styled from 'styled-components';

import { MAIN_CHOCO_COLOR } from '../../../../Share/StConstant';

export const ButtonGroupViewer = styled.div`
  width: 100%;
  height: 10%;

  display: flex;
  flex-direction: row;
  justify-content: center;
  align-items: flex-end;

  *:nth-child(${props => 1 + props.clickSlide}) {
    background-color: ${MAIN_CHOCO_COLOR};
  }
  *:not(:last-child) {
    margin: 0 6px 0 0;
  }
`;

export const ButtonViewer = styled.div`
  width: 10px;
  height: 10px;

  border-radius: 5px;
  background-color: #e6e6e6;
  cursor: pointer;
`;
