import React from 'react';

import { ButtonGroupViewer, ButtonViewer } from './Resource/Style/StButton';

export const ButtonGroup = ({ clickSlide, children }) => <ButtonGroupViewer clickSlide={clickSlide}>{children}</ButtonGroupViewer>;

export const Button = ({ onClick }) => <ButtonViewer onClick={onClick} />;
