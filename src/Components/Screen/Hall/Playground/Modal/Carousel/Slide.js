import React from 'react';

import { SlideViewer } from './Resource/Style/StSlide';

const Slide = ({ main }) => <SlideViewer>{main}</SlideViewer>;

export default Slide;
