import React, { useState } from 'react';

import Slide from './Slide';
import { Button, ButtonGroup } from './Button';
import { Viewer, InnerViewer } from './Resource/Style/StMain';

const Main = ({ children }) => {
  const [clickSlide, setClickSlide] = useState(0);

  children = children.filter(child => child !== false);

  return (
    <Viewer>
      <InnerViewer>{children.map((main, i) => clickSlide === i && <Slide key={i} main={main} />)}</InnerViewer>
      {children.length !== 1 && (
        <ButtonGroup clickSlide={clickSlide}>
          {children.map((_, i) => (
            <Button key={i} onClick={() => setClickSlide(i)} />
          ))}
        </ButtonGroup>
      )}
    </Viewer>
  );
};

export default Main;
