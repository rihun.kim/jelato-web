import React, { useContext } from 'react';

import Fetcher from './Fetcher/Main';
import { MainContext } from '../../../../Share/Context';

const Main = ({ match }) => {
  const { IPLAYERS } = useContext(MainContext);

  const iplayer = IPLAYERS.iplayers.find(iplayer => iplayer.playground.barcode === match.params.barcode);

  return <Fetcher iplayer={iplayer} />;
};

export default Main;
