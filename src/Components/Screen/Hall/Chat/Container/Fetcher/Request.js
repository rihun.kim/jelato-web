import { useQuery, useSubscription } from '@apollo/client';
import { cloneDeep } from '@apollo/client/utilities';

import { READ_CHATS, SUBSCRIBE_MESSAGES, SUBSCRIBE_CHATS } from '../../Share/Gql';

export const UseQueryChats = iplayer => {
  const { loading, data } = useQuery(READ_CHATS, {
    variables: { playerIds: [iplayer.id] },
  });

  return loading || data === undefined ? false : cloneDeep(data);
};

export const UseSubscribeChats = iplayer => {
  const { data } = useSubscription(SUBSCRIBE_CHATS, {
    variables: { playerIds: [iplayer.id] },
  });

  return data === undefined ? false : data;
};

export const UseSubsribeMessages = iplayer => {
  const { data } = useSubscription(SUBSCRIBE_MESSAGES, {
    variables: { playerIds: [iplayer.id] },
  });

  return data === undefined ? false : data;
};
