import React from 'react';

import Render from '../Render/Main';
import Subscribe from './Subscribe';
import { UseQueryChats } from './Request';

const Main = ({ iplayer }) => {
  const remoteChats = UseQueryChats(iplayer);

  // console.log('Chat/Container/Fetcher.Main');

  return (
    remoteChats && (
      <Subscribe iplayer={iplayer} remoteChats={remoteChats}>
        <Render />
      </Subscribe>
    )
  );
};

export default Main;
