import React, { useMemo } from 'react';

import { ChatContext } from '../../Share/Context';
import { memoRealtimeChats, memoRealtimeMessages } from './Memo';
import { UseSubscribeChats, UseSubsribeMessages } from './Request';

const Subscribe = ({ children, iplayer, remoteChats }) => {
  const realtimeChats = UseSubscribeChats(iplayer);
  const realtimeMessages = UseSubsribeMessages(iplayer);

  // remoteChats 은 최초에 한번만 부르고 안 부름, 채팅창을 새로고침하지 않는 이상...
  useMemo(() => {
    if (remoteChats && realtimeChats) memoRealtimeChats(remoteChats, realtimeChats);
  }, [remoteChats, realtimeChats]);

  useMemo(() => {
    if (remoteChats && realtimeMessages) memoRealtimeMessages(iplayer, remoteChats, realtimeMessages);
  }, [remoteChats, realtimeMessages, iplayer]);
  // 여기서 내가 쓴 메시지가 서버를 통해 올 경우는 그냥 무시하고 있음.

  return <ChatContext.Provider value={{ iplayer, remoteChats }}>{children}</ChatContext.Provider>;
};

export default Subscribe;
