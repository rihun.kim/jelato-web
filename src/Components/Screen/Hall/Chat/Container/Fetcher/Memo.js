import { cloneDeep } from '@apollo/client/utilities';

export const memoRealtimeMessages = (iplayer, remoteChats, realtimeMessages) => {
  const chats = remoteChats.readChats;
  const realtimeMessage = realtimeMessages.subscribeMessages;

  for (const chat of chats) {
    if (chat.id === realtimeMessage.chat.id) {
      if (realtimeMessage.type === 'FORMAL' || realtimeMessage.owner.id !== iplayer.id) {
        chat.messages.push(cloneDeep(realtimeMessage));
        break;
      }
    }
  }
};

export const memoRealtimeChats = (remoteChats, realtimeChats) => {
  const mutationType = realtimeChats.subscribeChats.mutationType;
  const chatPosition = realtimeChats.subscribeChats.chatPosition;
  const realtimeChat = realtimeChats.subscribeChats.node;

  if (mutationType === 'CREATE_CHAT') {
    // console.log(realtimeChat);
    remoteChats.readChats.push(cloneDeep(realtimeChat));
  } else if (mutationType === 'UPDATE_CHAT') {
    for (const chat of remoteChats.readChats) {
      if (chat.id === realtimeChat.id) {
        if (chatPosition === 'in') {
          remoteChats.readChats.push(cloneDeep(realtimeChat));
        } else if (chatPosition === 'away') {
          const chatIndex = remoteChats.readChats.findIndex(chat => chat.id === realtimeChat.id);
          remoteChats.readChats.splice(chatIndex, 1);
        } else if (chatPosition === 'same') {
          // console.log(realtimeChat);
          chat.count = realtimeChat.count;
          chat.title = realtimeChat.title;
          chat.owner = realtimeChat.owner;
          chat.players = cloneDeep(realtimeChat.players);
          break;
        }
      }
    }

    // console.log('update 됭었다.', remoteChats.readChats);
  } else if (mutationType === 'DELETE_CHAT') {
    const chatIndex = remoteChats.readChats.findIndex(chat => chat.id === realtimeChat);
    remoteChats.readChats.splice(chatIndex, 1);
  }
};

// else if (mutation === "UPDATED") {
//   let isChatExist = false;

//   for (const chat of remoteChats.readChats) {
//     if (chat.id === realtimeChat.id) {
//       chat.count = realtimeChat.count;
//       chat.title = realtimeChat.title;
//       chat.owner = realtimeChat.owner;
//       chat.players = cloneDeep(realtimeChat.players);

//       isChatExist = true;
//       break;
//     }
//   }

//   if (!isChatExist) {
//     const vhatIndex = remoteVhats.readVhats.findIndex((vhat) => vhat.chatid === realtimeChat.id);
//     if (vhatIndex > -1) remoteVhats.readVhats.splice(vhatIndex, 1);
//     remoteChats.readChats.push(cloneDeep(realtimeChat));
//   }
// } else if (mutation === "DELETED") {
//   const chatIndex = remoteChats.readChats.findIndex(
//     (chat) => chat.id === realtimeChats.subscribeChats.previousValues.id
//   );
//   remoteChats.readChats.splice(chatIndex, 1);
// }
