import React, { useRef } from 'react';

import Room from '../../Room/Container/Main';
import Overlayer from './Overlayer';
import { InnerViewer, Viewer } from './Style/StMain';

const Main = () => {
  const viewerRef = useRef();

  // console.log('Chat/Container/Render.Main');

  return (
    <Viewer ref={viewerRef}>
      <InnerViewer>
        <Overlayer ref={viewerRef}>
          <Room />
        </Overlayer>
      </InnerViewer>
    </Viewer>
  );
};

export default Main;
