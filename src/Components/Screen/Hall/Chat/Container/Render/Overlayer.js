import React, { useEffect, useState, forwardRef } from 'react';

import { ScreenContext } from '../../Share/Context';

const Overlayer = forwardRef(({ children }, ref) => {
  const [screenRef, setScreenRef] = useState(false);

  useEffect(() => {
    if (ref) setScreenRef(ref);
  }, [ref]);

  return screenRef && <ScreenContext.Provider value={{ screenRef, setScreenRef }}>{children}</ScreenContext.Provider>;
});

export default Overlayer;
