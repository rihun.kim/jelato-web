import React from 'react';

import { Text, TextStyle } from './Style/StText';
import { TEXT_CHATPOSITION_DEFAULT, TEXT_CHATPOSITION_OWN } from './Resource/Constant';

export const OwnNameplate = ({ player }) => {
  return (
    <>
      <Text style={TextStyle.chatposition.own}>{TEXT_CHATPOSITION_OWN}</Text>
      <Text style={TextStyle.name}>{player.name}</Text>
    </>
  );
};

export const INameplate = ({ player }) => {
  return (
    <>
      <Text style={TextStyle.chatposition.i}>{TEXT_CHATPOSITION_DEFAULT}</Text>
      <Text style={TextStyle.name}>{player.name} (me)</Text>
    </>
  );
};

export const OthersNameplate = ({ player }) => {
  const initialColor = player.chatPosition === 'in' ? '#35d274' : '#808080';
  const style = { ...TextStyle.chatposition.others, color: initialColor };

  return (
    <>
      <Text className={'nameplate_' + player.id} style={style}>
        {TEXT_CHATPOSITION_DEFAULT}
      </Text>
      <Text style={TextStyle.name}>{player.name}</Text>
    </>
  );
};
