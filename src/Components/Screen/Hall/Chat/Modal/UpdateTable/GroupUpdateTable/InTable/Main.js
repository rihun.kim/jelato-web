import React from 'react';

import { OwnNameplate, INameplate, OthersNameplate } from './Nameplate';
import { onDelete, onInvite, ControlButton, InviteButton } from './Button';
import { Viewer, RowViewer } from './Style/StMain';

const Main = ({ fplayers }) => {
  const onDeleteClick = () => {
    onDelete();
  };
  const onInviteClick = player => {
    onInvite(player, fplayers.inplayerIdsInChat);
  };

  return (
    <Viewer>
      <RowViewer>
        <OwnNameplate player={fplayers.own} />
      </RowViewer>
      <RowViewer>
        <INameplate player={fplayers.i} />
        <ControlButton onClick={onDeleteClick} />
      </RowViewer>
      {fplayers.others.map(player => (
        <RowViewer key={player.id}>
          <OthersNameplate player={player} />
          <InviteButton player={player} onClick={onInviteClick} />
        </RowViewer>
      ))}
    </Viewer>
  );
};

export default Main;
