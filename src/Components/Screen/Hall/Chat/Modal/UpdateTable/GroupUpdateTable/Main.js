import React from 'react';

import OwnTable from './OwnTable/Main';
import InTable from './InTable/Main';

const Main = ({ fplayersWithChat }) =>
  fplayersWithChat.owni ? <OwnTable fplayers={fplayersWithChat} /> : <InTable fplayers={fplayersWithChat} />;

export default Main;
