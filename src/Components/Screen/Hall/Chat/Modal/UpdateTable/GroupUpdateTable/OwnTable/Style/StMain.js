import styled from 'styled-components';

export const Viewer = styled.div``;

export const RowViewer = styled.div`
  width: 100%;
  height: 35px;

  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: flex-start;

  border-bottom: 1px solid #e8e8e8;
`;
