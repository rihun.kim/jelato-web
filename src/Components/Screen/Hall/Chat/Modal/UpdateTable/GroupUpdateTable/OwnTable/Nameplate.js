import React from 'react';

import { Text, TextStyle } from './Style/StText';
import { TEXT_CHATPOSITION_DEFAULT, TEXT_CHATPOSITION_OWNI } from './Resource/Constant';

export const OwnINameplate = ({ player }) => {
  return (
    <>
      <Text style={TextStyle.chatposition.owni}>{TEXT_CHATPOSITION_OWNI}</Text>
      <Text style={TextStyle.name}>{player.name} (me)</Text>
    </>
  );
};

export const OthersNameplate = ({ player }) => {
  const initialColor = player.chatPosition === 'in' ? '#35d274' : '#808080';
  const style = { ...TextStyle.chatposition.others, color: initialColor };

  return (
    <>
      <Text className={player.id} style={style}>
        {TEXT_CHATPOSITION_DEFAULT}
      </Text>
      <Text style={TextStyle.name}>{player.name}</Text>
    </>
  );
};
