import React from 'react';

import { OwnINameplate, OthersNameplate } from './Nameplate';
import { onDelegate, onDestroy, ControlButton, onToggle, ToggleButton } from './Button';
import { Viewer, RowViewer } from './Style/StMain';

const Main = ({ fplayers }) => {
  const onDelegateClick = () => {
    onDelegate();
  };
  const onDestroyClick = () => {
    onDestroy();
  };
  const onToggleClick = player => {
    onToggle(player, fplayers.inplayerIdsInChat);
  };

  return (
    <Viewer>
      <RowViewer>
        <OwnINameplate player={fplayers.owni} />
        <ControlButton onDelegateClick={onDelegateClick} onDestroyClick={onDestroyClick} />
      </RowViewer>
      {fplayers.others.map(player => (
        <RowViewer key={player.id}>
          <OthersNameplate player={player} />
          <ToggleButton player={player} onClick={onToggleClick} />
        </RowViewer>
      ))}
    </Viewer>
  );
};

export default Main;
