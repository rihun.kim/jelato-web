import React from 'react';

import { TEXT_BUTTON_DELETE, TEXT_BUTTON_INVITE } from './Resource/Constant';
import { Text, TextStyle } from './Style/StText';
import { ControlButtonViewer, InviteButtonViewer } from './Style/StButton';

export const ControlButton = ({ onClick }) => {
  return (
    <ControlButtonViewer>
      <Text style={TextStyle.button} onClick={onClick}>
        {TEXT_BUTTON_DELETE}
      </Text>
    </ControlButtonViewer>
  );
};

export const InviteButton = ({ player, onClick }) => {
  return player.chatPosition === 'in' ? (
    false
  ) : (
    <InviteButtonViewer className={'button_' + player.id}>
      <Text style={TextStyle.button} onClick={() => onClick(player)}>
        {TEXT_BUTTON_INVITE}
      </Text>
    </InviteButtonViewer>
  );
};

export const onDelete = () => {};

export const onInvite = (player, inplayerIdsInChat) => {
  // if (player.chatPosition === 'in') {
  //     player.chatPosition = 'away';
  //     inplayerIdsInChat.splice(
  //       inplayerIdsInChat.findIndex(inplayerId => inplayerId === player.id),
  //       1,
  //     );
  //   } else {
  //     player.chatPosition = 'in';
  //     inplayerIdsInChat.push(player.id);
  //   }

  let dom = document.querySelector('.button_' + player.id);
  dom.firstChild.innerHTML = '초대되었습니다.';
  dom.firstChild.style.pointerEvents = 'none';

  dom = document.querySelector('.nameplate_' + player.id);
  dom.style.color = dom.style.color[4] === '1' ? '#35d274' : '#808080';
};
