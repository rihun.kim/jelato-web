import styled from 'styled-components';

export const ControlButtonViewer = styled.div`
  width: 200px;
  height: 16px;

  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: flex-end;

  *:not(:last-child) {
    margin: 0 8px 0 0;
  }
  *:first-child {
    margin: 0 12px 0 0;
  }
  *:last-child {
    margin: 0 0 0 0;
  }
`;

export const ToggleButtonViewer = styled.div`
  width: 200px;
  height: 16px;

  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: flex-end;
`;

export const BUTTON_TOGGLE_STYLE = {
  width: '15px',
  height: '15px',
  cursor: 'pointer',
};
