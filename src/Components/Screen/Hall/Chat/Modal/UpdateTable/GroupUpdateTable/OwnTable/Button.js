import React from 'react';
import { VscCheck } from 'react-icons/vsc';

import { TEXT_BUTTON_DELEGATE, TEXT_BUTTON_DESTROY } from './Resource/Constant';
import { Text, TextStyle } from './Style/StText';
import { ControlButtonViewer, ToggleButtonViewer, BUTTON_TOGGLE_STYLE } from './Style/StButton';

export const ControlButton = ({ onDeletegateClick, onDestroyClick }) => {
  return (
    <ControlButtonViewer>
      <Text style={TextStyle.button} onClick={onDeletegateClick}>
        {TEXT_BUTTON_DELEGATE}
      </Text>
      <Text style={TextStyle.button} onClick={onDestroyClick}>
        {TEXT_BUTTON_DESTROY}
      </Text>
    </ControlButtonViewer>
  );
};

export const ToggleButton = ({ player, onClick }) => {
  const initialColor = player.chatPosition === 'in' ? '#35d274' : '#808080';

  return (
    <ToggleButtonViewer className={player.id} style={{ color: initialColor }}>
      <VscCheck style={BUTTON_TOGGLE_STYLE} onClick={() => onClick(player)} />
    </ToggleButtonViewer>
  );
};

export const onDelegate = () => {};

export const onDestroy = () => {};

export const onToggle = (player, inplayerIdsInChat) => {
  if (player.chatPosition === 'in') {
    player.chatPosition = 'away';
    inplayerIdsInChat.splice(
      inplayerIdsInChat.findIndex(inplayerId => inplayerId === player.id),
      1,
    );
  } else {
    player.chatPosition = 'in';
    inplayerIdsInChat.push(player.id);
  }

  const doms = document.querySelectorAll('.' + player.id);
  doms.forEach(dom => {
    dom.style.color = dom.style.color[4] === '1' ? '#35d274' : '#808080';
  });
};
