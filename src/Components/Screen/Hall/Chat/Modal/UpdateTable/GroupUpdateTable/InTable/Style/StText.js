import styled from 'styled-components';

export const Text = styled.div`
  width: ${props => props.width};
  height: ${props => props.height};

  color: ${props => props.color};
  cursor: ${props => props.cursor};
  font-size: ${props => props.fontSize};
  font-weight: ${props => props.fontWeight};
  margin: ${props => props.margin};
  white-space: pre-wrap;
`;

export const TextStyle = {
  name: {
    width: '280px',
    color: '#000000',
    fontSize: '13px',
    fontWeight: '400',
  },
  button: {
    color: '#989898',
    cursor: 'pointer',
    fontSize: '10px',
  },
  chatposition: {
    own: {
      width: '120px',
      color: '#4260f7',
      fontSize: '13px',
      margin: '0 0 0 15px',
    },
    i: {
      width: '120px',
      color: '#35d274',
      fontSize: '13px',
      margin: '0 0 0 15px',
    },
    others: {
      width: '120px',
      fontSize: '13px',
      margin: '0 0 0 15px',
    },
  },
};
