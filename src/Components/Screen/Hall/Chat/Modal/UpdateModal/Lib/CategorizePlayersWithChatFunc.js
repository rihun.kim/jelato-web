export const categorizePlayersWithChat = _props => {
  const { _ownerIdInChat, _iplayer, _inplayersInChat, _inplayersInPlayground } = _props;
  let categorizedPlayers = { own: null, i: null, owni: null, others: [], inplayerIdsInChat: [] };

  // console.log('LIb', _inplayersInChat);

  if (_iplayer.id === _ownerIdInChat) {
    categorizedPlayers.owni = _iplayer;
    _iplayer.chatPosition = 'owni';
  } else {
    categorizedPlayers.i = _iplayer;
    _iplayer.chatPosition = 'in';
  }

  let inplayer;
  for (const player of _inplayersInPlayground) {
    inplayer = _inplayersInChat.find(inplayer => inplayer.id === player.id);

    if (inplayer) {
      categorizedPlayers.inplayerIdsInChat.push(inplayer.id);

      if (inplayer.id === _iplayer.id) continue;
      else if (inplayer.id === _ownerIdInChat) {
        categorizedPlayers.own = inplayer;
        inplayer.chatPosition = 'own';
      } else {
        categorizedPlayers.others.push(inplayer);
        inplayer.chatPosition = 'in';
      }
    } else {
      categorizedPlayers.others.push(player);
      player.chatPosition = 'away';
    }
  }

  return categorizedPlayers;
};
