import { useQuery } from '@apollo/client';
import { cloneDeep } from '@apollo/client/utilities';

import { READ_PLAYERS_IN_PLAYGROUND } from '../../../../Share/Gql';

export const UseQueryPlayground = iplayer => {
  const { loading, data } = useQuery(READ_PLAYERS_IN_PLAYGROUND, {
    variables: { playgroundId: iplayer.playground.id },
    fetchPolicy: 'cache-and-network',
  });

  return loading || data === undefined ? false : cloneDeep(data);
};
