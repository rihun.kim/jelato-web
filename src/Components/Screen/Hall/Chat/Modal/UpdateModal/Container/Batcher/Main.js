import React from 'react';

import Render from '../Render';
import { getCategorizedPlayersWithChat } from './Process';

const Main = ({ batchProps, renderProps }) => {
  const { iplayer, chat, remotePlayground } = batchProps;

  renderProps.chat = chat;
  renderProps.fplayersWithChat = getCategorizedPlayersWithChat(iplayer, chat, remotePlayground);

  return <Render renderProps={renderProps} />;
};

export default Main;
