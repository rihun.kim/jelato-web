import React from 'react';
import GroupUpdateModal from '../GroupUpdateModal/Main';

const Render = ({ renderProps }) => {
  const { chat, fplayersWithChat, toggleModal } = renderProps;

  if (chat.type === 'GROUP') return <GroupUpdateModal chat={chat} fplayersWithChat={fplayersWithChat} toggleModal={toggleModal} />;
  else if (chat.type === 'PERSON') return false;
};

export default Render;
