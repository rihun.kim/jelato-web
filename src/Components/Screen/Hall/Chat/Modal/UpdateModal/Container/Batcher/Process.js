import { cloneDeep } from '@apollo/client/utilities';

import { categorizePlayersWithChat } from '../../Lib/CategorizePlayersWithChatFunc';

export const getCategorizedPlayersWithChat = (iplayer, chat, remotePlayground) => {
  const inplayersInPlayground = remotePlayground.readPlayersInPlayground.players.filter(player => player.position !== 'wait');

  const inputProps = {
    _iplayer: cloneDeep(iplayer),
    _ownerIdInChat: chat.ownerId,
    _inplayersInChat: chat.players,
    _inplayersInPlayground: inplayersInPlayground,
  };

  return categorizePlayersWithChat(inputProps);
};
