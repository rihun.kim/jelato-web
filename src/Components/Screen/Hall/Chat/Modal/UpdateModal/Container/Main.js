import React, { useContext } from 'react';

import Fetcher from './Fetcher/Main';
import { ChatContext, RoomContext } from '../../../Share/Context';

const Main = ({ toggleModal }) => {
  const { iplayer, remoteChats } = useContext(ChatContext);
  const { clickProps } = useContext(RoomContext);

  return <Fetcher iplayer={iplayer} remoteChats={remoteChats} clickProps={clickProps} toggleModal={toggleModal} />;
};

export default Main;
