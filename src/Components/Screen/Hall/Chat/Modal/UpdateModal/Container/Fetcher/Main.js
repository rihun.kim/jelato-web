import React from 'react';

import Batcher from '../Batcher/Main';
import { UseQueryPlayground } from './Request';

const Main = ({ iplayer, remoteChats, clickProps, toggleModal }) => {
  const remotePlayground = UseQueryPlayground(iplayer);

  const batchProps = {
    iplayer: iplayer,
    chat: remoteChats.readChats.find(chat => chat.id === clickProps.clickChatId),
    remotePlayground: remotePlayground,
  };
  const renderProps = {
    toggleModal: toggleModal,
  };

  return remotePlayground && <Batcher batchProps={batchProps} renderProps={renderProps} />;
};

export default Main;
