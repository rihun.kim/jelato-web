import React from 'react';
import { IoMdClose } from 'react-icons/io';

import { CloseButtonViewer, BUTTON_CLOSE_STYLE } from './Style/StButton';

export const CloseButton = ({ onClick }) => (
  <CloseButtonViewer>
    <IoMdClose title="close" style={BUTTON_CLOSE_STYLE} onClick={onClick} />
  </CloseButtonViewer>
);

// export const onComplete = (playgroundId, inplayerIds, title, toggleModal, createChatMutation) => {
//   try {
//     if (inplayerIds.length === 1) alert('채팅을 함께할 멤버를 선택하세요.');
//     else {
//       createChatMutation({
//         variables: {
//           type: 'GROUP',
//           playgroundId: playgroundId,
//           playerIds: inplayerIds,
//           title: title,
//         },
//       });

//       toggleModal();
//     }
//   } catch (error) {
//     console.error(error);
//   }
// };
