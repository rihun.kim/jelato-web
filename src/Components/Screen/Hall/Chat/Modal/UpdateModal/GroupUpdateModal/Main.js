import React from 'react';

import Bezel from './Bezel';
import GroupUpdateTable from '../../UpdateTable/GroupUpdateTable/Main';
import { useInput, Input } from './Input';

const Main = ({ chat, fplayersWithChat, toggleModal }) => {
  const titleUseInput = useInput(chat.title);
  const inputAble = fplayersWithChat.owni ? true : false;

  return (
    <Bezel fplayersWithChat={fplayersWithChat} toggleModal={toggleModal}>
      <Input inputAble={inputAble} {...titleUseInput} />
      <GroupUpdateTable fplayersWithChat={fplayersWithChat} />
    </Bezel>
  );
};

export default Main;
