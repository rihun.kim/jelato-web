import styled from 'styled-components';

export const CloseButtonViewer = styled.div`
  // FOR TAG HIDING
`;

export const BUTTON_CLOSE_STYLE = {
  width: '17px',
  height: '17px',
  color: '#a5a2a2',
  cursor: 'pointer',
  position: 'absolute',
  right: 10,
  top: 10,
  zIndex: 3,
};
