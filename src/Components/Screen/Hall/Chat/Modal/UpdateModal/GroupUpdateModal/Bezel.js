import React, { useContext } from 'react';

import { Viewer } from './Style/StBezel';
import { CloseButton } from './Button';
import { ScreenContext } from '../../../Share/Context';
import { adjustModalTopPosition } from '../Lib/AdujstModalPositionFunc';

const Bezel = ({ children, toggleModal, fplayersWithChat }) => {
  const { screenRef } = useContext(ScreenContext);

  return (
    <Viewer top={adjustModalTopPosition(screenRef.current)}>
      <CloseButton
        onClick={() => {
          // 여기서 Bezel 에서 뮤테이션 날리면됨
          console.log(fplayersWithChat);
          toggleModal();
        }}
      />
      {children}
    </Viewer>
  );
};

export default Bezel;
