import React from 'react';

import { Text, TextStyle } from './Style/StText';
import { TEXT_CHATPOSITION_DEFAULT } from './Resource/Constant';

export const OthersNameplate = ({ player }) => {
  return (
    <>
      <Text className={player.id} style={TextStyle.chatposition.others}>
        {TEXT_CHATPOSITION_DEFAULT}
      </Text>
      <Text style={TextStyle.name}>{player.name}</Text>
    </>
  );
};
