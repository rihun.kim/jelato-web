import styled from 'styled-components';

export const ToggleButtonViewer = styled.div`
  width: 200px;
  height: 16px;

  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: flex-end;
`;

export const BUTTON_TOGGLE_STYLE = {
  width: '15px',
  height: '15px',
  cursor: 'pointer',
};
