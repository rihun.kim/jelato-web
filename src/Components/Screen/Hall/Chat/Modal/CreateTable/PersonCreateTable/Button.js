import React from 'react';
import { VscCheck } from 'react-icons/vsc';

import { ToggleButtonViewer, BUTTON_TOGGLE_STYLE } from './Style/StButton';

export const ToggleButton = ({ player, onClick }) => {
  const initialColor = player.chatPosition === 'in' ? '#35d274' : '#808080';

  return (
    <ToggleButtonViewer className={player.id} style={{ color: initialColor }}>
      <VscCheck style={BUTTON_TOGGLE_STYLE} onClick={() => onClick(player)} />
    </ToggleButtonViewer>
  );
};

export const onToggle = (player, inplayerIdsInChat) => {
  if (player.chatPosition === 'in') player.chatPosition = 'away';
  else player.chatPosition = 'in';

  if (inplayerIdsInChat.length !== 1) {
    const prevId = inplayerIdsInChat.pop();

    const doms = document.querySelectorAll('.' + prevId);
    doms.forEach(dom => {
      dom.style.color = dom.style.color[4] === '1' ? '#35d274' : '#808080';
    });
  }

  inplayerIdsInChat.push(player.id);

  const doms = document.querySelectorAll('.' + player.id);
  doms.forEach(dom => {
    dom.style.color = dom.style.color[4] === '1' ? '#35d274' : '#808080';
  });
};
