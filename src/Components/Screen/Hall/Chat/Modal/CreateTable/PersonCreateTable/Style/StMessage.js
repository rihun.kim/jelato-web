import styled from 'styled-components';

export const Viewer = styled.div`
  width: 200px;

  display: flex;
  flex-direction: row;
  justify-content: flex-end;

  *:not(:last-child) {
    margin: 0 8px 0 0;
  }
  *:first-child {
    margin: 0 12px 0 0;
  }
  *:last-child {
    margin: 0 0 0 0;
  }
`;
