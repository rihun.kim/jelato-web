import React from 'react';

import { OthersNameplate } from './Nameplate';
import { onToggle, ToggleButton } from './Button';
import { Viewer } from './Style/StMain';

const Main = ({ fplayers }) => {
  const onToggleClick = player => {
    onToggle(player, fplayers.inplayerIdsInChat);
  };

  return (
    <>
      {fplayers.others.map(player => (
        <Viewer key={player.id}>
          <OthersNameplate player={player} />
          <ToggleButton player={player} onClick={onToggleClick} />{' '}
        </Viewer>
      ))}
    </>
  );
};

export default Main;
