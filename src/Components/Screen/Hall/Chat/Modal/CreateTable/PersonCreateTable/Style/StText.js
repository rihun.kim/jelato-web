import styled from 'styled-components';

export const Text = styled.div`
  width: ${props => props.width};
  height: ${props => props.height};

  color: ${props => props.color};
  cursor: ${props => props.cursor};
  font-size: ${props => props.fontSize};
  font-weight: ${props => props.fontWeight};
  margin: ${props => props.margin};
  white-space: nowrap;
`;

export const TextStyle = {
  name: {
    width: '280px',
    color: '#000000',
    fontSize: '13px',
    fontWeight: '400',
  },
  chatposition: {
    others: {
      width: '120px',
      color: '#808080',
      fontSize: '13px',
      margin: '0 0 0 15px',
    },
  },
};
