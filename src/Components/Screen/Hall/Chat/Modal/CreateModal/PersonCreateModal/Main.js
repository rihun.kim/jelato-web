import React from 'react';
import { useMutation } from '@apollo/client';

import Bezel from './Bezel';
import PersonCreateTable from '../../CreateTable/PersonCreateTable/Main';
import { onComplete } from './Button';
import { CREATE_CHAT } from '../../../Share/Gql';

const Main = ({ fplayers, toggleModal }) => {
  const [createChatMutation] = useMutation(CREATE_CHAT);

  const onCompleteClick = () => {
    onComplete(fplayers.owni.playground.id, fplayers.inplayerIdsInChat, toggleModal, createChatMutation);
  };

  return (
    <Bezel onCompleteClick={onCompleteClick} toggleModal={toggleModal}>
      <PersonCreateTable fplayers={fplayers} />
    </Bezel>
  );
};

export default Main;
