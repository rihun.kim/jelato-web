import React, { useContext } from 'react';

import { Viewer } from './Style/StBezel';
import { CloseButton, SaveButton } from './Button';
import { ScreenContext } from '../../../Share/Context';
import { adjustModalTopPosition } from '../Lib/AdujstModalPositionFunc';

const Bezel = ({ children, onCompleteClick, toggleModal }) => {
  const { screenRef } = useContext(ScreenContext);

  return (
    <Viewer top={adjustModalTopPosition(screenRef.current)}>
      <CloseButton onClick={toggleModal} />
      <SaveButton onClick={onCompleteClick} />
      {children}
    </Viewer>
  );
};

export default Bezel;
