import React from 'react';
import { IoMdClose, IoMdRadioButtonOff } from 'react-icons/io';

import { CloseButtonViewer, SaveButtonViewer, BUTTON_CLOSE_STYLE, BUTTON_SAVE_STYLE } from './Style/StButton';

export const CloseButton = ({ onClick }) => (
  <CloseButtonViewer>
    <IoMdClose title="close" style={BUTTON_CLOSE_STYLE} onClick={onClick} />
  </CloseButtonViewer>
);

export const SaveButton = ({ onClick }) => (
  <SaveButtonViewer>
    <IoMdRadioButtonOff title="save" style={BUTTON_SAVE_STYLE} onClick={onClick} />
  </SaveButtonViewer>
);

export const onComplete = (playgroundId, inplayerIds, toggleModal, createChatMutation) => {
  try {
    if (inplayerIds.length === 1) alert('채팅을 함께할 멤버를 선택하세요.');
    else {
      createChatMutation({
        variables: {
          type: 'PERSON',
          playgroundId: playgroundId,
          playerIds: inplayerIds,
        },
      });

      toggleModal();
    }
  } catch (error) {
    console.error(error);
  }
};
