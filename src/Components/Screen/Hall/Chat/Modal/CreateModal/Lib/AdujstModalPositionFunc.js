export const adjustModalTopPosition = _screenRef => {
  const screen = _screenRef;

  return screen.clientHeight > 300 ? screen.scrollTop + screen.clientHeight / 2 - 300 / 2 : screen.scrollTop + 20;
};
