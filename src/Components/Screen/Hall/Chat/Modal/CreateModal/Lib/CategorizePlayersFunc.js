export const categorizePlayers = _props => {
  const { _iplayer, _players } = _props;
  let categorizedPlayers = { owni: null, others: [], inplayerIdsInChat: [] };

  for (const player of _players) {
    if (player.id === _iplayer.id) {
      categorizedPlayers.owni = _iplayer;
      categorizedPlayers.inplayerIdsInChat.push(player.id);
    } else {
      player.chatPosition = 'away';
      categorizedPlayers.others.push(player);
    }
  }

  return categorizedPlayers;
};
