import React, { useContext } from 'react';

import Fetcher from './Fetcher/Main';
import { ChatContext } from '../../../Share/Context';

const Main = ({ chatType, toggleModal }) => {
  const { iplayer } = useContext(ChatContext);

  return <Fetcher iplayer={iplayer} chatType={chatType} toggleModal={toggleModal} />;
};

export default Main;
