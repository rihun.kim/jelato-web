import React from 'react';

import Batcher from '../Batcher/Main';
import { UseQueryPlayground } from './Request';

const Main = ({ iplayer, chatType, toggleModal }) => {
  const remotePlayground = UseQueryPlayground(iplayer);

  const batchProps = {
    iplayer: iplayer,
    remotePlayground: remotePlayground,
  };
  const renderProps = {
    chatType: chatType,
    toggleModal: toggleModal,
  };

  return remotePlayground && <Batcher batchProps={batchProps} renderProps={renderProps} />;
};

export default Main;
