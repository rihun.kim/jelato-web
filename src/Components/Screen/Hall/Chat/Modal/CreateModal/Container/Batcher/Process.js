import { categorizePlayers } from '../../Lib/CategorizePlayersFunc';

export const getCategorizedPlayers = (iplayer, remotePlayground) => {
  const inputProps = {
    _iplayer: iplayer,
    _players: remotePlayground.readPlayersInPlayground.players,
  };

  return categorizePlayers(inputProps);
};
