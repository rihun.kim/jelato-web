import React from 'react';

import Render from '../Render';
import { getCategorizedPlayers } from './Process';

const Main = ({ batchProps, renderProps }) => {
  const { iplayer, remotePlayground } = batchProps;

  renderProps.fplayers = getCategorizedPlayers(iplayer, remotePlayground);

  return <Render renderProps={renderProps} />;
};

export default Main;
