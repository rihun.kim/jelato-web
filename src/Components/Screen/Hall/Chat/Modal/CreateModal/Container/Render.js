import React from 'react';

import GroupCreateModal from '../GroupCreateModal/Main';
import PersonCreateModal from '../PersonCreateModal/Main';

const Render = ({ renderProps }) => {
  const { chatType, fplayers, toggleModal } = renderProps;

  if (chatType === 'GROUP') return <GroupCreateModal fplayers={fplayers} toggleModal={toggleModal} />;
  else if (chatType === 'PERSON') return <PersonCreateModal fplayers={fplayers} toggleModal={toggleModal} />;
};

export default Render;
