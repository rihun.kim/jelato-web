import React from 'react';
import { useMutation } from '@apollo/client';

import Bezel from './Bezel';
import GroupCreateTable from '../../CreateTable/GroupCreateTable/Main';
import { useInput, Input } from './Input';
import { onComplete } from './Button';
import { CREATE_CHAT } from '../../../Share/Gql';
import { MODAL_CREATE_TITLE } from './Resource/Constant';

const Main = ({ fplayers, toggleModal }) => {
  const [createChatMutation] = useMutation(CREATE_CHAT);
  const titleUseInput = useInput(MODAL_CREATE_TITLE);

  const onCompleteClick = () => {
    onComplete(fplayers.owni.playground.id, fplayers.inplayerIdsInChat, titleUseInput.value, toggleModal, createChatMutation);
  };

  return (
    <Bezel onCompleteClick={onCompleteClick} toggleModal={toggleModal}>
      <Input {...titleUseInput} />
      <GroupCreateTable fplayers={fplayers} />
    </Bezel>
  );
};

export default Main;
