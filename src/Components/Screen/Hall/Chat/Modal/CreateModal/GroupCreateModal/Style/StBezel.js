import styled from 'styled-components';

export const Viewer = styled.div`
  width: 700px;
  height: 300px;

  position: absolute;
  left: 0;
  right: 0;
  top: ${props => props.top}px;
  bottom: 0;

  background-color: #ffffff;
  border-radius: 2px;
  box-shadow: 1px 3px 4px rgba(0, 0, 0, 0.19), 0 1px 0 rgba(0, 0, 0, 0.23);
  margin: 0 auto 0 auto;
  overflow-y: auto;
  padding: 40px 25px 20px 25px;
  z-index: 3;
`;
