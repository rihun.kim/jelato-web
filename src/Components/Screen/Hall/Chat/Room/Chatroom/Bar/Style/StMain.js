import styled from 'styled-components';

export const Viewer = styled.div`
  width: 100%;
  height: 50px;

  display: flex;
  flex-direction: row;
  justify-content: flex-end;
  align-items: center;
`;
