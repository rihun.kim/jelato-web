import styled from 'styled-components';

export const CloseButtonViewer = styled.div`
  width: 17px;
  height: 17px;

  margin: 0 10px 0 0;
`;

export const SetupButtonViewer = styled.div`
  width: 17px;
  height: 17px;

  margin: 0 5px 0 0;
`;

export const BUTTON_CLOSE_STYLE = {
  width: '17px',
  height: '17px',
  color: '#a5a2a2',
  cursor: 'pointer',
};

export const BUTTON_SETTING_STYLE = {
  width: '17px',
  height: '17px',
  color: '#a5a2a2',
  cursor: 'pointer',
};
