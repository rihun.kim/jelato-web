import React, { useState, useContext } from 'react';

import UpdateModal from '../../../Modal/UpdateModal/Container/Main';
import { Viewer } from './Style/StMain';
import { CloseButton, SetupButton } from './Button';
import { RoomContext } from '../../../Share/Context';

const Main = () => {
  const { clickProps } = useContext(RoomContext);
  const [updateModal, setUpdateModal] = useState(false);

  const toggleUpdateModal = () => {
    // mutation 을 날려야함.
    updateModal ? setUpdateModal(false) : setUpdateModal(true);
  };

  const onSetupClick = () => toggleUpdateModal();
  const onCloseClick = () => clickProps.setClickChatId(false);

  return (
    <Viewer>
      <SetupButton onClick={onSetupClick} />
      <CloseButton onClick={onCloseClick} />
      {updateModal && <UpdateModal toggleModal={toggleUpdateModal} />}
    </Viewer>
  );
};

export default Main;
