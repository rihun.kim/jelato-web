import React from 'react';

import { INormalMessage, NormalMessage } from './NormalMessage';
import { FormalMessage } from './FormalMessage';

const Main = ({ message }) => {
  if (message.type === 'NORMAL') return message.isIMessage ? <INormalMessage message={message} /> : <NormalMessage message={message} />;
  else if (message.type === 'FORMAL') return <FormalMessage message={message} />;
};

export default Main;
