import React from 'react';

import { FormalMessageViewer, Text, TextStyle } from './Style/StFormalMessage';

export const FormalMessage = ({ message }) => (
  <FormalMessageViewer>
    <Text style={TextStyle.text}>{message.text}</Text>
  </FormalMessageViewer>
);
