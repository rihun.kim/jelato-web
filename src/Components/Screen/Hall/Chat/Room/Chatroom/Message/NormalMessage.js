import React from 'react';

import { NormalMessageViewer, Text, TextStyle } from './Style/StNormalMessage';

export const INormalMessage = ({ message }) => (
  <NormalMessageViewer>
    <Text style={TextStyle.iname}>{message.owner.name}</Text>
    <Text style={TextStyle.itext}>{message.text}</Text>
    <Text style={TextStyle.time}>{message.receivedAt}</Text>
  </NormalMessageViewer>
);

export const NormalMessage = ({ message }) => (
  <NormalMessageViewer>
    <Text style={TextStyle.name}>{message.owner.name}</Text>
    <Text style={TextStyle.text}>{message.text}</Text>
    <Text style={TextStyle.time}>{message.receivedAt}</Text>
  </NormalMessageViewer>
);
