import styled from 'styled-components';

export const FormalMessageViewer = styled.div`
  margin: 0 auto 20px auto;
`;

export const Text = styled.div`
  width: ${props => props.width};
  height: ${props => props.height};

  color: ${props => props.color};
  float: ${props => props.float};
  font-size: ${props => props.fontSize};
  font-weight: ${props => props.fontWeight};
  margin: ${props => props.margin};
  white-space: pre-wrap;
`;

export const TextStyle = {
  text: {
    color: '#a0a0a0',
    fontSize: '11px',
  },
};
