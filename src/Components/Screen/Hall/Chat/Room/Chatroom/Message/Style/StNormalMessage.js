import styled from 'styled-components';

export const NormalMessageViewer = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: flex-start;

  margin: 0 0 10px 20px;
`;

export const Text = styled.div`
  width: ${props => props.width};
  height: ${props => props.height};

  color: ${props => props.color};
  float: ${props => props.float};
  font-size: ${props => props.fontSize};
  font-weight: ${props => props.fontWeight};
  margin: ${props => props.margin};
  white-space: pre-wrap;
`;

export const TextStyle = {
  iname: {
    backgroundColor: '#ffeb00',
    borderRadius: '2px',
    color: '#000000',
    fontSize: '15px',
    fontWeight: '400',
    padding: '8px 8px 8px 8px',
  },
  itext: {
    backgroundColor: '#ffeb00',
    borderRadius: '2px',
    color: '#000000',
    fontSize: '15px',
    fontWeight: '400',
    maxWidth: '250px',
    padding: '8px 8px 8px 8px',
    whiteSpace: 'pre-wrap',
  },
  name: {
    backgroundColor: '#ffffff',
    borderRadius: '2px',
    color: '#000000',
    fontSize: '15px',
    fontWeight: '400',
    padding: '8px 8px 8px 8px',
  },
  text: {
    backgroundColor: '#ffffff',
    borderRadius: '2px',
    color: '#000000',
    fontSize: '15px',
    fontWeight: '400',
    maxWidth: '250px',
    padding: '8px 8px 8px 8px',
    whiteSpace: 'pre-wrap',
  },
  time: {
    color: '#a0a0a0',
    fontSize: '9px',
    margin: '5px auto 15px auto',
    padding: '2px 7px 3px 7px',
  },
};
