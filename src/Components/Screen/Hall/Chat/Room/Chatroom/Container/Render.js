import React, { useState, useCallback } from 'react';

import Bar from '../Bar/Main';
import Board from '../Board/Main';
import Typer from '../Typer/Main';
import { ChatroomContext } from '../../../Share/Context';
import { Viewer } from './Style/StRender';

const Render = ({ chat }) => {
  const [renderId, setRenderTarget] = useState();
  const FRenderChatroom = useCallback(() => setRenderTarget({}), []);

  // console.log('Chatroom/Render');

  const renderProps = {
    renderId: renderId,
    FRenderChatroom: FRenderChatroom,
  };

  return (
    <ChatroomContext.Provider value={{ chat, renderProps }}>
      <Viewer>
        <Bar />
        <Board />
        <Typer />
      </Viewer>
    </ChatroomContext.Provider>
  );
};

export default Render;
