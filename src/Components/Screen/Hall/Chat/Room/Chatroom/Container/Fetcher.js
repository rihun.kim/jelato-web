import React from 'react';

import Render from './Render';

const Fetcher = ({ remoteChats, clickProps }) => {
  const chat = remoteChats.readChats.find(chat => chat.id === clickProps.clickChatId);

  return <Render chat={chat} />;
};

export default Fetcher;
