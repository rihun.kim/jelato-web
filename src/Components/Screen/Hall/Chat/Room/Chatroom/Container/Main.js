import React, { useContext } from 'react';

import { ChatContext, RoomContext } from '../../../Share/Context';
import Fetcher from './Fetcher';

const Main = () => {
  const { remoteChats } = useContext(ChatContext);
  const { clickProps } = useContext(RoomContext);

  // console.log('Chatroom');

  return <Fetcher remoteChats={remoteChats} clickProps={clickProps} />;
};

export default Main;
