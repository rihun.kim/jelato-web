import styled from 'styled-components';

export const Viewer = styled.div`
  width: 600px;
  height: 100%;

  display: flex;
  flex-direction: column;

  background-color: #f6f6f6;
  border-right: 1px solid #e0e0e0;
`;
