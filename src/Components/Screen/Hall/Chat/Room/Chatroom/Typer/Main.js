import React, { useContext } from 'react';
import { useMutation } from '@apollo/client';

import { Viewer } from './Style/StMain';
import { TextareaInput, useInput } from './Input';
import { onSend, SendButton } from './Button';
import { CREATE_MESSAGE } from '../../../Share/Gql';
import { ChatContext, ChatroomContext } from '../../../Share/Context';

const Main = () => {
  const { iplayer } = useContext(ChatContext);
  const { chat, renderProps } = useContext(ChatroomContext);
  const [createMessageMutation] = useMutation(CREATE_MESSAGE);
  const textUseInput = useInput('');

  const inputProps = {
    iplayer: iplayer,
    createMessageMutation: createMessageMutation,
    chat: chat,
    FRenderChatroom: renderProps.FRenderChatroom,
    textUseInput: textUseInput,
  };

  const onSendClick = () => onSend(inputProps);

  return (
    <Viewer>
      <TextareaInput {...textUseInput} onClick={onSendClick} />
      <SendButton onClick={onSendClick} />
    </Viewer>
  );
};

export default Main;
