import React, { useState } from 'react';

import { TextareaViewer } from './Style/StInput';

export const useInput = () => {
  const [value, setValue] = useState('');

  const onChange = e => setValue(e.target.value);
  const onRefresh = () => setValue('');

  return { value, onChange, onRefresh };
};

const onKeyDown = (e, onClick) => {
  if (e.keyCode === 13 && !e.shiftKey) {
    onClick();
    e.preventDefault();
  }
};

export const TextareaInput = ({ onChange, onClick, value }) => (
  <TextareaViewer autoFocus="autofocus" value={value} onChange={onChange} onKeyDown={e => onKeyDown(e, onClick)} />
);
