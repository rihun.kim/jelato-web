import styled from 'styled-components';

export const SendButtonViewer = styled.div`
  width: 80px;
  height: 80px;

  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;

  cursor: pointer;
`;

export const BUTTON_SEND_STYLE = {
  width: '17px',
  height: '17px',
  color: '#717171',
};
