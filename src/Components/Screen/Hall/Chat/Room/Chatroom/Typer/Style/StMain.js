import styled from 'styled-components';

export const Viewer = styled.div`
  width: 100%;
  height: 80px;

  display: flex;
  flex-direction: row;
  justify-content: flex-start;
  align-items: center;
  flex-shrink: 0;
  flex-grow: 1;

  background-color: #ffffff;
`;
