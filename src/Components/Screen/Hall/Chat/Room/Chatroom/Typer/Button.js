import React from 'react';
import { MdSend } from 'react-icons/md';

import { BUTTON_SEND_STYLE, SendButtonViewer } from './Style/StButton';

export const SendButton = ({ onClick }) => (
  <SendButtonViewer onClick={onClick}>
    <MdSend title="send" style={BUTTON_SEND_STYLE} />
  </SendButtonViewer>
);

export const onSend = props => {
  const unixTime = new Date().getTime();

  props.chat.messages.push({
    id: unixTime,
    owner: props.iplayer,
    type: 'NORMAL',
    text: props.textUseInput.value,
    createdAt: unixTime,
  });

  // props.createMessageMutation({
  //   variables: {
  //     chatId: props.chat.id,
  //     playerId: props.iplayer.id,
  //     text: props.textUseInput.value,
  //     type: 'NORMAL',
  //   },
  // });

  props.FRenderChatroom();
  props.textUseInput.onRefresh();
};

// const checkBlanks = props => {
//   if (props.titleUseInput.value === '') alert(MODAL_CREATE_CHECK_TITLE);
//   else if (props.storyUseInput.value === '') alert(MODAL_CREATE_CHECK_STORY);
//   else return true;
// };

// export const onComplete = async props => {
//   try {
//     if (checkBlanks(props)) {
//       await props.createNoticeMutation({
//         variables: {
//           playerId: props.iplayer.id,
//           playgroundId: props.iplayer.playground.id,
//           title: props.titleUseInput.value,
//           story: props.storyUseInput.value,
//         },
//       });

//       window.location.reload();
//     }
//   } catch (e) {
//     console.error(e);
//   }
// };
