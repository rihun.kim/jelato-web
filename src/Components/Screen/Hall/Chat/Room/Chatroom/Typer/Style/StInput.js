import styled from 'styled-components';

export const TextareaViewer = styled.textarea`
  width: 100%;
  height: 80px;

  border: 0;
  font-size: 14px;
  padding: 10px 15px 10px 15px;
  resize: none;
  white-space: pre-wrap;
`;
