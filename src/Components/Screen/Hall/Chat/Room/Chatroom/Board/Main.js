import React, { useContext } from 'react';

import { ChatroomContext } from '../../../Share/Context';
import Fetcher from './Fetcher/Main';

const Main = () => {
  const { chat } = useContext(ChatroomContext);

  return <Fetcher chat={chat} />;
};

export default Main;
