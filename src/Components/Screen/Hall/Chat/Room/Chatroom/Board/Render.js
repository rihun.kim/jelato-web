import React, { useRef, useContext, useEffect } from 'react';

import Message from '../Message/Main';
import { onFetch, FetchButton } from './Button';
import { Viewer } from './Style/StRender';
import { ChatContext, ChatroomContext, RoomContext } from '../../../Share/Context';
import { categorizeMessages } from './Lib/CategorizeMessagesFunc';

const Render = ({ chat, fetchProps }) => {
  const { iplayer } = useContext(ChatContext);
  const { clickProps } = useContext(RoomContext);
  const { renderProps } = useContext(ChatroomContext);
  const viewerEndRef = useRef();

  const messages = categorizeMessages(chat.messages, iplayer.id);

  const onFetchClick = () => onFetch(chat, fetchProps);

  useEffect(() => {
    viewerEndRef.current.scrollIntoView({ alignToTop: false });
  }, [clickProps.clickChat, renderProps.renderId]);

  return (
    <Viewer>
      <FetchButton fetchProps={fetchProps} onClick={onFetchClick} />
      {messages.map(message => (
        <Message key={message.id} message={message} />
      ))}
      <div ref={viewerEndRef} />
    </Viewer>
  );
};

export default Render;

// TODO 메세지가 새로 도착하면, 표시만 나도록

// fetch 할 땐, 맨 아래로 끌고 내려오면 안됨 ===> 커버됨, scrollIntoView 가 동작하지 않으므로

// 아래의 useEffect 로 커버됨
// 최초에 보드가 켜지면, 맨 아래로감
// 자신이 글을 써도, 맨 아래로감 ==> Typer 에서 FRender 하므로
