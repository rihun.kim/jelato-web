export const convertJSDateTimetoMessageReceiveTime = _jsDateTime => {
  let hh = _jsDateTime.getHours();
  let mm = _jsDateTime.getMinutes();

  if (hh === 12) hh = 'PM ' + hh;
  else if (hh === 24) hh = 'AM ' + (hh - 12);
  else hh = hh < 12 ? 'AM ' + hh : 'PM ' + (hh - 12);

  mm = mm < 10 ? '0' + mm : mm;

  return hh + ':' + mm;
};

export const convertJSDateTimetoMessageGroupTime = _jsDateTime => {
  const time = [_jsDateTime.getFullYear(), _jsDateTime.getMonth() + 1, _jsDateTime.getDate(), _jsDateTime.getDay()];
  const week = [' Sun', ' Mon', ' Tue', ' Wed', ' Thu', ' Fri', ' Sat'];

  time[1] = time[1] < 10 ? '0' + time[1] : time[1];
  time[2] = time[2] < 10 ? '0' + time[2] : time[2];

  return time[0] + '.' + time[1] + '.' + time[2] + week[time[3]];
};
