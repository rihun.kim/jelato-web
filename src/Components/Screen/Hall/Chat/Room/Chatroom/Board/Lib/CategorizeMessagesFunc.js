import { convertUnixTimeToJSDateTime } from './ConvertUnixTimeFunc';
import { convertJSDateTimetoMessageGroupTime, convertJSDateTimetoMessageReceiveTime } from './ConvertJSDateTimeFunc';

export const categorizeMessages = (_messages, _iplayerId) => {
  let jsDateTime, previousMessageTime, currentMessageTime;
  let categorizedMessages = [];

  for (const _message of _messages) {
    jsDateTime = convertUnixTimeToJSDateTime(_message.createdAt);
    _message.receivedAt = convertJSDateTimetoMessageReceiveTime(jsDateTime);

    currentMessageTime = ('' + jsDateTime).substr(0, 10);

    if (_message.type === 'FORMAL') _message.isIMessage = false;
    else _message.isIMessage = _message.owner.id === _iplayerId ? true : false;

    if (previousMessageTime === currentMessageTime) categorizedMessages.push(_message);
    else {
      previousMessageTime = currentMessageTime;
      categorizedMessages.push({ id: jsDateTime, text: convertJSDateTimetoMessageGroupTime(jsDateTime), type: 'FORMAL' }, _message);
    }
  }

  return categorizedMessages;
};
