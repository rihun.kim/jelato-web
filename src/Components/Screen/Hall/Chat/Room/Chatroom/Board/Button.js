import React from 'react';
import { RiArrowDownSLine } from 'react-icons/ri';
import Loader from 'react-loader-spinner';

import { BUTTON_FETCH_STYLE, FetchButtonViewer, SpinnerViewer } from './Style/StButton';

export const FetchButton = ({ fetchProps, onClick }) => {
  if (fetchProps.fetchable) {
    return fetchProps.loading ? (
      <Spinner />
    ) : (
      <FetchButtonViewer onClick={onClick}>
        <RiArrowDownSLine title="more" style={BUTTON_FETCH_STYLE} />
      </FetchButtonViewer>
    );
  } else return false;
};

const Spinner = () => (
  <SpinnerViewer>
    <Loader type="ThreeDots" color="#a5a2a2" width="17px" height="17px" />
  </SpinnerViewer>
);

export const onFetch = (chat, fetchProps) => {
  fetchProps.query({
    variables: {
      chatId: chat.id,
      messageId: chat.messages[0].id,
    },
  });
};
