import { cloneDeep } from '@apollo/client/utilities';

export const memoRemoteMessages = (chat, remoteMessages) => {
  const fetchedMessages = remoteMessages.readMessages;

  if (chat.id === fetchedMessages[0].chat.id && chat.messages[0].id !== fetchedMessages[0].id)
    chat.messages.unshift(...cloneDeep(fetchedMessages));
};
