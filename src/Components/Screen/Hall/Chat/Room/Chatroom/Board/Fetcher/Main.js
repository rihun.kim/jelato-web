import React, { useMemo } from 'react';
import { useLazyQuery } from '@apollo/client';

import Render from '../Render';
import { memoRemoteMessages } from './Memo';
import { READ_MESSAGES } from '../../../../Share/Gql';

const Main = ({ chat }) => {
  const [fetchMsgsLQuery, { loading: fetchMsgsLoading, data: remoteMessages }] = useLazyQuery(READ_MESSAGES);

  useMemo(() => {
    if (remoteMessages) {
      if (remoteMessages.readMessages.length > 0) memoRemoteMessages(chat, remoteMessages);
    }
  }, [chat, remoteMessages]);

  const fetchProps = {
    query: fetchMsgsLQuery,
    loading: fetchMsgsLoading,
    fetchable: chat.messages[0].text[0] !== '새',
  };

  return <Render chat={chat} fetchProps={fetchProps} />;
};

export default Main;
