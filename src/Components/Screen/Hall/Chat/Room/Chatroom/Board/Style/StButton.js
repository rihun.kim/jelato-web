import styled from 'styled-components';

export const FetchButtonViewer = styled.div`
  width: 17px;
  height: 17px;

  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;

  cursor: pointer;
  margin: 0 auto 20px auto;
`;

export const BUTTON_FETCH_STYLE = {
  width: '17px',
  height: '17px',
  color: '#a5a2a2',
};

export const SpinnerViewer = styled.div`
  width: 17px;
  height: 17px;

  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;

  margin: 0 auto 20px auto;
`;
