import { getTitleInChat } from './GetTitleInChatFunc';
import { getLastMessageInChat } from './GetMessageInChatFunc';
import { convertUnixTimeToJSDateTime } from './ConvertUnixTimeFunc';
import { convertJSDateTimetoMessageReceiveTime } from './ConvertJSDateTimeFunc';

export const extractElementInChat = (_chat, _iplayer) => {
  const element = {};
  const lastMessage = getLastMessageInChat(_chat);

  element.title = getTitleInChat(_chat, _iplayer);
  element.text = lastMessage.text;
  element.receivedAt = convertJSDateTimetoMessageReceiveTime(convertUnixTimeToJSDateTime(lastMessage.createdAt));

  return element;
};
