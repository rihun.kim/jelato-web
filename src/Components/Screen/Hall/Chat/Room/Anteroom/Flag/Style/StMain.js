import styled from 'styled-components';

export const Viewer = styled.div`
  width: 220px;
  height: 20px;

  background-color: ${props => (props.clicked ? '#ffeb3b82' : '#ffffff')};
  cursor: pointer;
  margin: 5px auto 0 auto;

  &:hover {
    background-color: #ffeb3b82;
  }
`;
