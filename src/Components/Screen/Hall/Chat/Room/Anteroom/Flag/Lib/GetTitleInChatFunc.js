const makeCommaStringWithLastI = (_players, _iplayer) => {
  let commaString = '';

  _players.filter(player => player.id !== _iplayer.id && (commaString += player.name + ', '));

  return commaString + _iplayer.name;
};

export const getTitleInChat = (_chat, _iplayer) => {
  if (_chat.title === 'NONE') return makeCommaStringWithLastI(_chat.players, _iplayer);
  else return _chat.title;
};
