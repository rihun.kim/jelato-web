import React, { useContext } from 'react';

import { Viewer } from './Style/StMain';
import { Text, TextStyle } from './Style/StText';
import { RoomContext } from '../../../Share/Context';

const Main = ({ chat }) => {
  const { clickProps } = useContext(RoomContext);

  const onClicked = () => (!!clickProps.clickChatId && clickProps.clickChatId === chat.id ? true : false);
  const onOpenClick = () => clickProps.setClickChatId(chat.id);

  return (
    <Viewer clicked={onClicked()} onClick={onOpenClick}>
      <Text style={TextStyle.title}>{chat.title}</Text>
      {/* {상대방 이름을 표기하영야 함} */}
    </Viewer>
  );
};

export default Main;
