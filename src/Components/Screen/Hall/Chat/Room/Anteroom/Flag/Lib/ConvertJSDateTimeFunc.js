export const convertJSDateTimetoMessageReceiveTime = _jsDateTime => {
  const time = new Date();

  const cTime = [time.getFullYear(), time.getMonth(), time.getDate()];
  const rTime = [
    _jsDateTime.getFullYear(),
    _jsDateTime.getMonth() + 1,
    _jsDateTime.getDate(),
    _jsDateTime.getHours(),
    _jsDateTime.getMinutes(),
  ];

  if (rTime[0] === cTime[0])
    if (rTime[1] === cTime[1])
      if (rTime[2] === cTime[2]) return time[3] <= 12 ? 'AM ' + time[3] + ':' + time[4] : 'PM ' + (time[3] - 12) + ':' + time[4];
      else if (rTime[2] + 1 === cTime[2]) return 'Yesterday';
      else return rTime[1] + '.' + rTime[2];
    else return rTime[0] + '.' + rTime[1] + '.' + rTime[2];
};
