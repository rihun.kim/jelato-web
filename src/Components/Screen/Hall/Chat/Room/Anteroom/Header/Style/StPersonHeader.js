import styled from 'styled-components';

export const Viewer = styled.div`
  width: 220px;
  height: 25px;

  display: flex;
  flex-direction: row;

  margin: 50px auto 0 auto;
  border-bottom: 1px dotted #e0e0e0;
`;
