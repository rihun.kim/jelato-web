import React, { useState } from 'react';

import Create from '../../../Modal/CreateModal/Container/Main';
import { CreateButton } from './Button';
import { Text, TextStyle } from './Style/StText';
import { Viewer } from './Style/StPersonHeader';

const Main = () => {
  const [createModal, setCreateModal] = useState(false);

  const toggleCreateModal = () => {
    createModal ? setCreateModal(false) : setCreateModal(true);
  };

  return (
    <Viewer>
      {createModal && <Create chatType="PERSON" toggleModal={toggleCreateModal} />}
      <Text style={TextStyle.title}>1:1 Chats</Text>
      <CreateButton onClick={toggleCreateModal} />
    </Viewer>
  );
};

export default Main;
