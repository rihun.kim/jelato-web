import React, { useState } from 'react';

import Create from '../../../Modal/CreateModal/Container/Main';
import { CreateButton } from './Button';
import { Text, TextStyle } from './Style/StText';
import { Viewer } from './Style/StGroupHeader';

const Main = () => {
  const [createModal, setCreateModal] = useState(false);

  const toggleCreateModal = () => {
    createModal ? setCreateModal(false) : setCreateModal(true);
  };

  return (
    <Viewer>
      {createModal && <Create chatType="GROUP" toggleModal={toggleCreateModal} />}
      <Text style={TextStyle.title}>Group Chats</Text>
      <CreateButton onClick={toggleCreateModal} />
    </Viewer>
  );
};

export default Main;
