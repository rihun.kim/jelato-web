import styled from 'styled-components';

export const CreateButtonViewer = styled.div`
  width: 15px;
  height: 15px;

  margin: 2px 0 auto 0;
`;

export const ButtonStyle = {
  create: {
    width: '15px',
    height: '15px',
    color: '#484848',
    cursor: 'pointer',
  },
};
