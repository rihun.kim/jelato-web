import React from 'react';
import { RiChatNewLine } from 'react-icons/ri';

import { CreateButtonViewer, ButtonStyle } from './Style/StButton';

export const CreateButton = ({ onClick }) => {
  return (
    <CreateButtonViewer onClick={onClick}>
      <RiChatNewLine title="create" style={ButtonStyle.create} />
    </CreateButtonViewer>
  );
};
