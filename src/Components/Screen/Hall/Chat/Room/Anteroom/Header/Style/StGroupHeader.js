import styled from 'styled-components';

export const Viewer = styled.div`
  width: 220px;
  height: 25px;

  display: flex;
  flex-direction: row;

  margin: 21px auto 0 auto;
  border-bottom: 1px dotted #e0e0e0;
`;

// import styled from 'styled-components';

// export const Viewer = styled.div`
//   width: 100%;
//   height: 1px;

//   padding: 31px 0 21px 0;
// `;

// export const InnerViewer = styled.div`
//   width: 220px;
//   height: 1px;

//   display: flex;
//   flex-direction: row;
//   justify-content: flex-end;

//   border-bottom: 1px dotted #e0e0e0;
//   margin: auto auto auto auto;
// `;
