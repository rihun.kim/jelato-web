import styled from 'styled-components';

export const Text = styled.div`
  width: ${props => props.width};
  height: ${props => props.height};

  color: ${props => props.color};
  font-size: ${props => props.fontSize};
  font-weight: ${props => props.fontWeight};
  margin: ${props => props.margin};
  text-overflow: ${props => props.textOverflow};
  overflow: hidden;
  white-space: nowrap;
`;

export const TextStyle = {
  title: {
    width: '180px',
    height: '20px',
    color: '#000000',
    fontSize: '15px',
    fontWeight: '600',
    margin: '0 0 auto 15px',
    textOverflow: 'ellipsis',
  },
};
