import React from 'react';

import Batcher from './Batcher';

const Fetcher = ({ remoteChats }) => {
  const chats = remoteChats.readChats;

  return <Batcher chats={chats} />;
};

export default Fetcher;
