import React from 'react';

import Render from './Render';

const Batcher = ({ chats }) => {
  const groupChats = [];
  const personChats = [];

  chats.map(chat => (chat.type === 'GROUP' ? groupChats.push(chat) : personChats.push(chat)));

  return <Render groupChats={groupChats} personChats={personChats} />;
};

export default Batcher;
