import React from 'react';

import Flag from '../Flag/Main';
import GroupHeader from '../Header/GroupHeader';
import PersonHeader from '../Header/PersonHeader';

import { Viewer } from './Style/StRender';

const Render = ({ groupChats, personChats }) => {
  return (
    <Viewer>
      <GroupHeader />
      {groupChats && groupChats.map(chat => <Flag key={chat.id} chat={chat} />)}
      <PersonHeader />
      {personChats && personChats.map(chat => <Flag key={chat.id} chat={chat} />)}
    </Viewer>
  );
};

export default Render;
