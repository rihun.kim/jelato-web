import React, { useContext } from 'react';

import Fetcher from './Fetcher';
import { ChatContext } from '../../../Share/Context';

const Main = () => {
  const { remoteChats } = useContext(ChatContext);

  return <Fetcher remoteChats={remoteChats} />;
};

export default Main;
