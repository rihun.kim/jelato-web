const filterChatsByKeyword = (_chats, _keyword) =>
  _chats.filter(chat => isKeywordInTitle(chat.title, _keyword) || isKeywordInPlayerNames(chat.players, _keyword));

const isKeywordInTitle = (_title, _keyword) => _title.search(_keyword, 'i') !== -1;

const isKeywordInPlayerNames = (_players, _keyword) => _players.find(player => player.name.search(_keyword, 'i') !== -1);

export const searchChatsByKeyword = (_chats, _keyword) => {
  if (_keyword === '') return _chats;
  else return filterChatsByKeyword(_chats, _keyword);
};
