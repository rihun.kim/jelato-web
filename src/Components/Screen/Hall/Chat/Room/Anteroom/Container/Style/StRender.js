import styled from 'styled-components';

export const Viewer = styled.div`
  width: 270px;
  height: 100%;

  display: flex;
  flex-direction: column;
  justify-content: flex-start;

  border-right: 1px solid #e0e0e0;
  flex-shrink: 0;
  overflow-y: auto;
`;
