import React, { useState } from 'react';

import Anteroom from '../Anteroom/Container/Main';
import Chatroom from '../Chatroom/Container/Main';
// import Create from '../../Modal/Create/Container/Main';
// import Update from '../../Modal/Update/Main';
import { RoomContext } from '../../Share/Context';

const Main = () => {
  // const [keyword, setKeyword] = useState('');
  const [clickChatId, setClickChatId] = useState(false);
  const [createModal, setCreateModal] = useState(false);
  const [updateModal, setUpdateModal] = useState(false);

  const toggleCreateModal = () => {
    createModal ? setCreateModal(false) : setCreateModal(true);
  };
  const toggleUpdateModal = () => {
    updateModal ? setUpdateModal(false) : setUpdateModal(true);
  };

  const clickProps = {
    clickChatId: clickChatId,
    setClickChatId: setClickChatId,
  };
  const modalProps = {
    createModal: createModal,
    updateModal: updateModal,
    toggleCreateModal: toggleCreateModal,
    toggleUpdateModal: toggleUpdateModal,
  };
  // const searchProps = {
  //   keyword: keyword,
  //   setKeyword: setKeyword,
  // };

  // console.log('RoomContext, Container', clickProps.clickChat);
  // 세팅버튼을 토글링 할 때마다, remoteChat 과 clickProps.clickChat 이 달라짐
  // 모달에서 clickChatId 만 갖고 뭔갈 해도됨.
  // 하지만,, 채팅ㅇ방ㅇ에서는?? ㅇ어떻게 대처를???

  return (
    <RoomContext.Provider value={{ clickProps, modalProps }}>
      <Anteroom />
      {clickChatId && <Chatroom />}
      {/* {createModal && <Create />} */}
      {/* {updateModal && <Update />} */}
    </RoomContext.Provider>
  );
};

export default Main;
