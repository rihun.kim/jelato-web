import { createContext } from 'react';

export const ScreenContext = createContext();
export const ChatContext = createContext();
export const RoomContext = createContext();
export const ChatroomContext = createContext();
