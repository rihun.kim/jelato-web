import { gql } from '@apollo/client';

export const CREATE_CHAT = gql`
  mutation createChat($playgroundId: String!, $playerIds: [String], $type: String, $title: String) {
    createChat(playgroundId: $playgroundId, playerIds: $playerIds, type: $type, title: $title) {
      id
      ownerId
      type
      title
      count
      players {
        id
        name
      }
      playground {
        id
      }
      messages {
        id
        owner {
          id
          name
        }
        type
        text
        chat {
          id
        }
        createdAt
      }
      createdAt
      updatedAt
    }
  }
`;

export const READ_CHATS = gql`
  query readChats($playerIds: [String]) {
    readChats(playerIds: $playerIds) {
      id
      ownerId
      type
      title
      count
      players {
        id
        name
      }
      playground {
        id
      }
      messages {
        id
        owner {
          id
          name
        }
        type
        text
        chat {
          id
        }
        createdAt
      }
      createdAt
      updatedAt
    }
  }
`;

export const UPDATE_CHAT = gql`
  mutation updateChat($chatId: String!, $title: String, $inplayerIds: [String], $awayplayerIds: [String]) {
    updateChat(chatId: $chatId, title: $title, inplayerIds: $inplayerIds, awayplayerIds: $awayplayerIds) {
      id
      ownerId
      title
      count
      players {
        id
        name
      }
      playground {
        id
      }
      messages {
        id
        owner {
          id
          name
        }
        type
        text
        chat {
          id
        }
        createdAt
      }
      createdAt
      updatedAt
    }
  }
`;

export const DELETE_CHAT = gql`
  mutation deleteChat($chatId: String!) {
    deleteChat(chatId: $chatId)
  }
`;

export const READ_MESSAGES = gql`
  query readMessages($chatId: String!, $messageId: String) {
    readMessages(chatId: $chatId, messageId: $messageId) {
      id
      owner {
        id
        name
      }
      type
      text
      chat {
        id
      }
      createdAt
    }
  }
`;

export const SUBSCRIBE_CHATS = gql`
  subscription subscribeChats($playerIds: [String]) {
    subscribeChats(playerIds: $playerIds) {
      mutationType
      chatPosition
      node {
        id
        ownerId
        title
        type
        count
        players {
          id
          name
        }
        playground {
          id
        }
        messages {
          id
          owner {
            id
            name
          }
          type
          text
          chat {
            id
          }
          createdAt
        }
        createdAt
        updatedAt
      }
    }
  }
`;

export const CREATE_MESSAGE = gql`
  mutation createMessage($chatId: String!, $playerId: String!, $type: String, $text: String!) {
    createMessage(chatId: $chatId, playerId: $playerId, type: $type, text: $text) {
      id
      owner {
        id
        name
      }
      type
      text
      chat {
        id
      }
      createdAt
    }
  }
`;

export const SUBSCRIBE_MESSAGES = gql`
  subscription subscribeMessages($playerIds: [String]) {
    subscribeMessages(playerIds: $playerIds) {
      id
      owner {
        id
        name
      }
      type
      text
      chat {
        id
      }
      createdAt
    }
  }
`;

export const READ_PLAYERS_IN_PLAYGROUND = gql`
  query readPlayersInPlayground($playgroundId: String!) {
    readPlayersInPlayground(playgroundId: $playgroundId) {
      id
      title
      players {
        id
        name
        position
      }
    }
  }
`;
