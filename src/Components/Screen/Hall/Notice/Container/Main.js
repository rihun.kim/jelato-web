import React, { useContext } from 'react';

import Fetcher from './Fetcher';
import { MainContext } from '../../../../Share/Context';

const Main = ({ match }) => {
  const { IPLAYERS } = useContext(MainContext);

  const iplayer = IPLAYERS.iplayers.filter(iplayer => iplayer.playground.barcode === match.params.barcode)[0];

  return <Fetcher iplayer={iplayer} />;
};

export default Main;
