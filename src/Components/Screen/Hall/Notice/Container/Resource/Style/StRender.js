import styled from 'styled-components';

export const Viewer = styled.div`
  display: flex;
  flex-grow: 1;
  flex-shrink: 0;
  flex-direction: row;
  align-itmes: center;
  justify-content: center;

  overflow-y: auto;
`;

export const InnerViewer = styled.div`
  min-width: 1060px;

  position: relative;
`;
