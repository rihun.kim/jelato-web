import React, { useEffect, useState, useRef, forwardRef } from 'react';

import Header from '../Header/Main';
import Room from '../Room/Container/Main';
import { ScreenContext } from '../Share/Context';
import { Viewer, InnerViewer } from './Resource/Style/StRender';

const Render = () => {
  const viewerRef = useRef();
  const [keyword, setKeyword] = useState('');
  const [clickNotice, setClickNotice] = useState('');
  const [createModal, setCreateModal] = useState(false);
  const [updateModal, setUpdateModal] = useState(false);

  const toggleCreateModal = () => {
    createModal ? setCreateModal(false) : setCreateModal(true);
  };
  const toggleUpdateModal = notice => {
    setClickNotice(notice);
    updateModal ? setUpdateModal(false) : setUpdateModal(true);
  };

  const modalProps = {
    clickNotice: clickNotice,
    createModal: createModal,
    updateModal: updateModal,
    toggleCreateModal: toggleCreateModal,
    toggleUpdateModal: toggleUpdateModal,
  };
  const searchProps = {
    keyword: keyword,
    setKeyword: setKeyword,
  };

  return (
    <Viewer ref={viewerRef}>
      <InnerViewer>
        <Fowarder ref={viewerRef}>
          <Header modalProps={modalProps} searchProps={searchProps} />
          <Room modalProps={modalProps} searchProps={searchProps} />
        </Fowarder>
      </InnerViewer>
    </Viewer>
  );
};

const Fowarder = forwardRef(({ children }, ref) => {
  const [screenRef, setScreenRef] = useState(null);

  useEffect(() => {
    if (ref) setScreenRef(ref);
  }, [ref]);

  return screenRef && <ScreenContext.Provider value={{ screenRef, setScreenRef }}>{children}</ScreenContext.Provider>;
});

export default Render;
