import React from 'react';
import { useQuery } from '@apollo/client';

import Render from './Render';
import { NoticeContext } from '../Share/Context';
import { READ_NOTICES } from '../Share/Gql';

const Fetcher = ({ iplayer }) => {
  const { loading, data: remoteNotices } = useQuery(READ_NOTICES, {
    variables: { playgroundId: iplayer.playground.id },
    fetchPolicy: 'no-cache',
  });

  return (
    !loading && (
      <NoticeContext.Provider value={{ iplayer, remoteNotices }}>
        <Render />
      </NoticeContext.Provider>
    )
  );
};

export default Fetcher;
