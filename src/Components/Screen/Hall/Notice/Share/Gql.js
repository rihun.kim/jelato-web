import { gql } from '@apollo/client';

export const CREATE_NOTICE = gql`
  mutation createNotice($playerId: String!, $playgroundId: String!, $story: String!, $title: String!) {
    createNotice(playerId: $playerId, playgroundId: $playgroundId, story: $story, title: $title) {
      id
      title
      story
      owner {
        id
        name
      }
      createdAt
      updatedAt
    }
  }
`;

export const READ_NOTICES = gql`
  query readNotices($playgroundId: String!, $noticeId: String) {
    readNotices(playgroundId: $playgroundId, noticeId: $noticeId) {
      id
      title
      story
      owner {
        id
        name
      }
      createdAt
      updatedAt
    }
  }
`;

export const UPDATE_NOTICE = gql`
  mutation updateNotice($noticeId: String!, $title: String, $story: String) {
    updateNotice(noticeId: $noticeId, title: $title, story: $story) {
      id
      title
      story
      owner {
        id
        name
      }
      createdAt
      updatedAt
    }
  }
`;

export const DELETE_NOTICE = gql`
  mutation deleteNotice($noticeId: String!) {
    deleteNotice(noticeId: $noticeId)
  }
`;
