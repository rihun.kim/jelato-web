import { createContext } from 'react';

export const NoticeContext = createContext();
export const ScreenContext = createContext();
