const convertTimeToText = time => {
  const week = [' Sun ', ' Mon ', ' Tue ', ' Wed ', ' Thu ', ' Fri ', ' Sat '];
  return time[0] + '.' + time[1] + '.' + time[2] + week[time[3]] + time[4] + ':' + time[5];
};

export const convertTime = date => {
  let time = new Date(+date);

  const cTime = [time.getFullYear(), time.getMonth() + 1, time.getDate(), time.getDay(), time.getHours(), time.getMinutes()];

  cTime[4] = cTime[4] < 10 ? '0' + cTime[4] : cTime[4];
  cTime[5] = cTime[5] < 10 ? '0' + cTime[5] : cTime[5];

  return convertTimeToText(cTime);
};
