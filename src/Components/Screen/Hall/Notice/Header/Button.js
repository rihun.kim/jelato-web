import React from 'react';
import { BsFilePlus } from 'react-icons/bs';

import { CreateButtonStyle, CreateButtonViewer } from './Resource/Style/StButton';

export const CreateButton = ({ onClick }) => {
  return (
    <CreateButtonViewer onClick={onClick}>
      <BsFilePlus title="create" style={CreateButtonStyle} />
    </CreateButtonViewer>
  );
};
