import React from 'react';

import { Viewer, InnerViewer } from './Resource/Style/StMain';
import { Input } from './Input';
import { CreateButton } from './Button';

const Main = ({ modalProps, searchProps }) => {
  return (
    <Viewer>
      <InnerViewer>
        <Input setKeyword={searchProps.setKeyword} />
        <CreateButton onClick={modalProps.toggleCreateModal} />
      </InnerViewer>
    </Viewer>
  );
};

export default Main;
