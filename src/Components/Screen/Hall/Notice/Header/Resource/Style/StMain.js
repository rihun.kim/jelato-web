import styled from 'styled-components';

export const Viewer = styled.div`
  width: 100%;
  height: 80px;

  padding: 21px 0 31px 0;
`;

export const InnerViewer = styled.div`
  width: 330px;
  height: 28px;

  display: flex;
  flex-direction: row;

  border-bottom: 1px solid #e0e0e0;
  margin: auto 0 auto auto;
`;
