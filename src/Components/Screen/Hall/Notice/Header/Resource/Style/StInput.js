import styled from 'styled-components';

export const InputViewer = styled.input`
  width: 258px;
  height: 26px;

  border: none;
  font-size: 14px;
  padding: 0 0 0 15px;
  outline: none;
`;
