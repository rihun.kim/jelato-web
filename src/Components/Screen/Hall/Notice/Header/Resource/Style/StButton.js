import styled from 'styled-components';

export const CreateButtonViewer = styled.div`
  width: 16px;
  height: 16px;

  color: #929191;
  cursor: pointer;
  margin: auto 4px auto 4px;
`;

export const CreateButtonStyle = {
  width: '15px',
  height: '15px',
  strokeWidth: '0.08px',
};
