import React from 'react';

import { InputViewer } from './Resource/Style/StInput';

export const Input = ({ inputAble = true, setKeyword }) => (
  <InputViewer autoFocus="autofocus" disabled={!inputAble} onChange={e => setKeyword(e.target.value)} type="text" />
);
