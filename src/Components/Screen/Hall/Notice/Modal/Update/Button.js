import React from 'react';
import { IoMdClose } from 'react-icons/io';
import { RiDeleteBin5Line } from 'react-icons/ri';

import { MODAL_UPDATE_CHECK_STORY, MODAL_UPDATE_CHECK_TITLE } from './Resource/Constant';
import { BUTTON_CLOSE_STYLE, BUTTON_DELETE_STYLE, CloseButtonViewer, DeleteButtonViewer } from './Resource/Style/StButton';

export const CloseButton = ({ onClick }) => (
  <CloseButtonViewer>
    <IoMdClose title="close" style={BUTTON_CLOSE_STYLE} onClick={onClick} />
  </CloseButtonViewer>
);

export const DeleteButton = ({ onClick }) => (
  <DeleteButtonViewer>
    <RiDeleteBin5Line title="delete" style={BUTTON_DELETE_STYLE} onClick={onClick} />
  </DeleteButtonViewer>
);

const checkBlanks = props => {
  if (props.titleUseInput.value === '') alert(MODAL_UPDATE_CHECK_TITLE);
  else if (props.storyUseInput.value === '') alert(MODAL_UPDATE_CHECK_STORY);
  else return true;
};

const checkChanges = (prevProps, props) => {
  if (prevProps.title !== props.titleUseInput.value) return true;
  else if (prevProps.story !== props.storyUseInput.value) return true;
  else return false;
};

export const onComplete = (inputProps, completeProps) => {
  try {
    const prevProps = completeProps.clickNotice;

    if (checkBlanks(inputProps)) {
      if (checkChanges(prevProps, inputProps)) {
        completeProps.updateNoticeMutation({
          variables: {
            noticeId: completeProps.clickNotice.id,
            title: inputProps.titleUseInput.value,
            story: inputProps.storyUseInput.value,
          },
        });

        completeProps.clickNotice.title = inputProps.titleUseInput.value;
        completeProps.clickNotice.story = inputProps.storyUseInput.value;
      }

      completeProps.toggleModal();
    }
  } catch (e) {
    console.error(e);
  }
};

export const onDelete = props => {
  try {
    props.deleteNoticeMutation({ variables: { noticeId: props.clickNotice.id } });
    const origin = props.remoteNotices.readNotices;
    origin.splice(origin.indexOf(props.clickNotice), 1);

    props.toggleModal();
  } catch (e) {
    console.error(e);
  }
};
