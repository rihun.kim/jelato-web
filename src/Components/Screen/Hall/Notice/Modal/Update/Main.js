import React, { useContext } from 'react';
import { useMutation } from '@apollo/client';

import { Input, TextareaInput, useInput } from './Input';
import { CloseButton, DeleteButton, onComplete, onDelete } from './Button';
import { UPDATE_NOTICE, DELETE_NOTICE } from '../../Share/Gql';
import { Viewer } from './Resource/Style/StMain';
import { reconcileModalTop } from '../Share/Reconcile';
import { NoticeContext, ScreenContext } from '../../Share/Context';

const Main = ({ toggleModal, clickNotice }) => {
  const { screenRef } = useContext(ScreenContext);
  const { remoteNotices } = useContext(NoticeContext);
  const [updateNoticeMutation] = useMutation(UPDATE_NOTICE);
  const [deleteNoticeMutation] = useMutation(DELETE_NOTICE);

  const inputProps = {
    titleUseInput: useInput(clickNotice.title),
    storyUseInput: useInput(clickNotice.story),
  };
  const completeProps = {
    clickNotice: clickNotice,
    updateNoticeMutation: updateNoticeMutation,
    toggleModal: toggleModal,
  };
  const deleteProps = {
    clickNotice: clickNotice,
    remoteNotices: remoteNotices,
    deleteNoticeMutation: deleteNoticeMutation,
    toggleModal: toggleModal,
  };

  const onCompleteClick = () => {
    onComplete(inputProps, completeProps);
  };
  const onDeleteClick = () => {
    onDelete(deleteProps);
  };

  return (
    <Viewer top={() => reconcileModalTop(screenRef)}>
      <CloseButton onClick={onCompleteClick} />
      <DeleteButton onClick={onDeleteClick} />
      <Input {...inputProps.titleUseInput} />
      <TextareaInput {...inputProps.storyUseInput} />
    </Viewer>
  );
};

export default Main;
