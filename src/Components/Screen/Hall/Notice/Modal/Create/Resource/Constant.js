export const MODAL_CREATE_TITLE = '새 공지사항 제목';
export const MODAL_CREATE_STORY = '새 공지사항 내용';
export const MODAL_CREATE_CHECK_TITLE = '새 공지사항 제목을 적어주세요.';
export const MODAL_CREATE_CHECK_STORY = '새 공지사항 내용을 적어주세요.';
