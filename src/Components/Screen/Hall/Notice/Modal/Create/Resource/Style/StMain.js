import styled from 'styled-components';

import { MODAL_DEFAULT_WIDTH, MODAL_DEFAULT_HEIGHT } from '../../../Share/StConstant';

export const Viewer = styled.div`
  width: ${MODAL_DEFAULT_WIDTH}px;
  height: ${MODAL_DEFAULT_HEIGHT}px;

  position: absolute;
  left: 0;
  right: 0;
  top: ${props => props.top}px;
  bottom: 0;

  border-radius: 2px;
  border-top: 2px solid #d6cfff;
  border-bottom: 2px solid #d6cfff;
  box-shadow: 0px 2px 6px 4px rgba(235, 230, 235, 1);
  background-color: #ffffff;
  padding: 40px 25px 20px 25px;
  z-index: 3;
`;
