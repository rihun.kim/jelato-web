import React from 'react';
import { IoMdClose, IoMdRadioButtonOff } from 'react-icons/io';

import { BUTTON_CLOSE_STYLE, BUTTON_SAVE_STYLE, CloseButtonViewer, SaveButtonViewer } from './Resource/Style/StButton';
import { MODAL_CREATE_CHECK_TITLE, MODAL_CREATE_CHECK_STORY } from './Resource/Constant';

export const CloseButton = ({ onClick }) => (
  <CloseButtonViewer>
    <IoMdClose title="close" style={BUTTON_CLOSE_STYLE} onClick={onClick} />
  </CloseButtonViewer>
);

export const SaveButton = ({ onClick }) => (
  <SaveButtonViewer>
    <IoMdRadioButtonOff title="save" style={BUTTON_SAVE_STYLE} onClick={onClick} />
  </SaveButtonViewer>
);

const checkBlanks = props => {
  if (props.titleUseInput.value === '') alert(MODAL_CREATE_CHECK_TITLE);
  else if (props.storyUseInput.value === '') alert(MODAL_CREATE_CHECK_STORY);
  else return true;
};

export const onComplete = async props => {
  try {
    if (checkBlanks(props)) {
      await props.createNoticeMutation({
        variables: {
          playerId: props.iplayer.id,
          playgroundId: props.iplayer.playground.id,
          title: props.titleUseInput.value,
          story: props.storyUseInput.value,
        },
      });

      window.location.reload();
    }
  } catch (e) {
    console.error(e);
  }
};
