import React, { useContext } from 'react';
import { useMutation } from '@apollo/client';

import { MODAL_CREATE_TITLE, MODAL_CREATE_STORY } from './Resource/Constant';
import { NoticeContext, ScreenContext } from '../../Share/Context';
import { useInput, Input, TextareaInput } from './Input';
import { onComplete, CloseButton, SaveButton } from './Button';
import { Viewer } from './Resource/Style/StMain';
import { reconcileModalTop } from '../Share/Reconcile';
import { CREATE_NOTICE } from '../../Share/Gql';

const Main = ({ toggleModal }) => {
  const { screenRef } = useContext(ScreenContext);
  const { iplayer } = useContext(NoticeContext);
  const [createNoticeMutation] = useMutation(CREATE_NOTICE);

  const inputProps = {
    titleUseInput: useInput(MODAL_CREATE_TITLE),
    storyUseInput: useInput(MODAL_CREATE_STORY),
    iplayer: iplayer,
    createNoticeMutation: createNoticeMutation,
  };

  const onCompleteClick = () => {
    onComplete(inputProps);
  };

  return (
    <Viewer top={() => reconcileModalTop(screenRef)}>
      <CloseButton onClick={toggleModal} />
      <SaveButton onClick={onCompleteClick} />
      <Input {...inputProps.titleUseInput} />
      <TextareaInput {...inputProps.storyUseInput} />
    </Viewer>
  );
};

export default Main;
