import styled from 'styled-components';

export const Viewer = styled.div`
  min-width: 1060px;

  padding: 0 0 20px 0;
`;
