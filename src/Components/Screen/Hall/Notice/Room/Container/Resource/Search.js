export const searchNotices = (notices, keyword) => {
  if (keyword === '') return notices;
  return searchKeywordInNotices(notices, keyword);
};

const searchKeywordInNotices = (notices, keyword) => {
  return notices.filter(
    notice =>
      searchKeywordInTitle(notice.title, keyword) ||
      searchKeywordInStory(notice.story, keyword) ||
      searchKeywordInOwner(notice.owner.name, keyword),
  );
};

const searchKeywordInTitle = (title, keyword) => title.search(keyword, 'i') !== -1;
const searchKeywordInStory = (story, keyword) => story.search(keyword, 'i') !== -1;
const searchKeywordInOwner = (name, keyword) => name.search(keyword, 'i') !== -1;
