import React, { useContext } from 'react';

import Create from '../../Modal/Create/Main';
import Update from '../../Modal/Update/Main';
import { NoticeContext } from '../../Share/Context';
import { searchNotices } from './Resource/Search';
import { Paper } from '../Paper/Main';
import { Viewer } from './Resource/Style/StMain';

const Main = ({ modalProps, searchProps }) => {
  const { remoteNotices } = useContext(NoticeContext);
  const notices = searchNotices(remoteNotices.readNotices, searchProps.keyword);

  return (
    <Viewer>
      {notices.map(notice => (
        <Paper key={notice.id} notice={notice} onClick={() => modalProps.toggleUpdateModal(notice)} />
      ))}
      {modalProps.createModal && <Create toggleModal={modalProps.toggleCreateModal} />}
      {modalProps.updateModal && <Update toggleModal={modalProps.toggleUpdateModal} clickNotice={modalProps.clickNotice} />}
    </Viewer>
  );
};

export default Main;
