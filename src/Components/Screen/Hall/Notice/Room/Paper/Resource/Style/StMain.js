import styled from 'styled-components';

import { PAPER_DEFAULT_WIDTH } from '../StConstant';

export const Text = styled.div`
  width: ${props => props.width};
  height: ${props => props.height};

  color: ${props => props.color};
  float: ${props => props.float};
  font-size: ${props => props.fontSize};
  font-weight: ${props => props.fontWeight};
  margin: ${props => props.margin};
  white-space: pre-wrap;
`;

export const PaperViewer = styled.div`
  width: ${PAPER_DEFAULT_WIDTH};
  height: 100%;

  border-radius: 3px;
  box-shadow: 0px 2px 2px 0px rgba(179, 177, 179, 1);
  cursor: pointer;
  margin: 0 0 30px 0;
  padding: 20px 20px 20px 20px;
  position: relative;
`;

export const PaperStyle = {
  info: {
    color: '#3e3d3d',
    fontSize: '12px',
    fontWeight: '100',
    margin: '10px 0 0 0',
    overflow: 'hidden',
  },
  title: {
    fontSize: '25px',
    fontWeight: '600',
    overflow: 'hidden',
  },
  story: {
    fontSize: '14px',
    fontWeight: '300',
    margin: '30px 0 0 0',
    overflow: 'hidden',
  },
};
