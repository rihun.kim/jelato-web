import React from 'react';

import { convertTime } from '../../Share/Convert';
import { PaperViewer, PaperStyle, Text } from './Resource/Style/StMain';

export const Paper = ({ notice, onClick }) => (
  <PaperViewer onClick={onClick}>
    <Text style={PaperStyle.title}>{notice.title}</Text>
    <Text style={PaperStyle.info}>
      {notice.owner.name} | {convertTime(notice.createdAt)}
    </Text>
    <Text style={PaperStyle.story}>{notice.story}</Text>
  </PaperViewer>
);
