import React from 'react';

import { ButtonViewer } from './Resource/Style/StButton';

export const onComplete = async (
  passwordUseInput,
  passwordConfirmUseInput,
  updateUserPasswordMutation,
  setInputAble,
  setButtonAble,
  setClickView,
) => {
  try {
    if (passwordUseInput.value === '') {
      alert('새 비밀번호 입력하세요.');
      return;
    } else if (passwordUseInput.value !== passwordConfirmUseInput.value) {
      alert('변경하려는 새 비밀번호를 동일하게 입력하세요.');
      return;
    }

    setButtonAble(false);
    setInputAble(false);

    const {
      data: { updateUserPassword: userPssword },
    } = await updateUserPasswordMutation({
      variables: {
        password: passwordUseInput.value,
      },
    });

    if (userPssword.includes('[ALERT]')) {
      alert(userPssword);
      setButtonAble(true);
      setInputAble(true);
    } else {
      setClickView('logoutView');
      alert(userPssword);
    }
  } catch (error) {
    alert(error);
  }
};

export const Button = ({ buttonAble, onClick }) => (
  <ButtonViewer onClick={onClick} disabled={!buttonAble}>
    완료
  </ButtonViewer>
);
