import styled from 'styled-components';

import { MAIN_PINK_COLOR } from '../../../../../../Share/StConstant';

export const ButtonViewer = styled.button`
  width: 200px;
  height: 40px;

  background-color: ${MAIN_PINK_COLOR};
  border: 0;
  border-radius: 2px;
  font-size: 14px;
  text-align: center;

  &:hover {
    background-color: #ffe6f1;
  }
`;
