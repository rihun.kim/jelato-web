import React, { useState } from 'react';
import { useMutation } from '@apollo/client';

import { useInput, Input } from './Input';
import { onComplete, Button } from './Button';
import { Viewer, Text } from './Resource/Style/StMain';
import { UPDATE_USERPASSWORD } from '../Share/Gql';

const Main = ({ setClickView }) => {
  const [inputAble, setInputAble] = useState(true);
  const [buttonAble, setButtonAble] = useState(true);
  const [updateUserPasswordMutation] = useMutation(UPDATE_USERPASSWORD);
  const passwordUseInput = useInput('새 비밀번호');
  const passwordConfirmUseInput = useInput('새 비밀번호 확인');

  const onCompleteClick = () => {
    onComplete(passwordUseInput, passwordConfirmUseInput, updateUserPasswordMutation, setInputAble, setButtonAble, setClickView);
  };

  return (
    <Viewer>
      <Text>변경하실 비밀번호를 입력해주세요.</Text>
      <Input type="password" inputAble={inputAble} {...passwordUseInput} />
      <Input type="password" inputAble={inputAble} {...passwordConfirmUseInput} />
      <Button buttonAble={buttonAble} placeholder="완료" onClick={onCompleteClick} />
    </Viewer>
  );
};

export default Main;
