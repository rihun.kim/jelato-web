import { gql } from '@apollo/client';

export const UPDATE_USERPASSWORD = gql`
  mutation updateUserPassword($email: String, $password: String!, $authword: String) {
    updateUserPassword(email: $email, password: $password, authword: $authword)
  }
`;

export const DELETE_USER = gql`
  mutation deleteUser($password: String!) {
    deleteUser(password: $password)
  }
`;
