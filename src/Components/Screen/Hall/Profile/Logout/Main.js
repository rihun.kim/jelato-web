import React, { useState } from 'react';

import { onLogout, LogoutButton, SignoutButton, UpdateButton, ButtonGroup } from './Button';
import { Viewer, Text } from './Resource/Style/StMain';
import { getUserEmail } from '../../../../../Auth/Email';
import { useDoLogout } from '../../../../../Auth/Context';

const Main = ({ setClickView }) => {
  const [buttonAble, setButtonAble] = useState(true);
  const doLogout = useDoLogout();
  const userEmail = getUserEmail();

  const onSignoutClick = () => {
    setClickView('signoutView');
  };
  const onUpdateClick = () => {
    setClickView('updateView');
  };
  const onLogoutClick = () => {
    onLogout({ doLogout, setButtonAble });
  };

  return (
    <Viewer>
      <Text>
        {userEmail} 사용자님
        <br />
        다음에 또 봤으면 좋겠어요:P
      </Text>
      <LogoutButton buttonAble={buttonAble} onClick={onLogoutClick} />
      <ButtonGroup>
        <SignoutButton onClick={onSignoutClick} />
        <UpdateButton onClick={onUpdateClick} />
      </ButtonGroup>
    </Viewer>
  );
};

export default Main;
