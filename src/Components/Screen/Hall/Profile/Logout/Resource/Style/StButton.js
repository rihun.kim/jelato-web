import styled from 'styled-components';

import { MAIN_PINK_COLOR, MAIN_CHOCO_COLOR } from '../../../../../../Share/StConstant';

export const LogoutButtonViewer = styled.button`
  width: 200px;
  height: 40px;

  background-color: #ffffff;
  border: 0;
  border-radius: 2px;
  font-size: 14px;
  text-align: center;
  margin: 40px 0 0 0;

  &:hover {
    background-color: ${MAIN_PINK_COLOR};
  }
`;

export const SignoutButtonViewer = styled.button`
  height: 30px;

  background-color: ${MAIN_CHOCO_COLOR};
  border: none;
  color: #cccccc;
  font-size: 11px;
  outline: 0;
`;

export const UpdateButtonViewer = styled.button`
  height: 30px;

  background-color: ${MAIN_CHOCO_COLOR};
  border: none;
  color: #cccccc;
  font-size: 11px;
  outline: 0;
`;

export const ButtonGroupViewer = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: center;
  margin: 10px 0 0 0;

  *:nth-child(1) {
    margin: 0 10px 0 0;
  }

  *:hover {
    color: ${MAIN_PINK_COLOR};
  }
`;
