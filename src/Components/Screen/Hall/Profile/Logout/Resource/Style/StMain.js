import styled from 'styled-components';

export const Viewer = styled.div`
  width: 300px;
  height: 240px;

  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
`;

export const Text = styled.div`
  color: #ffffff;
  font-size: 14px;
  line-height: 1.6;
  margin: 20px 0 0 0;
  text-align: center;
`;
