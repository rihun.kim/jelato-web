import React from 'react';

import { LogoutButtonViewer, SignoutButtonViewer, UpdateButtonViewer, ButtonGroupViewer } from './Resource/Style/StButton';

export const LogoutButton = ({ buttonAble, onClick }) => (
  <LogoutButtonViewer onClick={onClick} disabled={!buttonAble}>
    logout
  </LogoutButtonViewer>
);

export const ButtonGroup = ({ children }) => <ButtonGroupViewer>{children}</ButtonGroupViewer>;

export const SignoutButton = ({ onClick }) => <SignoutButtonViewer onClick={onClick}>회원 탈퇴</SignoutButtonViewer>;

export const UpdateButton = ({ onClick }) => <UpdateButtonViewer onClick={onClick}>PW 변경</UpdateButtonViewer>;

export const onLogout = ({ doLogout, setButtonAble }) => {
  try {
    setButtonAble(false);
    doLogout();
  } catch (e) {
    setButtonAble(true);
    console.err(e);
  }
};
