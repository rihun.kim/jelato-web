import styled from 'styled-components';

export const Viewer = styled.div`
  width: 700px;
  height: 300px;

  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: center;
  position: absolute;
  left: 0;
  right: 0;
  top: 0;
  bottom: 0;

  background-color: #2b2b28;
  border-radius: 2px;
  box-shadow: 1px 3px 4px rgba(0, 0, 0, 0.19), 0 1px 0 rgba(0, 0, 0, 0.23);
  margin: auto auto auto auto;
  z-index: 3;
`;
