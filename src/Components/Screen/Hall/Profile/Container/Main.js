import React, { useState } from 'react';

import Logout from '../Logout/Main';
import Signout from '../Signout/Main';
import Update from '../Update/Main';
import { CloseButton } from './Button';
import { Viewer } from './Resource/Style/StMain';

const Main = ({ toggleModal }) => {
  const [clickView, setClickView] = useState('logoutView');

  return (
    <Viewer>
      <CloseButton onClick={toggleModal} />
      {clickView === 'logoutView' && <Logout setClickView={setClickView} />}
      {clickView === 'signoutView' && <Signout setClickView={setClickView} />}
      {clickView === 'updateView' && <Update setClickView={setClickView} />}
    </Viewer>
  );
};

export default Main;
