import React from 'react';
import { IoMdClose } from 'react-icons/io';

import { CloseButtonViewer } from './Resource/Style/StButton';

export const CloseButton = ({ onClick }) => (
  <CloseButtonViewer>
    <IoMdClose
      style={{
        color: '#ffffff',
        cursor: 'pointer',
        position: 'absolute',
        right: 10,
        top: 10,
        zIndex: 3,
      }}
      onClick={onClick}
    />
  </CloseButtonViewer>
);
