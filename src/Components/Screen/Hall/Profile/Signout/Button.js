import React from 'react';

import { ButtonViewer } from './Resource/Style/StButton';

export const onComplete = async (passwordUseInput, deleteUserMutation, doLogout, setButtonAble, setInputAble) => {
  try {
    if (passwordUseInput.value === '') {
      alert('회원탈퇴 하시려면 비밀번호 입력해주세요.');
      return;
    }

    setButtonAble(false);
    setInputAble(false);

    const {
      data: { deleteUser: user },
    } = await deleteUserMutation({
      variables: {
        password: passwordUseInput.value,
      },
    });

    alert(user);
    if (user.includes('[ALERT]')) {
      setButtonAble(true);
      setInputAble(true);
    } else doLogout();
  } catch (error) {
    alert(error);
  }
};

export const Button = ({ buttonAble, onClick }) => (
  <ButtonViewer onClick={onClick} disabled={!buttonAble}>
    완료
  </ButtonViewer>
);
