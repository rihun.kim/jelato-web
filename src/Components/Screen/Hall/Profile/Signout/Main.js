import React, { useState } from 'react';
import { useMutation } from '@apollo/client';

import { onComplete, Button } from './Button';
import { useInput, Input } from './Input';
import { Viewer, Text } from './Resource/Style/StMain';
import { useDoLogout } from '../../../../../Auth/Context';
import { DELETE_USER } from '../Share/Gql';

const Main = () => {
  const [inputAble, setInputAble] = useState(true);
  const [buttonAble, setButtonAble] = useState(true);
  const [deleteUserMutation] = useMutation(DELETE_USER);
  const passwordUseInput = useInput('비밀번호');
  const doLogout = useDoLogout();

  const onCompleteClick = () => {
    onComplete(passwordUseInput, deleteUserMutation, doLogout, setButtonAble, setInputAble);
  };

  return (
    <Viewer>
      <Text>정말 회원 탈퇴하실건가요?</Text>
      <Input type="password" inputAble={inputAble} {...passwordUseInput} />
      <Button buttonAble={buttonAble} onClick={onCompleteClick} />
    </Viewer>
  );
};

export default Main;
