import React from 'react';

import Render from './Render';

const Fetcher = ({ remoteNotifiers }) => {
  const remoteNotifications = [];
  remoteNotifiers.map(notifier => remoteNotifications.push(...notifier.notifications));

  return <Render remoteNotifiers={remoteNotifiers} remoteNotifications={remoteNotifications} />;
};

export default Fetcher;
