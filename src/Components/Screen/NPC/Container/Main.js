import React, { useContext } from 'react';

import Fetcher from './Fetcher';
import { NotificationContext } from '../../../Share/Context';

const Main = () => {
  const { remoteNotifiers } = useContext(NotificationContext);

  return <Fetcher remoteNotifiers={remoteNotifiers} />;
};

export default Main;
