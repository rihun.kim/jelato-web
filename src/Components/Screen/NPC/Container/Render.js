import React, { useEffect, useState } from 'react';
import { useMutation } from '@apollo/client';

import Notification from '../Notification/Main';
import { onConfirm } from '../Notification/Button';
import { DELETE_NOTIFICATION } from '../Share/Gql';

const Render = ({ remoteNotifiers, remoteNotifications }) => {
  const [notifications, setNotifications] = useState(remoteNotifications);
  const [deleteNotificationMutation] = useMutation(DELETE_NOTIFICATION);

  const onConfirmClick = notification => {
    notificationProps.notification = notification;
    onConfirm(notificationProps);
  };

  const notificationProps = {
    notifications: notifications,
    remoteNotifiers: remoteNotifiers,
    setNotifications: setNotifications,
    deleteNotificationMutation: deleteNotificationMutation,
  };

  useEffect(() => {
    setNotifications(remoteNotifications);
  }, [remoteNotifications]);

  return notifications.map(notification => <Notification key={notification.id} notification={notification} onClick={onConfirmClick} />);
};

export default Render;
