import { gql } from '@apollo/client';

export const DELETE_NOTIFICATION = gql`
  mutation deleteNotification($notificationId: String!) {
    deleteNotification(notificationId: $notificationId)
  }
`;
