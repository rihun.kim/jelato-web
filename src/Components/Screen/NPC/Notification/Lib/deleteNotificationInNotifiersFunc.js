export const deleteNotificationInNotifiers = (_notification, _notifiers) => {
  const deleteNotifierIndex = _notifiers.findIndex(notifier => notifier.id === _notification.addressee.id);

  const deleteNotificationIndex = _notifiers[deleteNotifierIndex].notifications.findIndex(
    notification => notification.id === _notification.id,
  );

  _notifiers[deleteNotifierIndex].notifications.splice(deleteNotificationIndex, 1);
};
