import React from 'react';

import Message from './Message';
import { ConfirmButton } from './Button';
import { Viewer } from './Style/StMain';

const Main = ({ notification, onClick }) => {
  return (
    <Viewer>
      <Message title={notification.title} story={notification.story} />
      <ConfirmButton onClick={() => onClick(notification)} />
    </Viewer>
  );
};

export default Main;
