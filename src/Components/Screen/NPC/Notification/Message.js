import React from 'react';

import { MessageViewer } from './Style/StMessage';
import { Text, TextStyle } from './Style/StText';

const Message = ({ title, story }) => {
  return (
    <MessageViewer>
      <Text style={TextStyle.title}>{title}</Text>
      <Text style={TextStyle.story}>{story}</Text>
    </MessageViewer>
  );
};

export default Message;
