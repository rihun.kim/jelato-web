import styled from 'styled-components';

export const MessageViewer = styled.div`
  width: 280px;
  height: 100%;

  display: flex;
  flex-direction: column;
  justify-content: flex-start;
  align-items: center;

  padding: 10px 10px 10px 10px;
`;
