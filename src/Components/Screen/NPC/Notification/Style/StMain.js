import styled from 'styled-components';

export const Viewer = styled.div`
  width: 340px;
  height: 70px;

  display: flex;
  flex-direction: row;
  justify-content: flex-start;
  align-items: center;
  flex-shrink: 0;
  flex-grow: 1;

  position: absolute;
  top: 20px;
  right: 10px;

  background-color: #fbfbfbe6;
  border: 0;
  border-radius: 5px;
  box-shadow: 0px 2px 1px 0px rgba(214, 214, 214, 1);
  z-index: 100;

  animation-name: moveRightToLeft;
  animation-duration: 1s;
  animation-iteration-count: 1;
  @keyframes moveRightToLeft {
    from {
      right: -300px;
    }
    to {
      right: 10px;
    }
  }
`;
