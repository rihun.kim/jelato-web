import styled from 'styled-components';

export const ConfirmButtonViewer = styled.div`
  width: 60px;
  height: 100%;

  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;

  border-left: 1px solid #efefef;
  cursor: pointer;

  &:hover {
    background-color: #fdf0f6;
  }
`;
