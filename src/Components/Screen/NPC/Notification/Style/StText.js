import styled from 'styled-components';

export const Text = styled.div`
  width: ${props => props.width};
  height: ${props => props.height};

  color: ${props => props.color};
  float: ${props => props.float};
  font-size: ${props => props.fontSize};
  font-weight: ${props => props.fontWeight};
  margin: ${props => props.margin};
  text-align: ${props => props.textAlign};
  text-overflow: ${props => props.textOverflow};
  white-space: nowrap;
`;

export const TextStyle = {
  title: {
    width: '100%',
    height: '20px',
    color: '#6E7273',
    fontSize: '13px',
    fontWeight: '400',
    float: 'left',
    textOverflow: 'ellipsis',
  },
  story: {
    width: '100%',
    height: '100%',
    color: '#838788',
    fontSize: '11px',
    fontWeight: '348',
    float: 'left',
    margin: '5px 0 0 0',
    textOverflow: 'ellipsis',
  },
  confirm: {
    width: '100%',
    height: '18px',
    color: '#6E7273',
    fontSize: '11px',
    fontWeight: '348',
    textAlign: 'center',
    textOverflow: 'ellipsis',
  },
};
