import React from 'react';

import { deleteNotificationInNotifiers } from './Lib/deleteNotificationInNotifiersFunc';
import { ConfirmButtonViewer } from './Style/StButton';
import { Text, TextStyle } from './Style/StText';

export const onConfirm = props => {
  props.setNotifications(props.notifications.filter(notification => notification.id !== props.notification.id));

  deleteNotificationInNotifiers(props.notification, props.remoteNotifiers);

  props.deleteNotificationMutation({
    variables: {
      notificationId: props.notification.id,
    },
  });
};

export const ConfirmButton = ({ onClick }) => {
  return (
    <ConfirmButtonViewer onClick={onClick}>
      <Text style={TextStyle.confirm}>confirm</Text>
    </ConfirmButtonViewer>
  );
};
