import React from "react";
import ReactDOM from "react-dom";

import App from "./App/Main";

ReactDOM.render(<App />, document.getElementById("root"));
