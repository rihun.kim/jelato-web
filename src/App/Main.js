import React, { useEffect, useState } from 'react';

import { setAuth } from '../Auth/Token';
import { setApollo } from './Apollo/Main';
import { ExecRoute, WaitRoute } from './Route';

const Main = () => {
  const [userToken, setUserToken] = useState(false);
  const [apolloClient, setApolloClient] = useState(false);

  useEffect(() => {
    setApollo(setApolloClient);
    setAuth(setUserToken);
  }, []);

  if (!userToken || !apolloClient) return <WaitRoute />;
  return <ExecRoute apolloClient={apolloClient} userToken={userToken} />;
};

export default Main;
