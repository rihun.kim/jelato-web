import { ApolloClient, InMemoryCache } from '@apollo/client';
import { onError } from 'apollo-link-error';
import { ApolloLink, split } from 'apollo-link';
import { getMainDefinition } from 'apollo-utilities';

import { cacheProperties } from './Cache';
import { authHttpLink, authWsLink } from './Link';
import { handleGraphqlErrors, handleNetworkError } from './Error';

export const setApollo = setApolloClient => {
  setApolloClient(
    new ApolloClient({
      cache: new InMemoryCache({ ...cacheProperties }),
      link: ApolloLink.from([
        onError(({ graphQLErrors, networkError }) => {
          if (graphQLErrors) handleGraphqlErrors(graphQLErrors);
          if (networkError) handleNetworkError(networkError);
        }),
        split(
          ({ query }) => {
            const definition = getMainDefinition(query);
            return definition.kind === 'OperationDefinition' && definition.operation === 'subscription';
          },
          authWsLink,
          authHttpLink,
        ),
      ]),
    }),
  );
};
