import { HttpLink } from '@apollo/client';
import { WebSocketLink } from 'apollo-link-ws';

import { DEV_NODE_ENV, DEV_HTTP_SERVER_IP, DEV_WS_SERVER_IP, OPS_HTTP_SERVER_IP, OPS_WS_SERVER_IP } from './Constant';
import { SESSION_KEY_USERTOKEN } from '../../Auth/Constant';

//
export const authHttpLink = new HttpLink({
  uri: process.env.NODE_ENV === DEV_NODE_ENV ? DEV_HTTP_SERVER_IP : OPS_HTTP_SERVER_IP,
  headers: {
    Authorization: `Bearer ${sessionStorage.getItem(SESSION_KEY_USERTOKEN)}`,
  },
});

//
export const authWsLink = new WebSocketLink({
  uri: process.env.NODE_ENV === DEV_NODE_ENV ? DEV_WS_SERVER_IP : OPS_WS_SERVER_IP,
  options: {
    reconnect: true,
    connectionParams: {
      token: sessionStorage.getItem(SESSION_KEY_USERTOKEN),
    },
  },
});
