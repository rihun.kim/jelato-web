export const handleGraphqlErrors = graphQLErrors => {
  graphQLErrors.map(({ message, locations, path }) =>
    console.log(`GraphQL Error:: Message: ${message}, Location: ${locations}, Path: ${path}`),
  );
};

export const handleNetworkError = message => {
  console.log(`Network Error:: Message: ${JSON.stringify(message)}`);
};
