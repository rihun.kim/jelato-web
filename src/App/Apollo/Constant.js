export const DEV_NODE_ENV = 'development';

export const DEV_HTTP_SERVER_IP = 'http://localhost:1990/graphql';
// export const DEV_HTTP_SERVER_IP = 'http://192.168.35.2:1990/graphql';
export const OPS_HTTP_SERVER_IP = '';

export const DEV_WS_SERVER_IP = 'ws://localhost:1990/graphql';
// export const DEV_WS_SERVER_IP = 'ws://192.168.35.2:1990/graphql';
export const OPS_WS_SERVER_IP = '';
