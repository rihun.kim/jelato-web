import { chatTypePolicies } from './TypePolicies';

export const cacheProperties = {
  addTypename: true,
  typePolicies: {
    ...chatTypePolicies,
  },
};
