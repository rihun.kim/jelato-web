import React from 'react';
import { ApolloProvider } from '@apollo/client';

import { AuthProvider } from '../Auth/Context';
import Navigation from '../Components/Navigation/Container/Main';

export const ExecRoute = ({ apolloClient, userToken }) => {
  return (
    <ApolloProvider client={apolloClient}>
      <AuthProvider userToken={userToken}>
        <Navigation />
      </AuthProvider>
    </ApolloProvider>
  );
};

export const WaitRoute = () => {
  return <></>;
};
